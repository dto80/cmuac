'use strict';

// Declare app level module which depends on views, and components
var labApp = angular.module('labApp', [
  'ngRoute',
  'ui.bootstrap',
  'languageControllers',
  'languageServices',
  'pascalprecht.translate',
  'ui.grid',
  'ngCookies',
  'flow',
  'securityControllers',
  'studentControllers',
  'activityControllers',
  'activityViewControllers',
  'editStaffControllers',
  'checkStudentControllers',
  'facultyControllers', //
  'activityTemplateControllers',
  'activityReviewTemplateControllers',
  'singletonFactories',
  'ngCookies',
  'ngActivityIndicator'
]);

labApp.config(['$routeProvider',
  function ($routeProvider) {
    $routeProvider.
    when('/login/:hash/:type', {
        templateUrl: 'views/login.html',
        controller: 'loginController'
    })
    .when('/login/loginList', {
        templateUrl: 'views/loginList.html',
        controller: 'loginListController'
    })
    .otherwise({
        redirectTo: '/login/loginList'
    });
  }]);

labApp.config(function ($translateProvider, $activityIndicatorProvider) {
    $activityIndicatorProvider.setActivityIndicatorStyle('SpinnerDark');
    
    $translateProvider.useUrlLoader('/messageBundle');
    $translateProvider.useStorage('UrlLanguageStorage');
    $translateProvider.preferredLanguage('en');
    $translateProvider.fallbackLanguage('en');
})

labApp.config(['flowFactoryProvider', function (flowFactoryProvider) {
    flowFactoryProvider.defaults = {
        target: '',
        permanentErrors: [500, 501],
        maxChunkRetries: 1,
        chunkRetryInterval: 5000,
        simultaneousUploads: 4,
        singleFile: false
    };
    flowFactoryProvider.on('catchAll', function (event) {
        if($rootScope.debug){
            console.log('catchAll', arguments);
        }
    });
    // Can be used with different implementations of Flow.js
    // flowFactoryProvider.factory = fustyFlowFactory;
}]);

var mainURL = "http://wolvescorporation.com/cmuac/system/api/json/main.php?apikey=cfcd208495d565ef66e7dff9f98764da&code=znrrb";
var redirectURL = 'http://www.google.com';
var redirectPDFURL = "http://wolvescorporation.com/cmuac/system/mod_ex/CMUActivity/o_print_studentenroll_approve.php?code=znrrb&activity_id=";

labApp.config(['$locationProvider', '$httpProvider', function ($locationProvider, $httpProvider) {
    /* Register error provider that shows message on failed requests or redirects to login page on
     * unauthenticated requests */
    $httpProvider.interceptors.push(function ($q, $rootScope, $location) {
        return {
            'responseError': function (rejection) {
                var status = rejection.status;
                var config = rejection.config;
                var method = config.method;
                var url = config.url;

                if (status == 401) {
                    // $location.path("/listProduct");
                } else {
                    $rootScope.error = method + " on " + url + " failed with status " + status;
                }
                return $q.reject(rejection);
            }
        }
    });

    /* Registers auth token interceptor, auth token is either passed by header or by query parameter
     * as soon as there is an authenticated user */
    var exampleAppConfig = {
        /* When set to false a query parameter is used to pass on the auth token.
         * This might be desirable if headers don't work correctly in some
         * environments and is still secure when using https. */
        useAuthTokenHeader: true
    };

    $httpProvider.interceptors.push(function ($q, $rootScope, $location) {
        return {
            'request': function (config) {
                if (angular.isDefined($rootScope.authToken)) {
                    var authToken = $rootScope.authToken;
                    if (exampleAppConfig.useAuthTokenHeader) {
                        config.headers['X-Auth-Token'] = authToken;
                    } else {
                        config.url = config.url + "?token=" + authToken;
                    }
                }
                return config || $q.when(config);
            }

        }
    })

}]).run(function ($rootScope, $location, $cookieStore, UserService, facultyFactory, $timeout) {
    $rootScope.userLoginInfo = {};
    $rootScope.debug = true;
    $rootScope.resultMessage = false;
    $rootScope.errorMessage = false;
    
    $rootScope.$watch('resultMessage', function(){
        $timeout(function() {
            $rootScope.resultMessage = false;
        }, 2000);
    }, true);

    $rootScope.$watch('errorMessage', function(){
        $timeout(function() {
            $rootScope.errorMessage = false;
        }, 2000);
    }, true);

    $rootScope.$on('$viewContentLoaded', function () {
        delete $rootScope.error;
    });

    $rootScope.hasRole = function (role) {
        if ($rootScope.user == undefined) {
            return false;
        }

        if ($rootScope.user.roles[role] == undefined) {
            return false;
        }

        return $rootScope.user.roles[role];
    }

    $rootScope.logout = function () {
        delete $rootScope.user;
        delete $rootScope.authToken;
        $cookieStore.remove('authToken');
        // $location.path("/listProduct")
    }

    /* Try getting valid user from cookie or go to login page */
    var originalPath = $location.path();
    // $location.path("/listProduct");
    var authToken = $cookieStore.get('authToken');
    if (authToken != undefined) {
        $rootScope.authToken = authToken;
        UserSerivce.get(function (user) {
            $rootScope.user = user;
            $location.path(originalPath);
        })
    }
    $rootScope.initialized = true;
    // initilize data
    // just to show what should be here
    $rootScope.facultyList = null;
    $rootScope.loadFacultyList = function(){
        $rootScope.facultyList = {};
        facultyFactory.getFaculties().$promise.then(
            function (returnValue) {
                // success result
                $rootScope.facultyList = returnValue;
            });
    }
});


labApp.directive('focusMe', function ($timeout) {
    return {
        link: function (scope, element, attrs) {
            scope.$watch(attrs.focusMe, function (value) {
                if (value === true) {
                    if($rootScope.debug){
                        console.log('value=', value);
                    }
                    //$timeout(function() {
                    element[0].focus();
                    scope[attrs.focusMe] = false;
                    //});
                }
            });
        }
    };
});

var loadFaculties = function ($q, $timeout, facultyFactory, $http){
    var defer = $q.defer();
    facultyFactory.getFaculties({},
        function(faculties){
            defer.resolve(faculties);
        },
        function(error){
            defer.reject();
        });
    return defer.promise;
};

var checkLogin = function($rootScope, $cookieStore, $window){
    $rootScope.userLoginInfo = $cookieStore.get('userLoginInfo');

    if($rootScope.debug){
        console.log(typeof $rootScope.userLoginInfo);
    }
    if(typeof $rootScope.userLoginInfo === "undefined"){
        $window.location.href = redirectURL;
    }
    else if($rootScope.userLoginInfo.loginID == undefined || $rootScope.userLoginInfo.loginUsername == undefined || $rootScope.userLoginInfo.loginFacultyID == undefined){ //Check login if not redirect to another website
        $window.location.href = redirectURL;
    }

    return $rootScope.userLoginInfo;
}

labApp.controller('loginController', ['$scope', '$rootScope', '$location', '$routeParams', 'httpRequest', '$cookieStore', function ($scope, $rootScope, $location, $routeParams, httpRequest, $cookieStore) {
  //remove old cookie first
  $cookieStore.remove("loginID");
  $cookieStore.remove("loginUsername");
  $cookieStore.remove("loginFacultyID");
  $cookieStore.remove("loginType");

  httpRequest.get('&action=login&user_type=' + $routeParams.type + '&user_hash=' + $routeParams.hash, function(res){
    if($rootScope.debug){
        console.log("Login: " + JSON.stringify(res));
    }

    var userLoginInformation = {};
    userLoginInformation.loginID = res[0].user_id;
    userLoginInformation.loginUsername = res[0].user_name;
    userLoginInformation.loginFacultyID = res[0].user_faculty_id;
    userLoginInformation.loginType = $routeParams.type;

    $cookieStore.put('userLoginInfo', userLoginInformation);

    $rootScope.userLoginInfo = $cookieStore.get('userLoginInfo');

    if($rootScope.userLoginInfo.loginType == "staff"){
        if($rootScope.debug){
            console.log("staff");
        }
        $location.path('/faculty/editStaff');
    }
    else{
        if($rootScope.debug){
            console.log("student");
        }
        $location.path('/studentRegisterActivity');
    }
  });
}]);

labApp.controller('sideMenuController', function ($scope, $rootScope, $cookieStore){
    $scope.getUserType = function() {
        return $rootScope.userLoginInfo.loginType;
    };
});

labApp.controller('basicInfoController', function ($scope, $rootScope, $cookieStore){
    $scope.getUserType = function() {
        return $rootScope.userLoginInfo.loginType;
    };

    $scope.getUserName = function() {
        return $rootScope.userLoginInfo.loginUsername;
    };
});

labApp.controller('loginListController', function ($scope, $rootScope, $cookieStore){

});