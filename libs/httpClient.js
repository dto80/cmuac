'use strict';

var factoryModule = angular.module('httpClient', [
  'ngResource',
  'ngActivityIndicator'
]);

factoryModule.factory('httpRequest', ['$http', '$rootScope', function ($http, $rootScope){
  return {
        get: function(getURL, callback){
          if($rootScope.debug){
            console.log('This link: ' + mainURL + getURL);
          }
          $http({
            method: 'GET',
            url: mainURL + getURL,
            headers: {
              //'Access-Control-Request-Headers': '*'
            }
          })
          .success(function(result) {
            if($rootScope.debug){
              console.log('result: ' + JSON.stringify(result));
            }
            if(result.response_status){
              return callback(result.response_data);
            }
            else {
              if($rootScope.debug){
                console.log(result.response_message);
              }
              return callback(result.response_data);
            }
          })
          .error(function(data, status, headers, config) {
            return callback(status);
          });
        },
        post: function(postURL, sendData, callback){
            $http({
            method: 'POST',
            url: mainURL + postURL,
            headers: {
              //'Access-Control-Request-Headers': '*',
              'Content-Type':'application/json'
            },
            data: sendData
          })
            .success(function(result) {
              if(result.response_status){
                return callback(result.response_data);
              }
              else {
                if($rootScope.debug){
                  console.log(result.response_message);
                }
              }
            })
            .error(function(data, status, headers, config) {
              return callback(status);
            });
        }
    }
}]);