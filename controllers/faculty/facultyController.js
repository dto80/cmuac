'use strict';

var facultyController = angular.module('facultyControllers', [
  'ui.grid',
  'ui.grid.edit',
  'ui.grid.selection',
  'ui.bootstrap',
  'schemaForm',
  'singletonFactories',
  'facultyServices',
  'httpClient',
  'ngCookies'
]);

facultyController.config(['$routeProvider',
  function ($routeProvider) {
    $routeProvider.
    when('/faculty/organizerList', {
      templateUrl: 'views/faculty/organizerList.html',
      controller: 'organizerListController',
      resolve: {
        checkLogin: checkLogin,
        getfacultyList: getAllFaculty
      }
    })
  }]);

// controller for organization
facultyController.controller('organizerListController', function ($scope, $rootScope, $cookieStore, $window, $modal, $interval, facultyFactory, httpRequest, checkLogin, getfacultyList) {
  ////////// CHECK USER LOGIN //////////
  var userInfo = checkLogin;

  $scope.isOrganizer = true;

  //////////////////////////////
  ////////// GET DATA //////////
  //////////////////////////////
  $scope.facultyList = getfacultyList;
  $scope.currentFaculty = {};
  $scope.organizationList = {};
  $scope.selectedOrganizer = {};

  ///////////////////////////////////////////
  ////////// SETTING FACULTY TABLE //////////
  ///////////////////////////////////////////

  $scope.gridOptions = {
    enableRowSelection: true,
    enableRowHeaderSelection: false,
    enableSorting: true,
    multiSelect: false,
    modifierKeysToMultiSelect: true,
    columnDefs: [
      {name: 'รหัส', field: 'faculty_id', width:60},
      {name: 'คณะ', field: 'faculty_name'},
      {name: 'แก้ไข', filed: '', cellTemplate: 'views/faculty/dialog/editButton.html', width:80}
    ]
  };

  $scope.gridOptions.onRegisterApi = function (gridApi) {
    $scope.gridApi = gridApi;
    gridApi.selection.on.rowSelectionChanged($scope, function (row) {
      // get a new organization list
      if(row.isSelected){
        $scope.currentFaculty = row.entity;
        $scope.organizationList = row.entity.organizers;
        //$scope.organizer = $scope.organizationList[0];
      }
      else {
        $scope.organizerListOptions.data = [];
      }
    });
    gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {

    });
  }

  $scope.gridOptions.data = $scope.facultyList;

  ///////////////////////////////////////////
  ////////// SETTING FACULTY TABLE //////////
  ///////////////////////////////////////////

  $scope.organizerListOptions = {
    enableRowSelection: true,
    enableRowHeaderSelection: false,
    enableSorting: true,
    multiSelect: false,
    modifierKeysToMultiSelect: true,
    columnDefs: [
      {name: 'รหัสหน่วยงาน', field: 'organizer_id', width:80},
      {name: 'ชื่อหน่วยงาน', field: 'organizer_name'}
    ]
  };

  $scope.organizerListOptions.onRegisterApi = function (organizerGridApi) {
    $scope.organizerGridApi = organizerGridApi;
    organizerGridApi.selection.on.rowSelectionChanged($scope, function (row) {
      // get a new organization list
      if($rootScope.debug){
        console.log('selected organizer: ' + JSON.stringify(row.entity));
      }
      $scope.selectedOrganizer = row.entity;
    });
    organizerGridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {

    });
  }

  $scope.$watch('currentFaculty', function(){
    $scope.organizerListOptions.data = $scope.organizationList;
  }, true);

  /////////////////////////////////
  ////////// ADD FACULTY //////////
  /////////////////////////////////

  $scope.addFaculty = function () {
    $modal.open({
      animation: true,
      templateUrl: 'views/faculty/dialog/editModal.html',
      controller: 'RowAddCtrl',
      resolve: {
        tableData: function () {
          return $scope.gridOptions.data;
        }
      }
    });
  };

  //////////////////////////////////
  ////////// EDIT FACULTY //////////
  ////////////////////////////////// 

  $scope.openEditModal = function (grid, row) {
    $modal.open({
      animation: true,
      templateUrl: 'views/faculty/dialog/editModal.html',
      controller: 'RowEditCtrl',
      resolve: {
        grid: function () {
          return grid;
        },
        row: function () {
          return row;
        }
      }
    });
  };

  ////////////////////////////////////
  ////////// OGANIZER INDEX //////////
  ////////////////////////////////////

  var getOrganizerIndex = function (faculty, organizer) {
    for (var i = 0; i < faculty.organizers.length; i++) {
      console.log(faculty.organizers[i].organizer_id + ' == ' + organizer.organizer_id + '???');
      if (faculty.organizers[i].organizer_id == organizer.organizer_id) {
        return i;
      }
    }
    return -1;
  }

  ////////// ADD OGANIZER //////////
  $scope.addOrganizer = function () {
    $modal.open({
      animation: true,
      templateUrl: 'views/faculty/dialog/editModal.html',
      controller: 'OrganizerCtrl',
      resolve: {
        faculty: function () {
          return $scope.currentFaculty;
        },
        initialEntity: function () {
          return {};
        },
        updateOp: function () {
          return function (organizers, entity) {
            httpRequest.get('&action=organizeradd&faculty_id=' + $scope.currentFaculty.faculty_id + '&organizer_name=' + entity.organizer_name, function(res){
              if(res !== undefined){
                $rootScope.resultMessage = true; //show Success alert
                $scope.facultyList = res;
                $scope.gridOptions.data = $scope.facultyList;
                for(var i = 0; i < $scope.facultyList.length; i++){
                  if($scope.currentFaculty.faculty_id === $scope.facultyList[i].faculty_id){
                    $scope.organizerListOptions.data = $scope.facultyList[i].organizers;
                  }
                }
              }
              else {
                $rootScope.errorMessage = true; //show Error alert
              }
            });
          }
        }
      }
    });
  }

  ////////// EDIT OGANIZER //////////
  $scope.updateOrganizer = function () {
    $modal.open({
      animation: true,
      templateUrl: 'views/faculty/dialog/editModal.html',
      controller: 'OrganizerCtrl',
      resolve: {
        faculty: function () {
          return $scope.currentFaculty;
        },
        initialEntity: function () {
          return angular.copy($scope.selectedOrganizer);
        },
        updateOp: function () {
          return function (organizers, entity) {
            if($rootScope.debug){
              console.log('>>>: ' + JSON.stringify(entity));
            }
            httpRequest.get('&action=organizeredit&organizer_id=' + entity.organizer_id + '&new_organizer_name=' + entity.organizer_name, function(res){
              if(res !== undefined){
                $rootScope.resultMessage = true; //show Success alert
                $scope.facultyList = res;
                $scope.gridOptions.data = $scope.facultyList;
                //$scope.organizer = angular.extend($scope.organizer, entity);
                for(var i = 0; i < $scope.facultyList.length; i++){
                  if($scope.currentFaculty.faculty_id === $scope.facultyList[i].faculty_id){
                    $scope.organizerListOptions.data = $scope.facultyList[i].organizers;
                  }
                }
              }
              else {
                $rootScope.errorMessage = true; //show Error alert
              }
            });
          }
        }
              //row.entity = angular.extend(row.entity, $scope.entity)
      }
    });
  }

  ////////// DELETE OGANIZER //////////

  $scope.deleteOrganizer = function () {
    var entity = angular.copy($scope.organizer);
    if($rootScope.debug){
      console.log('Data: ' + JSON.stringify($scope.selectedOrganizer));
    }

    //console.log('Organizer selected index:' + $scope.getOrganizerIndex());

    httpRequest.get('&action=organizerdelete&organizer_id=' + $scope.selectedOrganizer.organizer_id, function(res){
      if(res !== undefined){
        $rootScope.resultMessage = true; //show Success alert
        $scope.facultyList = res;
        $scope.gridOptions.data = $scope.facultyList;
        for(var i = 0; i < $scope.facultyList.length; i++){
          if($scope.currentFaculty.faculty_id === $scope.facultyList[i].faculty_id){
            $scope.organizerListOptions.data = $scope.facultyList[i].organizers;
          }
        }
      }
      else {
        $rootScope.errorMessage = true; //show Error alert
      }
    });
  }
});

// for modal form
facultyController.controller('RowEditCtrl', function ($scope, $rootScope, $modalInstance, FacultySchema, grid, row, facultyFactory, httpRequest) {
  var vm = this;
  $scope.schema = FacultySchema;
  $scope.entity = row.entity;//angular.copy(row.entity);
  var oldID = $scope.entity.faculty_id;
  if($rootScope.debug){
    console.log($scope.schema);
    console.log(grid);
    console.log('oldEntity: ' + oldID + " >> " + JSON.stringify(row.entity));
  }

  $scope.form = [{
    key: 'faculty_id',
    title: 'รหัสคณะ'
  },
  {
    key: 'faculty_name',
    title: 'คณะ'
  }];
  $scope.title = "แก้ไขคณะ";

  $scope.save = function () {
    row.entity = angular.extend(row.entity, $scope.entity); //remove row

    httpRequest.get('&action=facultyedit&old_faculty_id=' + oldID + '&new_faculty_id=' + $scope.entity.faculty_id + '&new_faculty_name=' + $scope.entity.faculty_name, function(res){
      if(res !== undefined){
        $rootScope.resultMessage = true; //show Success alert
        $modalInstance.close(row.entity); // close modal
      }
      else {
        $rootScope.errorMessage = true; //show Error alert
      }
    });
  }
});

facultyController.controller('RowAddCtrl', function ($scope, $rootScope, $modalInstance, FacultySchema, tableData, facultyFactory, httpRequest) {
  var vm = this;
  $scope.title = "เพิ่มคณะ";
  $scope.schema = FacultySchema;
  $scope.entity = {};

  $scope.form = [{
    key: 'faculty_id',
    title: 'รหัสคณะ'
  },
  {
    key: 'faculty_name',
    title: 'คณะ'
  }];

  $scope.save = function () {
    if(typeof $scope.entity.faculty_id == "undefined"){
      $rootScope.errorMessage = true; //show Error alert
    }
    else if(typeof $scope.entity.faculty_name == "undefined"){
      $rootScope.errorMessage = true; //show Error alert
    }
    else{
      httpRequest.get('&action=facultyadd&faculty_id=' + $scope.entity.faculty_id + '&faculty_name=' + $scope.entity.faculty_name, function(res){
        if(res !== undefined){
          $rootScope.resultMessage = true; //show Success alert
          tableData.push($scope.entity);
          $modalInstance.close($scope.entity);
        }
        else {
          $rootScope.errorMessage = true; //show Error alert
        }
      });
    }
      
  }
});


facultyController.controller('OrganizerCtrl', function ($scope, $modalInstance, OrganizerSchema, faculty, initialEntity, updateOp) {
    var vm = this;
    $scope.schema = OrganizerSchema;
    $scope.entity = initialEntity;
    $scope.form = [{
      key: 'organizer_name',
      title: 'หน่วยงาน'
    }];
    $scope.title = "เพิ่มหน่วยงาน";
    $scope.save = function () {
      updateOp(faculty.organizers, $scope.entity);
      $modalInstance.close($scope.entity);
    }
});

facultyController.constant('FacultySchema', {
  type: 'object',
  properties: {
    faculty_id: {type: 'string', title: 'รหัสคณะ'},
    faculty_name: {type: 'string', title: 'คณะ'}
  }
});

facultyController.constant('OrganizerSchema', {
  type: 'object',
  properties: {
    organizer_name: {type: 'string', title: 'ชื่อหน่วยงาน', placeholder: 'ระบุชื่อหน่วยงาน',}
  }
});


