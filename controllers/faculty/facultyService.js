/**
 * Created by Dto on 7/7/2015.
 */
'use strict'

var facultyMockData = angular.module('facultyServices',[
    'ngResource',
    'ngActivityIndicator'
]);


var getAllFaculty = function ($q, $timeout, $http, $activityIndicator) {
    var defer = $q.defer();
    $activityIndicator.startAnimating();

    console.log("Enter prepareData");
    $http({
        method: 'GET',
        url: mainURL + '&action=facultygetall',
        headers: {
          //'Access-Control-Request-Headers': '*'
        }
      })
        .success(function(result) {
            $activityIndicator.stopAnimating();
          if(result.response_status){
            defer.resolve(result.response_data);
          }
          else {
            console.log(result.response_message);
          }
        })
        .error(function(data, status, headers, config) {
            $activityIndicator.stopAnimating();
            defer.resolve(status);
        });
    
    return defer.promise;
};

facultyMockData.factory('getFacultyData',function(){
    return [
        {
            "facultyId": "01",
            "name": "มนุษยศาสตร์",
            "organizers":[
                {"id":"01","name":"คณะ"},
                {"id":"02","name":"สโมสรนักศึกษาคณะมนุษยศาสตร์"}
            ]
        },
        {
            "facultyId": "02",
            "name": "ศึกษาศาสตร์",
            "organizers":[
                {"id":"01","name":"คณะ"},
                {"id":"02","name":"สโมสรนักศึกษาคณะศึกษาศาสตร์"}
            ]

        },
        {
            "facultyId": "03",
            "name": "สังคมศาสตร์",
            "organizers":[
                {"id":"01","name":"คณะ"},
                {"id":"02","name":"สโมสรนักศึกษาคณะสังคมศาสตร์"}
            ]

        },{
            "facultyId": "04",
            "name": "วิทยาศาสตร์",
            "organizers":[
                {"id":"01","name":"คณะ"},
                {"id":"02","name":"สโมสรนักศึกษาคณะวิทยาศาสตร์"}
            ]

        }
    ]
})