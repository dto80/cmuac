'use strict';

var studentMainController = angular.module('studentControllers', [
  'studentServices',
  'httpClient',
  'naif.base64',
  'ngCookies'
  ]);

studentMainController.config(['$routeProvider',
  function ($routeProvider){
    $routeProvider.
    /*when('/registerStudent', {
      templateUrl: 'views/Student/registerStudent.html',
      controller: 'registerStudentController'
    })*/  
  when('/studentRegisterActivity', {
    templateUrl: 'views/Student/registerActivity.html',
    controller: 'studentRegisterActivityController',
    resolve: {
      checkLogin: checkLogin,
      getAvailableActivity: getStudentAvailableActivity
    }
  })
  .when('/studentRegisterActivityStatus', {
    templateUrl: 'views/Student/registerActivityStatus.html',
    controller: 'studentRegisterActivityStatusController',
    resolve: {
      checkLogin: checkLogin,
      getStudentEnrollStatus: getStudentEnrollStatus
    }
  }) 
  .when('/studentSelectEvaluateActivity', {
    templateUrl: 'views/Student/evaluateSelectActivity.html',
    controller: 'studentSelectEvaluateActivityController',
    resolve: {
      checkLogin: checkLogin,
      getStudentEvaluateList: getStudentAvailableEvaluateList
    }
  })
  .when('/studentEvaluateActivity/:studentId/:activityId', {
    templateUrl: 'views/Student/evaluateActivity.html',
    controller: 'studentEvaluateActivityController',
    // resolve: {
    //   getEvaTemplate: getEvaluateTemplate
    // }
  }) 
}]);

studentMainController.controller('studentRegisterActivityController', function ($scope, $rootScope, $cookieStore, $window, $location, checkLogin, getAvailableActivity, httpRequest){
  $scope.selectedActivity = [];

  /////////////////////////////////
  ////////// Check Login //////////
  /////////////////////////////////
  $rootScope.userLoginInfo = checkLogin;

  ////////// GET DATA //////////
  $scope.avaliableActivityForStudent = getAvailableActivity;
  console.log('>> ' + JSON.stringify($scope.avaliableActivityForStudent));
  $scope.isClick = false;

  /////////////////////////////////////////////
  ////////// AVAILABLE ACTIVITY LIST //////////
  /////////////////////////////////////////////

  $scope.rgisterActivityListOptions = {
    enableRowSelection: true,
    enableSelectAll: true,
    selectionRowHeaderWidth: 35,
    rowHeight: 35,
    //showGridFooter:true
  };

  $scope.rgisterActivityListOptions.columnDefs = [
    { name: 'ID', field: 'activity_id', width:100},
    { name: 'Activity name', field: 'activity_name'}
  ];

  $scope.rgisterActivityListOptions.multiSelect = true;

  ////////// SET DATA FOR STUDENT TO SELECT //////////
  $scope.rgisterActivityListOptions.data = $scope.avaliableActivityForStudent;

  $scope.rgisterActivityListOptions.onRegisterApi = function(gridApi){
    //set gridApi on scope
    $scope.gridApi = gridApi;
    gridApi.selection.on.rowSelectionChanged($scope,function(row){
      var msg = 'row selected ' + JSON.stringify(row.entity);
      console.log('>> ' + row.isSelected);
      console.log(msg);
      if(row.isSelected){
        $scope.selectedActivity.push(row.entity.activity_id);
      }
      else{
        $scope.selectedActivity.splice($scope.selectedActivity.indexOf(row.entity.activity_id), 1);
      }
    });

    gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
      var msg = 'rows changed ' + rows.length;
      console.log(msg);
    });

    $scope.submitRegisterActivity = function(){
      $scope.isClick = true;
      //This need to show dialog first then submit selected activities

      var mURL = '&action=studentenroll&faculty_id=' + $rootScope.userLoginInfo.loginFacultyID + '&student_id=' + $rootScope.userLoginInfo.loginID;
      var sURL = '';

      for(var i = 0; i < $scope.selectedActivity.length; i++){
        sURL = sURL + '&activity_id[]=' + $scope.selectedActivity[i];
      }

      console.log('Check submit URL: ' + mURL+sURL);

      httpRequest.get(mURL+sURL, function(res){
        if(res != undefined){
          $rootScope.resultMessage = false; //show Success alert
          $scope.isClick = false;
          $location.path('/studentRegisterActivityStatus');
        }
        else{
          $rootScope.resultMessage = false; //show Success alert
          $rootScope.errorMessage = true; //show Error alert
        }
      });
    }
  };
});

studentMainController.controller('studentRegisterActivityStatusController', function ($scope, $rootScope, $cookieStore, $window, $location, checkLogin, getStudentEnrollStatus){
  /////////////////////////////////
  ////////// Check Login //////////
  /////////////////////////////////
  $rootScope.userLoginInfo = checkLogin;

  /////////////////////////////////////////////
  ////////// AVAILABLE ACTIVITY LIST //////////
  /////////////////////////////////////////////

  $scope.rgisterActivityListOptions = {
    enableRowSelection: false,
    enableSelectAll: false,
    //selectionRowHeaderWidth: 35,
    rowHeight: 35,
    //showGridFooter:true
  };

  $scope.rgisterActivityListOptions.columnDefs = [
    { name: 'ID', field: 'activity_id', width:100},
    { name: 'ชื่อกิจกรรม', field: 'activity_name'},
    { name: 'สถานะ', field: 'enroll_status', width:100}
    //{ name: 'หมายเหตุ', field: 'reason'}
    //{ name: 'age', displayName: 'Age (not focusable)', allowCellFocus : false },
    //{ name: 'address.city' }
  ];

  console.log(JSON.stringify(getStudentEnrollStatus));
  $scope.rgisterActivityListOptions.data = getStudentEnrollStatus;

});

studentMainController.controller('studentSelectEvaluateActivityController', function ($scope, $rootScope, $cookieStore, $window, $location, httpRequest, checkLogin, getStudentEvaluateList){
  /////////////////////////////////
  ////////// Check Login //////////
  /////////////////////////////////
  $rootScope.userLoginInfo = checkLogin;

  /////////////////////////////////////////////
  ////////// AVAILABLE ACTIVITY LIST //////////
  /////////////////////////////////////////////

  $scope.evaluateActivityListOptions = {
    enableRowSelection: false,
    enableSelectAll: false,
    rowHeight: 35,
    modifierKeysToMultiSelect: false,
  };

  $scope.evaluateActivityListOptions.multiSelect = false;

  $scope.evaluateActivityListOptions.columnDefs = [
    { name: 'ID', field: 'activity_id', width:100},
    { name: 'ชื่อกิจกรรม', field: 'activity_name'},
    { name: 'สถานะ', cellTemplate: 'views/Student/tableTemplate/evaluateButton.html', width:100}
  ];

  console.log(JSON.stringify(getStudentEvaluateList));
  $scope.evaluateActivityListOptions.data = getStudentEvaluateList;

  $scope.evaluateClick = function (grid, row) {
    console.log("evaluate activity id: " + row.entity.activity_id);
    $location.path('/studentEvaluateActivity/' + $rootScope.userLoginInfo.loginID + '/' + row.entity.activity_id);
    
  };


});

studentMainController.controller('studentEvaluateActivityController', function ($scope, $routeParams, $cookieStore, $window, $location, httpRequest){
  //studentEvaluateActivity/:studentId/:activityId
  $scope.evaType = '';
  $scope.activityName = '';

  //mockup data
  $scope.evaList = ["รายการที่ 1", "รายการที่ 2", "รายการที่ 3"];

  httpRequest.get("&action=activitygetreviewlist&student_id=" + $routeParams.studentId + "&activity_id=" + $routeParams.activityId, function(res){
    console.log('return value >>> ' + JSON.stringify(res));
    $scope.evaType = res[0].activity_review_template_review_type;
    $scope.activityName = res[0].activity_review_template_name;
  });


  $scope.submitEvaluate = function(){
    $location.path('/studentSelectEvaluateActivity');
    /*httpRequest.get("&action=activitygetreviewlist&student_id=" + $routeParams.studentId + "&activity_id=" + $routeParams.activityId, function(res){
      console.log('return value >>> ' + JSON.stringify(res));

    });*/
  }
});


/*studentMainController.controller('registerStudentController', ['$scope', '$rootScope', 'studentService', function ($scope, $rootScope, studentService){
  $scope.student = {};

  $scope.days = [];
  $scope.months = [];
  $scope.years = [];

  ////////// ADD Day //////////
  for(var i = 1; i <= 31; i++){
    $scope.days.push(i);
  }

  ////////// ADD Month //////////
  for(var i = 1; i <= 12; i++){
    $scope.months.push(i);
  }

  ////////// ADD Year //////////
  for(var i = 0; i <= 100; i++){
    //this need to get current year first and count-down by 100
    $scope.years.push(2015-i);
  }


  $scope.addStudent = function(flowFiles){
    //This need to upload to server
    //$scope.student.profile_image = $scope.student.profile_image.base64;
    console.log("Student information: " + JSON.stringify($scope.student));
  }

  $scope.getLoanPeriod = function(){
    try {
      var output = parseInt($scope.student.studyRecord.studentLoanRecord.borrowTo) -
      parseInt($scope.student.studyRecord.studentLoanRecord.borrowFrom);
    }
    catch(err){
      // do nothing
    }

    if (!isNaN(output) && output >= 0)
      return output +1;
    else
      return '-';
  }

  $scope.setLanguageSkillType = function(langauge){
    if (language == 'thai') {
      $scope.student.abilityAndSpecialSkill.thaiSkill.type = langauge;
    }else if (language == 'chinese') {
      $scope.student.abilityAndSpecialSkill.chineseSkill.type = langauge;
    }else if (langauge == 'english'){
      $scope.student.abilityAndSpecialSkill.englishSkill.type = langauge;
    }else if (langauge == 'japanese'){
      $scope.student.abilityAndSpecialSkill.japaneseSkill.type = langauge;
    }else if (langauge == 'other'){
      $scope.student.abilityAndSpecialSkill.otherLangagueSkill.type = langauge;
    }
  }

  // Default show/hide div component
  $scope.showCongentialDisease = false;
  $scope.showAllergy = false;
  $scope.showPreviousGrantStatus= false;
  $scope.showPreviousLoanStatus = false;
  $scope.showPreviousActivity = false;
  $scope.musicAbilityCheck = false;
  $scope.programmingAbilityCheck = false;
  $scope.culturalCheck = false;
  $scope.sportCheck = false;
  $scope.otherCheck = false;
  $scope.student.studyRecord = {};
  $scope.student.studyRecord.previousActivityRecords = [];
}]);*/

// directive for add the previous activity list
studentMainController.directive('addPreviousActivity',
  function() {
    return {
      restrict: 'A',
      templateUrl: 'views/Student/registerTemplate/subTemplate/previousActivity.html'
    }
  });


