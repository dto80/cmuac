'use strict'
var studentService = angular.module('studentServices',[
  'ngResource',
  'ngCookies',
  'ngActivityIndicator'
]);

studentService.factory('studentService',function($resource){
    return $resource('/student',{
        save: {
            method: 'POST'
        }
    });
});

var getStudentAvailableActivity = function ($q, $rootScope, $timeout, $http, $cookieStore, $activityIndicator) {
    var defer = $q.defer();
    $activityIndicator.startAnimating();

    console.log("Enter getStudentAvailableActivity: " + $rootScope.userLoginInfo.loginFacultyID);
    $http({
        method: 'GET',
        url: mainURL + '&action=activitygetallforenroll&faculty_id=' + $rootScope.userLoginInfo.loginFacultyID,
        headers: {
          //'Access-Control-Request-Headers': '*'
        }
      })
      .success(function(result) {
        $activityIndicator.stopAnimating();
        if(result.response_status){
          defer.resolve(result.response_data);
        }
        else {
          console.log(result.response_message);
        }
      })
      .error(function(data, status, headers, config) {
        $activityIndicator.stopAnimating();
        defer.resolve(status);
      });
    return defer.promise;
};

var getStudentEnrollStatus = function ($q, $rootScope, $timeout, $http, $cookieStore, $activityIndicator) {
    var defer = $q.defer();
    $activityIndicator.startAnimating();

    console.log("Enter getStudentAvailableActivity: " + $rootScope.userLoginInfo.loginFacultyID);
    $http({
        method: 'GET',
        url: mainURL + '&action=activitygetcurrentenrolllist&student_id=' + $rootScope.userLoginInfo.loginID,
        headers: {
          //'Access-Control-Request-Headers': '*'
        }
      })
      .success(function(result) {
        $activityIndicator.stopAnimating();
        if(result.response_status){
          defer.resolve(result.response_data);
        }
        else {
          console.log(result.response_message);
        }
      })
      .error(function(data, status, headers, config) {
        $activityIndicator.stopAnimating();
        defer.resolve(status);
      });
    return defer.promise;
};

var getStudentAvailableEvaluateList = function ($q, $rootScope, $timeout, $http, $cookieStore, $activityIndicator) {
    var defer = $q.defer();
    $activityIndicator.startAnimating();

    console.log("Enter getStudentAvailableActivity: " + $rootScope.userLoginInfo.loginFacultyID);
    console.log('URL >> ' + mainURL + '&action=activitygetcurrentenrolllist&enroll_status=approve&student_id=' + $rootScope.userLoginInfo.loginID);
    $http({
        method: 'GET',
        url: mainURL + '&action=activitygetcurrentenrolllist&enroll_status=approve&student_id=' + $rootScope.userLoginInfo.loginID,
        headers: {
          //'Access-Control-Request-Headers': '*'
        }
      })
      .success(function(result) {
        $activityIndicator.stopAnimating();
        if(result.response_status){
          defer.resolve(result.response_data);
        }
        else {
          console.log(result.response_message);
        }
      })
      .error(function(data, status, headers, config) {
        $activityIndicator.stopAnimating();
        defer.resolve(status);
      });
    return defer.promise;
};