'use strict';
//var selectedStaffID = 1;

var activityTemplateController = angular.module('activityTemplateControllers',[
  'ui.grid',
  'ui.grid.edit',
  'ui.grid.cellNav',
  'ui.bootstrap',
  'activityTemplateService',
  'httpClient',
  'singletonFactories',
  'ngCookies'
  ]);

activityTemplateController.config(['$routeProvider',
  function ($routeProvider) {
    $routeProvider.
      when('/faculty/activityTemplate', {
        templateUrl: 'views/activityTemplate/activityTemplateEdit.html',
        controller: 'AddActivityTemplate',
        resolve: {
          checkLogin: checkLogin,
          getOrganizerListData: activityTemplatePrepareData //activityTemplateService
        }
      })
}])

activityTemplateController.controller("AddActivityTemplate", function ($scope, $rootScope, $cookieStore, $window, $modal, httpRequest, checkLogin, getOrganizerListData) {
  ////////// CHECK USER LOGIN //////////
  //Check Login
  var userInfo = checkLogin;
  
  //////////
  $scope.isClick = false;
  $scope.selectedOganizerID;
  $scope.selectedTemplateID = 0;
  $scope.selectedEntityID = 0;
  $scope.facultyName = 'ชื่อคณะ จะได้มาจาก link';

  $scope.organizerList = {};
  $scope.templateList = {};
  $scope.entityList = {};
    
  //////////////////////////////////
  ////////// SETUP TABLEs //////////
  //////////////////////////////////

  $scope.templateListOption = {
    enableRowSelection: true,
    enableRowHeaderSelection: false,
    enableSorting: true,
    multiSelect: false,
    columnDefs: [
      {name: 'Template name', field: 'activity_template_name'}
    ]
  };

  $scope.templateListOption.onRegisterApi = function (templateListApi) {
    templateListApi.selection.on.rowSelectionChanged($scope, function (row) {
      if(row.isSelected){
        $scope.selectedTemplateID = row.entity;
      }
    });
    templateListApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
      
    });
  };

  $scope.entityListOption = {
    enableRowSelection: true,
    enableRowHeaderSelection: false,
    enableSorting: true,
    multiSelect: false,
    columnDefs: [
      {name: 'Entity name', field: 'activity_template_entity_name'}
    ]
  };

  $scope.entityListOption.onRegisterApi = function (entityListApi) {
    entityListApi.selection.on.rowSelectionChanged($scope, function (row) {
      if(row.isSelected){
        $scope.selectedEntityID = row.entity;
      }
    });
    entityListApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
      //console.log(row);
    });
  };

  ////////////////////////////////////
  ////////// FECTH NEW DATA //////////
  ////////////////////////////////////
  $scope.organizerList = getOrganizerListData;
  if($rootScope.debug){
    console.log('>>> ' + JSON.stringify($scope.organizerList));
  }

  $scope.$watch('selectedOganizerID', function(value){
    $scope.templateListOption.data = [];
    $scope.entityListOption.data = [];
    $scope.getTemplate();
  }, true);

  $scope.getTemplate = function(){
    httpRequest.get('&action=activitytemplategetbyorganizer&organizer_id=' + $scope.selectedOganizerID, function(response){
      if($rootScope.debug){  
        console.log('>>>: ' + response);
      }
      $scope.selectedTemplateID = {};
      $scope.templateListOption.data = response;
      $scope.templateList = response;
    });
  }

  $scope.$watch('selectedTemplateID', function(value){
    if($rootScope.debug){
      console.log("selected template id: " + $scope.selectedTemplateID.activity_template_id);
    }
    $scope.entityListOption.data = [];
    $scope.showOrganizerEntity();
  }, true);

  $scope.showOrganizerEntity = function(){
    for(var i = 0; i < $scope.templateList.length; i++){
      if($scope.templateList[i].activity_template_id == $scope.selectedTemplateID.activity_template_id){
        $scope.entityListOption.data = $scope.templateList[i].activitytemplateentities;
      }
    }
  }

  //////////////////////////////////////////////////
  ////////// SELECT TEMPLATE BUTTON EVENT //////////
  //////////////////////////////////////////////////

  ////////// ADD TEMPLATE //////////
  $scope.templateAddEvent = function () {
    if($scope.selectedOganizerID){
      var modalWindow = $modal.open({
        animation: true,
        templateUrl: 'views/activityTemplate/modalTemplate/modalAddTemplate.html',
        controller: 'addTemplateController',
        resolve: {
          data: function () {
            return {
              organizerID: $scope.selectedOganizerID
            }
          }
        }
      });

      modalWindow.result.then(function (result) {
          $scope.templateListOption.data = [];
          $scope.entityListOption.data = [];
          $scope.getTemplate();
      }, function (result) {
          
      });
    }
  };

  ////////// EDIT TEMPLATE //////////
  $scope.templateEditEvent = function () {
    if($scope.selectedOganizerID && $scope.selectedTemplateID != 0){
      var modalWindow = $modal.open({
        animation: true,
        templateUrl: 'views/activityTemplate/modalTemplate/modalEditTemplate.html',
        controller: 'editTemplateController',
        resolve: {
          data: function () {
            return {
              organizerID: $scope.selectedOganizerID,
              template: $scope.selectedTemplateID
            }
          }
        }
      });

      modalWindow.result.then(function (result) {
          $scope.templateListOption.data = [];
          $scope.getTemplate();
      }, function (result) {
          
      });
    }
  };

  ////////// DELETE TEMPLATE //////////
  $scope.templateDeleteEvent = function () {
    if($scope.selectedOganizerID  == undefined){
      if($rootScope.debug){
        console.log("Organizer is required: " + $scope.selectedOganizerID.activity_template_id);
      }
    }
    else if($scope.selectedTemplateID.activity_template_id == undefined){
      if($rootScope.debug){
        console.log("Template is required: " + $scope.selectedTemplateID.activity_template_id);
      }
      alert("Please select Template above to delete");
    }
    else{
      httpRequest.get('&action=activitytemplatedelete&organizer_id=' + $scope.selectedOganizerID + '&activity_template_id=' + $scope.selectedTemplateID.activity_template_id, function(res){
        $rootScope.resultMessage = true; //show Success alert
        $scope.templateListOption.data = [];
        $scope.entityListOption.data = [];
        $scope.getTemplate();
      });
    }
  };

  ////////////////////////////////////////////////
  ////////// SELECT ENTITY BUTTON EVENT //////////
  ////////////////////////////////////////////////

  ////////// ADD ENTITY //////////
  $scope.entityAddEvent = function () {
    if($scope.selectedOganizerID && $scope.selectedTemplateID.activity_template_id != undefined){
      var modalWindow = $modal.open({
        animation: true,
        templateUrl: 'views/activityTemplate/modalTemplate/modalEntity.html',
        controller: 'addEntityController',
        resolve: {
          data: function () {
            return {
              template: $scope.selectedTemplateID
            }
          }
        }
      });

      modalWindow.result.then(function (result) {
          $scope.templateListOption.data = [];
          $scope.entityListOption.data = [];
          $scope.getTemplate();
      }, function (result) {
          
      });
    }
  }

  ////////// EDIT ENTITY //////////
  $scope.entityEditEvent = function () {
    if($scope.selectedOganizerID && $scope.selectedTemplateID.activity_template_id != undefined){
      var modalWindow = $modal.open({
        animation: true,
        templateUrl: 'views/activityTemplate/modalTemplate/modalEntity.html',
        controller: 'editEntityController',
        resolve: {
          data: function () {
            return {
              entityID: $scope.selectedEntityID
            }
          }
        }
      });

      modalWindow.result.then(function (result) {
          $scope.templateListOption.data = [];
          $scope.entityListOption.data = [];
          $scope.getTemplate();
      }, function (result) {
          
      });
    }
  }

  ////////// DELETE ENTITY //////////
  $scope.entityDeleteEvent = function () {
    if($scope.selectedOganizerID && $scope.selectedTemplateID.activity_template_id != undefined && $scope.selectedEntityID.activity_template_entity_id != undefined){
      httpRequest.get('&action=activitytemplateentitydelete&activity_template_entity_id=' + $scope.selectedEntityID.activity_template_entity_id, function(){
        $rootScope.resultMessage = true; //show Success alert
        $scope.templateListOption.data = [];
        $scope.entityListOption.data = [];
        $scope.getTemplate();
      });
    }
    else {
      alert("Please select Entity above to delete");
    }
  }
});

////////// EVENT CONTROLLERs //////////

//////////////////////////////////
////////// ADD TEMPLATE //////////
//////////////////////////////////

activityTemplateController.controller('addTemplateController', function ($scope, $rootScope, $modalInstance, data, httpRequest){
  
  $scope.templateName = "";

  $scope.save = function(){
    if($rootScope.debug){
      console.log("Template name: " + $scope.templateName);
    }
    httpRequest.get('&action=activitytemplateadd&organizer_id=' + data.organizerID + '&activity_template_name=' + $scope.templateName, function(res){
      if(res !== undefined){
        $rootScope.resultMessage = true; //show Success alert
        $modalInstance.close('Successful');
      }
      else{
        $rootScope.errorMessage = true; //show Error alert
      }
    });
  }
});

//////////////////////////////////
////////// EDIT TEMPLATE /////////
//////////////////////////////////

activityTemplateController.controller('editTemplateController', function ($scope, $rootScope, $modalInstance, data, httpRequest){
  if($rootScope.debug){
    console.log(data.template);
  }
  $scope.templateName = data.template.activity_template_name;

  $scope.save = function(){
    if($rootScope.debug){
      console.log("Template name: " + $scope.templateName);
    }
    httpRequest.get('&action=activitytemplateedit&organizer_id=' + data.organizerID + '&activity_template_id=' + data.template.activity_template_id + '&new_activity_template_name=' + $scope.templateName, function(res){
      if(res !== undefined){
        $rootScope.resultMessage = true; //show Success alert
        $modalInstance.close('Successful');
      }
      else{
        $rootScope.errorMessage = true; //show Error alert
      }
    });
  }
});

////////////////////////////////
////////// ADD ENTITY //////////
////////////////////////////////

activityTemplateController.controller('addEntityController', function ($scope, $rootScope, $modalInstance, data, httpRequest){
  $scope.abbreviation = "";
  $scope.entityName = "";

  $scope.save = function(){
    if($rootScope.debug){
      console.log("Template name: " + $scope.entityName);
    }
    httpRequest.get('&action=activitytemplateentityadd&activity_template_id=' + data.template.activity_template_id + '&activity_template_entity_abbreviation=' + $scope.abbreviation + '&activity_template_entity_name=' + $scope.entityName, function(res){
      if($rootScope.debug){
        console.log('>>> ' + JSON.stringify(res));
      }
      //if(res !== undefined){
        $rootScope.resultMessage = true; //show Success alert
        $modalInstance.close('Successful');
      //}
      //else {
        //$rootScope.errorMessage = true; //show Error alert
      //}
      
    });
  }
});

/////////////////////////////////
////////// EDIT ENTITY //////////
/////////////////////////////////

activityTemplateController.controller('editEntityController', function ($scope, $rootScope, $modalInstance, data, httpRequest){
  $scope.abbreviation = data.entityID.activity_template_entity_abbreviation;
  $scope.entityName = data.entityID.activity_template_entity_name;

  if($rootScope.debug){
    console.log('Entity obj: ' + JSON.stringify(data.entityID));
  }

  $scope.save = function(){
    if($rootScope.debug){  
      console.log("Template name: " + $scope.templateName);
    }
    httpRequest.get('&action=activitytemplateentityedit&activity_template_entity_id=' + data.entityID.activity_template_entity_id + '&new_activity_template_entity_abbreviation=' + $scope.abbreviation + '&new_activity_template_entity_name=' + $scope.entityName, function(res){
      //if(res !== undefined){
        $rootScope.resultMessage = true; //show Success alert
        $modalInstance.close('Successful');  
      //}
      //else{
        //$rootScope.errorMessage = true; //show Error alert
      //}
    });
  }
});
