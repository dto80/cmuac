'use strict'
//var activityTemplateDetailsMockData = angular.module('activityTemplateDetailsMockData', ['ngResource']);
var activityReviewTemplateDetailsMockData = angular.module('activityReviewTemplateService', [
    'ngResource',
    'ngActivityIndicator'
]);

//////////////////////////////////////////////////
///////// GET DATA BEFORE SHOW TEMPLATE //////////
//////////////////////////////////////////////////

var activityReivewTemplatePrepareData = function ($q, $timeout, $http, $rootScope, $activityIndicator) {
    var defer = $q.defer();
    $activityIndicator.startAnimating();

    $http({
        method: 'GET',
        url: mainURL + '&action=dropdownorganizer&staff_id=' + $rootScope.userLoginInfo.loginID,
        headers: {
          //'Access-Control-Request-Headers': '*'
        }
      })
      .success(function(result) {
        $activityIndicator.stopAnimating();
        if(result.response_status){
          defer.resolve(result.response_data);
        }
        else {
          console.log(result.response_message);
        }
      })
      .error(function(data, status, headers, config) {
        $activityIndicator.stopAnimating();
          defer.resolve(status);
      });
    
    return defer.promise;
};




/*activityTemplateDetailsMockData.factory('getActivityTemplateDetails', function () {
    return [
        {
            id: '1',
            name: 'ประเภทกิจกรรม',
            header: {
                id: '1',
                name: 'ประเภทกิจกรรม',
                abbreviation: '',
                status: false,
                percent: 0.0
            },
            details: [
                {
                    id: '2',
                    name: 'กิจกรรมกีฬา หรือการส่งเสริมสุขภาพ',
                    abbreviation: '',
                    status: false,
                    percent: 0.0
                }, {
                    id: '3',
                    name: 'กิจกรรมบำเพ็ญประโยชน์ หรือรักษาสิ่งแวดล้อม',
                    abbreviation: '',
                    status: false,
                    percent: 0.0
                }, {
                    id: '4',
                    name: 'กิจกรรมวิชาการที่ส่งเสริม คุณลักษณะบัณฑิตที่พึงประสงค์',
                    abbreviation: '',
                    status: false,
                    percent: 0.0
                }, {
                    id: '5',
                    name: 'กิจกรรมส่งเสริมศิลปะ และวัฒนธรรม',
                    abbreviation: '',
                    status: false,
                    percent: 0.0
                }, {
                    id: '6',
                    name: 'กิจกรรมเสริมสร้างคุณธรรม และจริยธรรม',
                    abbreviation: '',
                    status: false,
                    percent: 0.0
                }
            ]
        },
        {
            id: '2',
            name: 'พัฒนาทักษะด้าน',
            header: {
                id: '7',
                name: 'ทักษะ',
                abbreviation: 'ชื่อย่อ',
                status: false,
                percent: 0.0
            },
            details: [
                {
                    id: '8',
                    name: 'Intelligence Quotient',
                    abbreviation: 'IQ',
                    status: false,
                    percent: 0.0
                }, {
                    id: '9',
                    name: 'Emotional Quotient',
                    abbreviation: 'EQ',
                    status: false,
                    percent: 0.0
                }, {
                    id: '10',
                    name: 'Morality Quotient',
                    abbreviation: 'MQ',
                    status: false,
                    percent: 0.0
                }, {
                    id: '11',
                    name: 'Technology Quotient',
                    abbreviation: 'TQ',
                    status: false,
                    percent: 0.0
                }, {
                    id: '12',
                    name: 'Protean-Career Quotient',
                    abbreviation: 'PQ',
                    status: false,
                    percent: 0.0
                }, {
                    id: '13',
                    name: 'Cultural Quotient',
                    abbreviation: 'CQ',
                    status: false,
                    percent: 0.0
                }, {
                    id: '14',
                    name: 'Entrepreneurial-Spirit Quotient',
                    abbreviation: 'ES-Q',
                    status: false,
                    percent: 0.0
                }
            ]
        },
        {
            id: '3',
            name: 'กรอบมาตรฐานคุณวุฒิระดับอุดมศึกษา',
            header: {
                id: '15',
                name: 'กรอบมาตรฐานคุณวุฒิระดับอุดมศึกษา',
                abbreviation: '',
                status: false,
                percent: 0.0
            },
            details: [
                {
                    id: '16',
                    name: 'คุณธรรมจริยธรรม',
                    abbreviation: '',
                    status: false,
                    percent: 0.0
                }, {
                    id: '17',
                    name: 'ความรู้',
                    abbreviation: '',
                    status: false,
                    percent: 0.0
                }, {
                    id: '18',
                    name: 'ทักษะทางปัญญา',
                    abbreviation: '',
                    status: false,
                    percent: 0.0
                }, {
                    id: '19',
                    name: 'ทักษะความสัมพันธ์ระหว่างบุคคล และความรับผิดชอบ',
                    abbreviation: '',
                    status: false,
                    percent: 0.0
                }, {
                    id: '20',
                    name: ' 	ทักษะการวิเคราะห์เชิงตัวเลข การสื่อสาร และการใช้เทคโนโลยีสารสนเทศ',
                    abbreviation: '',
                    status: false,
                    percent: 0.0
                }
            ]
        }
    ]
})*/