'use strict';

var activityReviewTemplateController = angular.module('activityReviewTemplateControllers',[
  'ui.grid',
  'ui.grid.edit',
  'ui.grid.cellNav',
  'ui.bootstrap',
  'activityReviewTemplateService',
  'httpClient',
  'singletonFactories',
  'ngCookies'
  ]);

activityReviewTemplateController.config(['$routeProvider',
  function ($routeProvider) {
    $routeProvider.
      when('/faculty/activityReviewTemplate', {
        templateUrl: 'views/activityReviewTemplate/activityReviewTemplateEdit.html',
        controller: 'addActivityReviewTemplate',
        resolve: {
          checkLogin: checkLogin,
          getOrganizerListData: activityReivewTemplatePrepareData //activityReviewTemplateService
        }
      })
}])

activityReviewTemplateController.controller("addActivityReviewTemplate", function ($scope, $rootScope, $cookieStore, $window, $modal, httpRequest, checkLogin, getOrganizerListData) {
  ////////// CHECK USER LOGIN //////////
  //Check Login
  var userInfo = checkLogin;
  
  $scope.selectedOganizerID;
  $scope.selectedReviewTemplateID = 0;
  $scope.selectedReviewEntityID = 0;
  $scope.facultyName = 'ชื่อคณะ จะได้มาจาก link';

  $scope.organizerList = {};
  $scope.templateList = {};
  $scope.entityList = {};
    
  //////////////////////////////////
  ////////// SETUP TABLEs //////////
  //////////////////////////////////

  $scope.reviewTemplateListOption = {
    enableRowSelection: true,
    enableRowHeaderSelection: false,
    enableSorting: true,
    multiSelect: false,
    columnDefs: [
      {name: 'Review Template name', field: 'activity_review_template_name'}
    ]
  };

  $scope.reviewTemplateListOption.onRegisterApi = function (templateListApi) {
    templateListApi.selection.on.rowSelectionChanged($scope, function (row) {
      if(row.isSelected){
        console.log("selected template >> " + JSON.stringify(row.entity));
        $scope.selectedReviewTemplateID = row.entity;
      }
    });
    templateListApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
      
    });
  };

  $scope.reviewEntityListOption = {
    enableRowSelection: true,
    enableRowHeaderSelection: false,
    enableSorting: true,
    multiSelect: false,
    columnDefs: [
      {name: 'Review Entity name', field: 'activity_review_template_entity_name'},
      {name: 'Review Entity type', field: 'activity_review_template_entity_review_type', width: 120}
    ]
  };

  $scope.reviewEntityListOption.onRegisterApi = function (entityListApi) {
    entityListApi.selection.on.rowSelectionChanged($scope, function (row) {
      if(row.isSelected){
        console.log("selected review entity: " + JSON.stringify(row.entity));
        $scope.selectedReviewEntityID = row.entity;
      }
    });
    entityListApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
      //console.log(row);
    });
  };

  ////////////////////////////////////
  ////////// FECTH NEW DATA //////////
  ////////////////////////////////////
  $scope.organizerList = getOrganizerListData;
  console.log('>>> ' + JSON.stringify($scope.organizerList));

  $scope.$watch('selectedOganizerID', function(value){
    $scope.reviewTemplateListOption.data = [];
    $scope.reviewEntityListOption.data = [];
    $scope.getReviewTemplate();
  }, true);

  $scope.getReviewTemplate = function(){
    httpRequest.get('&action=activityreviewtemplategetbyorganizer&organizer_id=' + $scope.selectedOganizerID, function(response){
      console.log('>>>: ' + JSON.stringify(response));
      $scope.selectedReviewTemplateID = {};
      $scope.reviewTemplateListOption.data = response;
      $scope.templateList = response;
    });
  }

  $scope.$watch('selectedReviewTemplateID', function(value){
    console.log("selected template id: " + $scope.selectedReviewTemplateID.activity_review_template_id);
    $scope.reviewEntityListOption.data = [];
    $scope.showOrganizerReivewEntity();
  }, true);

  $scope.showOrganizerReivewEntity = function(){
    for(var i = 0; i < $scope.templateList.length; i++){
      console.log($scope.templateList[i].activity_review_template_id + " == " + $scope.selectedReviewTemplateID.activity_review_template_id);
      if($scope.templateList[i].activity_review_template_id == $scope.selectedReviewTemplateID.activity_review_template_id){
        console.log(JSON.stringify($scope.templateList[i]));
        $scope.reviewEntityListOption.data = $scope.templateList[i].activityreviewtemplateentities;
      }
    }
  }

  //////////////////////////////////////////////////
  ////////// SELECT TEMPLATE BUTTON EVENT //////////
  //////////////////////////////////////////////////

  ////////// ADD TEMPLATE //////////
  $scope.reviewTemplateAddEvent = function () {
    if($scope.selectedOganizerID){
      var modalWindow = $modal.open({
        animation: true,
        templateUrl: 'views/activityReviewTemplate/modalReviewTemplate/modalAddReviewTemplate.html',
        controller: 'addReviewTemplateController',
        resolve: {
          data: function () {
            return {
              organizerID: $scope.selectedOganizerID
            }
          }
        }
      });

      modalWindow.result.then(function (result) {
          $scope.reviewTemplateListOption.data = [];
          $scope.reviewEntityListOption.data = [];
          $scope.getReviewTemplate();
      }, function (result) {
          
      });
    }
  };

  ////////// EDIT TEMPLATE //////////
  $scope.reviewTemplateEditEvent = function () {
    if($scope.selectedOganizerID && $scope.selectedReviewTemplateID != 0){
      var modalWindow = $modal.open({
        animation: true,
        templateUrl: 'views/activityReviewTemplate/modalReviewTemplate/modalEditReviewTemplate.html',
        controller: 'editReviewTemplateController',
        resolve: {
          data: function () {
            return {
              organizerID: $scope.selectedOganizerID,
              reviewTemplate: $scope.selectedReviewTemplateID
            }
          }
        }
      });

      modalWindow.result.then(function (result) {
          $scope.reviewTemplateListOption.data = [];
          $scope.getReviewTemplate();
      }, function (result) {
          
      });
    }
  };

  ////////// DELETE TEMPLATE //////////
  $scope.reviewTemplateDeleteEvent = function () {
    if($scope.selectedOganizerID  == undefined){
      console.log("Organizer is required: " + $scope.selectedOganizerID.activity_review_template_id);
    }
    else if($scope.selectedReviewTemplateID.activity_review_template_id == undefined){
      console.log("Template is required: " + $scope.selectedReviewTemplateID.activity_review_template_id);
      alert("Please select Template above to delete");
    }
    else{
      httpRequest.get('&action=activityreviewtemplatedelete&organizer_id=' + $scope.selectedOganizerID + '&activity_review_template_id=' + $scope.selectedReviewTemplateID.activity_review_template_id, function(res){
        $rootScope.resultMessage = true; //show Success alert
        $scope.reviewTemplateListOption.data = [];
        $scope.reviewEntityListOption.data = [];
        $scope.getReviewTemplate();
      });
    }
  };

  ////////////////////////////////////////////////
  ////////// SELECT ENTITY BUTTON EVENT //////////
  ////////////////////////////////////////////////

  ////////// ADD ENTITY //////////
  $scope.reviewEntityAddEvent = function () {
    if($scope.selectedOganizerID && $scope.selectedReviewTemplateID.activity_review_template_id != undefined){
      var modalWindow = $modal.open({
        animation: true,
        templateUrl: 'views/activityReviewTemplate/modalReviewTemplate/modalReviewEntity.html',
        controller: 'addReviewEntityController',
        resolve: {
          data: function () {
            return {
              reviewTemplate: $scope.selectedReviewTemplateID
            }
          }
        }
      });

      modalWindow.result.then(function (result) {
          $scope.reviewTemplateListOption.data = [];
          $scope.reviewEntityListOption.data = [];
          $scope.getReviewTemplate();
      }, function (result) {
          
      });
    }
  }

  ////////// EDIT ENTITY //////////
  $scope.reviewEntityEditEvent = function () {
    if($scope.selectedOganizerID && $scope.selectedReviewTemplateID.activity_review_template_id != undefined){
      var modalWindow = $modal.open({
        animation: true,
        templateUrl: 'views/activityReviewTemplate/modalReviewTemplate/modalReviewEntity.html',
        controller: 'editReviewEntityController',
        resolve: {
          data: function () {
            return {
              reviewEntityID: $scope.selectedReviewEntityID
            }
          }
        }
      });

      modalWindow.result.then(function (result) {
          $scope.reviewTemplateListOption.data = [];
          $scope.reviewEntityListOption.data = [];
          $scope.getReviewTemplate();
      }, function (result) {
          
      });
    }
  }

  ////////// DELETE ENTITY //////////
  $scope.reviewEntityDeleteEvent = function () {
    if($scope.selectedOganizerID != undefined && $scope.selectedReviewTemplateID.activity_review_template_id != undefined && $scope.selectedReviewEntityID.activity_review_template_entity_id != undefined){
      httpRequest.get('&action=activityreviewtemplateentitydelete&activity_review_template_entity_id=' + $scope.selectedReviewEntityID.activity_review_template_entity_id, function(res){
        $rootScope.resultMessage = true; //show Success alert
        $scope.reviewTemplateListOption.data = [];
        $scope.reviewEntityListOption.data = [];
        $scope.getReviewTemplate();
      });
    }
    else {
      alert("Please select Entity above to delete");
    }
  }
});

////////// EVENT CONTROLLERs //////////

//////////////////////////////////
////////// ADD TEMPLATE //////////
//////////////////////////////////

activityReviewTemplateController.controller('addReviewTemplateController', function ($scope, $rootScope, $modalInstance, data, httpRequest){
  
  $scope.reviewTemplateName = "";

  $scope.save = function(){
    console.log("Template name: " + $scope.reviewTemplateName);
    httpRequest.get('&action=activityreviewtemplateadd&organizer_id=' + data.organizerID + '&activity_review_template_name=' + $scope.reviewTemplateName, function(res){
      if(res !== undefined){
        $rootScope.resultMessage = true; //show Success alert
        $modalInstance.close('Successful');
      }
      else{
        $rootScope.errorMessage = true; //show Error alert
      }
    });
  }
});

//////////////////////////////////
////////// EDIT TEMPLATE /////////
//////////////////////////////////

activityReviewTemplateController.controller('editReviewTemplateController', function ($scope, $rootScope, $modalInstance, data, httpRequest){
  console.log(data.reviewTemplate);
  $scope.reviewTemplateName = data.reviewTemplate.activity_review_template_name;

  $scope.save = function(){
    console.log("Template name: " + $scope.reviewTemplateName);
    httpRequest.get('&action=activityreviewtemplateedit&organizer_id=' + data.organizerID + '&activity_review_template_id=' + data.reviewTemplate.activity_review_template_id + '&new_activity_review_template_name=' + $scope.reviewTemplateName, function(res){
      if(res !== undefined){
        $rootScope.resultMessage = true; //show Success alert
        $modalInstance.close('Successful');
      }
      else{
        $rootScope.errorMessage = true; //show Error alert
      }
    });
  }
});

////////////////////////////////
////////// ADD ENTITY //////////
////////////////////////////////

activityReviewTemplateController.controller('addReviewEntityController', function ($scope, $rootScope, $modalInstance, data, httpRequest){
  $scope.reviewEntityName = "";
  $scope.reviewEntityType = "text";

  $scope.save = function(){
    console.log("Template name: " + $scope.templateName);
    httpRequest.get('&action=activityreviewtemplateentityadd&activity_review_template_id=' + data.reviewTemplate.activity_review_template_id + '&activity_review_template_entity_name=' + $scope.reviewEntityName + '&activity_review_template_entity_review_type=' + $scope.reviewEntityType, function(res){
      $rootScope.resultMessage = true; //show Success alert
      $modalInstance.close('Successful');
    });
  }
});

/////////////////////////////////
////////// EDIT ENTITY //////////
/////////////////////////////////

activityReviewTemplateController.controller('editReviewEntityController', function ($scope, $rootScope, $modalInstance, data, httpRequest){
  $scope.reviewEntityName = data.reviewEntityID.activity_review_template_entity_name;
  $scope.reviewEntityType = data.reviewEntityID.activity_review_template_entity_review_type;

  console.log('Review Entity obj: ' + JSON.stringify(data.reviewEntityID));

  $scope.save = function(){
    console.log("Review Template name: " + $scope.templateName);
    httpRequest.get('&action=activityreviewtemplateentityedit&activity_review_template_entity_id=' + data.reviewEntityID.activity_review_template_entity_id + '&new_activity_review_template_entity_name=' + $scope.reviewEntityName + '&new_activity_review_template_entity_review_type=' + $scope.reviewEntityType, function(res){
      $rootScope.resultMessage = true; //show Success alert
      $modalInstance.close('Successful');
    });
  }
});
