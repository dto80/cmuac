'use strict';
var selectedStaffID = '1';

var activityMainController = angular.module('activityViewControllers',[
  'activityServices',
  'ngRoute',
  'ui.grid',
  'ui.grid.edit',
  'ui.grid.rowEdit',
  'ui.grid.cellNav',
  'ui.bootstrap',
  'activityServices',
  'httpClient',
  'activityTemplateService',
  'ngCookies'
]);

activityMainController.config(['$routeProvider',
  function ($routeProvider) {
    $routeProvider.
    when('/viewActivity', {
      templateUrl: 'views/activity/viewActivity.html',
      controller: 'viewActivityController',
      resolve: {
        checkLogin: checkLogin,
        getData: staffGetFaculty
      }
    })
  }]);

activityMainController.controller('viewActivityController', 
  function ($scope, $rootScope, $cookieStore, $window, $modal, httpRequest, checkLogin, getData, $location) {
    ////////// CHECK USER LOGIN //////////
    //Check Login
    var userInfo = checkLogin;
    
    $scope.viewActivity = {};
    $scope.viewActivity.activity_id = ""

    ///////////////////////////////////
    ////////// TABLE SETTING //////////
    ///////////////////////////////////
    $scope.allActivityListOptions = {
      enableRowSelection: true,
      enableRowHeaderSelection: false,
      enableSorting: true,
      multiSelect: false,
      rowHeight: 35,
      //showGridFooter:true
    };

  $scope.allActivityListOptions.columnDefs = [
    { name: 'ID', field: 'activity_id', width:100},
    { name: 'ชื่อกิจกรรม', field: 'activity_name'}
  ];

  $scope.allActivityListOptions.onRegisterApi = function (activitiesListApi) {
    $scope.activitiesListApi = activitiesListApi;
    activitiesListApi.selection.on.rowSelectionChanged($scope, function (row) {
      //console.log("123: " + JSON.stringify(row));
      if(row.isSelected){
        //if($rootScope.debug){
          console.log("selected activity id: " + JSON.stringify(row.entity.activity_id));
          $scope.viewActivity.activity_id = row.entity.activity_id;
        //}
      }
      else{
        console.log("else")
        $scope.viewActivity.activity_id = ""
      }

    });
    activitiesListApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
      //console.log(row);
    });
  }

  ////////////////////////////////////
  ////////// FECTH NEW DATA //////////
  ////////////////////////////////////

  // GET ORGANIZER DATA
  $scope.facultyList = getData;
  if($rootScope.debug){
    console.log($scope.facultyList);
  }

  $scope.$watch('viewActivity.faculty_id', function(value){
    if($scope.viewActivity.faculty_id !== undefined)
      $scope.getTemplate();
  }, true);

  $scope.getTemplate = function(){
    if($rootScope.debug){
      console.log('faculty id: ' + $scope.viewActivity.faculty_id);
    }
    httpRequest.get('&action=activitygetall&faculty_id=' + $scope.viewActivity.faculty_id, function(res){
      if(res !== undefined){
        $scope.allActivityListOptions.data = res;
      }
    });
  };


  ////////// FECTH ACTIVITY INFORMATION //////////
  //////////
  $scope.$watch('viewActivity.activity_id', function(value){
    console.log("activity id is change to : " + $scope.viewActivity.activity_id);
    if($scope.viewActivity.activity_id !== ""){
      //This will go get activity information
      httpRequest.get('&action=activitygetbyid&activity_id=' + $scope.viewActivity.activity_id, function(res) {
        console.log("activity id is change to : " + $scope.viewActivity.activity_id);
        console.log("res: " + JSON.stringify(res));
      });
    }
  }, true);




});