'use strict'

var activityService = angular.module('activityServices', [
  'ngResource',
  'ngActivityIndicator'
]);

activityService.factory('act',function($resource){
  return $resource(mainURL + '&action=activityadd', {},
  { 
    save:{
      method: "POST",
      isArray:false,
      headers:{
        //'Access-Control-Request-Headers': '*',
        'content-type':'application/json'
      }
    }
  })
});

/*activityService.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function(file){
        var fd = new FormData();
        fd.append('image1', file);
        $http.post(mainURL + 'activityadd.php' + '?apikey=cfcd208495d565ef66e7dff9f98764da&code=znrrb', fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(res){
          console.log(':)' + JSON.stringify(res));
        })
        .error(function(){
          console.log(':(');
        });
    }
}])*/

/////////////////////////////////////////
////////// ACTIVITY CONTROLLER //////////
/////////////////////////////////////////

var prepareData = function ($q, $timeout, $http, $rootScope, $activityIndicator) {
    var defer = $q.defer();
    $activityIndicator.startAnimating();

    console.log("Enter prepareData");
    $http({
        method: 'GET',
        url: mainURL + '&action=dropdownorganizer&staff_id=' + $rootScope.userLoginInfo.loginID,
        headers: {
          //'Access-Control-Request-Headers': '*'
        }
      })
        .success(function(result) {
          $activityIndicator.stopAnimating();
          if(result.response_status){
            defer.resolve(result.response_data);
          }
          else {
            console.log(result.response_message);
          }
        })
        .error(function(data, status, headers, config) {
            $activityIndicator.stopAnimating();
            defer.resolve(status);
        });
    
    return defer.promise;
};

var converstDate = function(selectDate){
  var newDate = new Date(selectDate)
  var newDateFormat = newDate.getFullYear() + '-' + (newDate.getMonth()+1) + '-' + newDate.getDate();
  return newDateFormat;
}

//////////////////////////////////////////////
////////// ACTIVITY VIEW CONTROLLER //////////
//////////////////////////////////////////////

var staffGetFaculty = function ($q, $timeout, $http, $rootScope, $activityIndicator) {
    var defer = $q.defer();
    $activityIndicator.startAnimating();

    console.log("Enter prepareData");
    $http({
        method: 'GET',
        url: mainURL + '&action=dropdownfaculty&staff_id=' + $rootScope.userLoginInfo.loginID,
        headers: {
          //'Access-Control-Request-Headers': '*'
        }
      })
      .success(function(result) {
        $activityIndicator.stopAnimating();
        if(result.response_status){
          defer.resolve(result.response_data);
        }
        else {
          console.log(result.response_message);
        }
      })
      .error(function(data, status, headers, config) {
          $activityIndicator.stopAnimating();
          defer.resolve(status);
      });
    return defer.promise;
};