'use strict';
var selectedStaffID = '1';

var activityMainController = angular.module('activityControllers',[
  'activityServices',
  'ngRoute',
  'ui.grid',
  'ui.grid.edit',
  'ui.grid.rowEdit',
  'ui.grid.cellNav',
  'ui.bootstrap',
  'activityServices',
  'httpClient',
  'activityTemplateService',
  'naif.base64',
  'ngCookies'
  ]);

activityMainController.directive('fileModel', ['$parse', function ($parse) {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      var model = $parse(attrs.fileModel);
      var modelSetter = model.assign;
      
      element.bind('change', function(){
        scope.$apply(function(){
          console.log('scope: ' + scope);
          console.dir(element[0].files[0]);
          modelSetter(scope, element[0].files[0]);
        });
      });
    }
  };
}]);

activityMainController.config(['$routeProvider',
  function ($routeProvider) {
    $routeProvider.
    when('/registerActivity', {
      templateUrl: 'views/activity/registerActivity.html',
      controller: 'registerActivityController',
      resolve: {
        checkLogin: checkLogin,
        getData: prepareData
      }
    })
  }]);

activityMainController.controller('registerActivityController', 

  function ($scope, $rootScope, $cookieStore, $window, $modal, httpRequest, checkLogin, getData, act, $location) {
    ////////// CHECK USER LOGIN //////////
    //Check Login
    var userInfo = checkLogin;
    
    console.log("get in the function");
    $scope.activity = {};
    $scope.activity.staff_id = selectedStaffID;
    $scope.activity.academic_semester = '1';
    $scope.activity.activity_type = 'main';

  //$scope.selectedTemplateID = 0;
  //$scope.facultyName = 'ชื่อคณะ';

  $scope.isSelecedOrganizer = false;
  $scope.organizerList = {};

  // Selectemplate variables
  $scope.allTemplateList = [];
  $scope.evaluateTemplates = [];

  $scope.faculties = [
  {id:"01", name:"คณะมนุษยศาสตร์", checked: false},
  {id:"02", name:"คณะศึกษาศาสตร์", checked: false},
  {id:"03", name:"คณะวิจิตรศิลป์", checked: false},
  {id:"04", name:"คณะสังคมศาสตร์", checked: false},
  {id:"05", name:"คณะวิทยาศาสตร์", checked: false},
  {id:"06", name:"คณะวิศวกรรมศาสตร์", checked: false},
  {id:"07", name:"คณะแพทยศาสตร์", checked: false},
  {id:"08", name:"คณะเกษตรศาสตร์", checked: false},
  {id:"09", name:"คณะทันตแพทยศาสตร์", checked: false},
  {id:"10", name:"คณะเภสัชศาสตร์", checked: true},
  {id:"11", name:"คณะเทคนิคการแพทย์", checked: false},
  {id:"12", name:"คณะพยาบาลศาสตร์", checked: false},
  {id:"13", name:"คณะอุตสาหกรรมเกษตร", checked: false},
  {id:"14", name:"คณะสัตวแพทยศาสตร์", checked: false},
  {id:"15", name:"คณะบริหารธุรกิจ", checked: false},
  {id:"16", name:"คณะเศรษฐศาสตร์", checked: false},
  {id:"17", name:"คณะสถาปัตยกรรมศาสตร์", checked: false},
  {id:"18", name:"คณะการสื่อสารมวลชน", checked: false},
  {id:"19", name:"คณะรัฐศาสตร์", checked: false},
  {id:"20", name:"คณะนิติศาสตร์", checked: false},
  {id:"21", name:"วิทยาลัยศิลปะ สื่อและเทคโนโลยี", checked: true}
  ];

  ////////////////////////////////////
  ////////// FECTH NEW DATA //////////
  ////////////////////////////////////

  // GET ORGANIZER DATA
  $scope.organizerList = getData;
  //$scope.selectedOganizerID = "";

  // SELECT ORGANIZER AND GET TEMPLATEs
  $scope.changeOrganizer = function(organiz){ //Need this function then it will not work
    console.log('Change organizer: ' + organiz);
    $scope.isSelecedOrganizer = true;
    //$scope.selectedOganizerID = organiz;
  }

  $scope.$watch('activity.organizer_id', function(value){
    console.log('watch');
    $scope.allTemplateList = [];
    for(var i=0; i < $scope.organizerList.length; i++) {
      if($scope.organizerList[i].organizerid == $scope.activity.organizer_id){
        $scope.activity.faculty_id = $scope.organizerList[i].faculty_id; //Get faculty ID
      }
    }

    $scope.getTemplate();
    $scope.getEvaluateTemplate();
  }, true);

  $scope.getTemplate = function(){
    console.log('Organizer ID: ' + $scope.activity.organizer_id);
    if($scope.activity.organizer_id != undefined){
      httpRequest.get('&action=activitytemplategetbyorganizer&organizer_id=' + $scope.activity.organizer_id, function(response){
        $scope.allTemplateList = response;
        console.log('Teplate list: ' + $scope.allTemplateList);
        console.log('>> ' + JSON.stringify($scope.allTemplateList));
      });
    }
  }

  $scope.getEvaluateTemplate = function(){
    console.log('Organizer ID: ' + $scope.activity.organizer_id);
    httpRequest.get('&action=activityreviewtemplategetbyorganizer&organizer_id=' + $scope.activity.organizer_id, function(response){
        console.log('#### evaluate template >>>: ' + JSON.stringify(response));
        $scope.evaluateTemplates = response;
      });
  }

  $scope.$watch('seletedTemplateList', function(value){
    console.log('Selected template id list: ' + JSON.stringify($scope.seletedTemplateList));
  }, true);

  ///////////////////////////////////////////////
  ////////// CEHCK TEMPLATE PERCENTAGE //////////
  ///////////////////////////////////////////////
  $scope.$watch('activity.activitytemplateentities', function(value){
    console.log('activitytemplateentities got something change: ' + JSON.stringify($scope.seletedTemplateList));
  }, true);

  //////////////////////////////////
  ////////// SELECT FORMs //////////
  //////////////////////////////////
  /*$scope.selectFromAllList;
  $scope.selectFromSelectedList = {};

  $scope.changeSelecTemplate = function(template){ //Need this function then it will not work
    console.log('Change selected template: ' + template);
    //$scope.selectFromAllList = template;
  }

  $scope.addTemplate = function () {
    if ($scope.selectFromAllList != null && $scope.allTemplateList.length != 0) {
      $scope.seletedTemplateList.push($scope.selectFromAllList);
      $scope.allTemplateList.splice($scope.allTemplateList.indexOf($scope.selectFromAllList), 1);
    }
    if ($scope.allTemplateList.length > 0) {
      $scope.selectFromAllList = $scope.allTemplateList[0];
    }
    if ($scope.seletedTemplateList.length > 0) {
      $scope.selectFromSelectedList = $scope.seletedTemplateList[0];
    }
  }

  $scope.addAll = function () {
    $scope.seletedTemplateList.push.apply($scope.seletedTemplateList, $scope.allTemplateList);
    $scope.allTemplateList.splice(0, $scope.allTemplateList.length);
    if ($scope.allTemplateList.length > 0) {
      $scope.selectFromAllList = $scope.allTemplateList[0];
    }
    if ($scope.seletedTemplateList.length > 0) {
      $scope.selectFromSelectedList = $scope.seletedTemplateList[0];
    }
  }

  $scope.removeTemplate = function () {
    if ($scope.selectFromSelectedList != null && $scope.seletedTemplateList.length != 0) {
      $scope.allTemplateList.push($scope.selectFromSelectedList);
      $scope.seletedTemplateList.splice($scope.seletedTemplateList.indexOf($scope.selectFromSelectedList), 1);
    }
    if ($scope.allTemplateList.length > 0) {
      $scope.selectFromAllList = $scope.allTemplateList[0];
    }
    if ($scope.seletedTemplateList.length > 0) {
      $scope.selectFromSelectedList = $scope.seletedTemplateList[0];
    }
  }

  $scope.removeAll = function () {
    $scope.allTemplateList.push.apply($scope.allTemplateList, $scope.seletedTemplateList);
    $scope.seletedTemplateList.splice(0, $scope.seletedTemplateList.length);
    if ($scope.allTemplateList.length > 0) {
      $scope.selectFromAllList = $scope.allTemplateList[0];
    }
    if ($scope.seletedTemplateList.length > 0) {
      $scope.selectFromSelectedList = $scope.seletedTemplateList[0];
    }
  };*/

  /*$scope.showOrganizerEntity = function(){
    for(var i = 0; i < $scope.templateList.length; i++){
      if($scope.templateList[i].id == $scope.selectedTemplateID.id){
        //$scope.entityListOption.data = $scope.templateList[i].activitytemplateentities;
      }
    }
  }*/

  ////////////////////////////////////////////
  ////////// GET SELECTED FACULTIES //////////
  ////////////////////////////////////////////

  /*for (i = 0; i < arr_to_be_checked; i++) {
    data[arr_to_be_checked[i]].checked = true;
  }*/
  ////////// END SELECTED FACULTIES //////////






  // set the selection
  //$scope.faculty = {};
  //$scope.facultyList = {};
        /*facultyFactory.getFaculties().$promise.then(
            function(returnValue){
                // success result
                $scope.facultyList = returnValue;
              });*/

  //$scope.activityYear = {};

  //////////////////////////////////////
  ////////// ACTIVITY DETAILs //////////
  //////////////////////////////////////

  var getSelectedYearList = function(){
    var yearList = [];
    var currentYear = new Date().getFullYear();
    $scope.activityYear = currentYear;
    for (var i = -2; i <= 2; i++){
      yearList.push((currentYear+i));
    }
    return yearList;
  }
  $scope.yearList = getSelectedYearList();

  //////////////////////////////////////////////
  /////////// ACTIVITY DATE SELECTION //////////
  //////////////////////////////////////////////

  $scope.datepickers = {
    applyStart: false,
    applyStop: false,
    activityStart: false,
    activityStop: false,
    assessmentStart: false,
    assessmentStop: false
  }
  
  $scope.formData = {};

  $scope.today = function() {
    $scope.formData.applyStart = new Date();
    $scope.formData.applyStop = new Date();
    $scope.formData.activityStart = new Date();
    $scope.formData.activityStop = new Date();
    $scope.formData.assessmentStart = new Date();
    $scope.formData.assessmentStop = new Date();
  };

  $scope.today();

  $scope.showWeeks = true;
  $scope.toggleWeeks = function () {
    $scope.showWeeks = ! $scope.showWeeks;
  };

  $scope.clear = function () {
    $scope.dt = null;
  };

  // Disable weekend selection
  $scope.disabled = function(date, mode) {
    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
  };

  $scope.toggleMin = function() {
    $scope.minDate = ( $scope.minDate ) ? null : new Date();
  };
  $scope.toggleMin();

  $scope.open = function($event, which) {
    $event.preventDefault();
    $event.stopPropagation();

    $scope.datepickers[which]= true;
  };

  $scope.dateOptions = {
    'year-format': "'yy'",
    'starting-day': 1
  };

  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate', 'yyyy-MM-dd'];
  $scope.format = $scope.formats[3];

  //////////////////////////////////////
  ////////// ADD BUTTON EVENT //////////
  //////////////////////////////////////

  $scope.save = function(){
    ////////// DEBUG //////////
    console.log("Staff ID: " + $scope.activity.staff_id);
    console.log("Organizer ID: " + $scope.activity.organizer_id);
    console.log("Activity Type ID: " + $scope.activity.activity_type);
    //##########
    $scope.activity.activity_template = $scope.allTemplateList;
    console.log("all Template List" + JSON.stringify($scope.activity.activity_template));
    //##########
    $scope.activity.faculty_list = $scope.faculties;
    //##########
    console.log("Academic Semester: " + $scope.activity.academic_semester);
    console.log("Academic Year: " + $scope.activity.academic_year);
    console.log("Activity Name: " + $scope.activity.name);
    console.log("Activity Description: " + $scope.activity.description);
    console.log("Activity Place: " + $scope.activity.place);
    console.log("Activity Hour: " + $scope.activity.no_of_hours);
    //##########
    console.log("Register start: " + converstDate($scope.activity.date_open_register));
    console.log("Register end: " + converstDate($scope.activity.date_close_register));
    console.log("Activity start: " + converstDate($scope.activity.date_start_activity));
    console.log("Activity end: " + converstDate($scope.activity.date_end_activity));
    console.log("Evaluate start: " + converstDate($scope.activity.date_start_evaluate));
    console.log("Evaluate end: " + converstDate($scope.activity.date_stop_evaluate));
    //##########
    console.log("objective: " + $scope.activity.description_objective);
    console.log("traget: " + $scope.activity.description_target);
    //##########
    console.log("Estimate Budget: " + $scope.activity.budget_estimate);
    console.log("Actual Budget: " + $scope.activity.budget_actual);
    console.log("Maximum enroll student: " + $scope.activity.enroll_maximum_student);

    $scope.activity.date_open_register = converstDate($scope.activity.date_open_register);
    $scope.activity.date_close_register = converstDate($scope.activity.date_close_register);
    $scope.activity.date_start_activity = converstDate($scope.activity.date_start_activity);
    $scope.activity.date_end_activity = converstDate($scope.activity.date_end_activity);
    $scope.activity.date_start_evaluate = converstDate($scope.activity.date_start_evaluate);
    $scope.activity.date_stop_evaluate = converstDate($scope.activity.date_stop_evaluate);
    $scope.activity.activity_check_type = 'check type';

    //Image directory
    console.log("This is image file: ");
    console.log($scope.activity.image1);

    //console.log("Type of image: " + typeof $scope.activity.image1.base64);

    if($scope.activity.image1 !== undefined && $scope.activity.image1 !== null){
      $scope.activity.image1 = "data:" + $scope.activity.image1.filetype + ";base64," + $scope.activity.image1.base64;
    }

    if($scope.activity.image2 !== undefined && $scope.activity.image2 !== null){
      $scope.activity.image1 = "data:" + $scope.activity.image2.filetype + ";base64," + $scope.activity.image2.base64;
    }

    //fileUpload.uploadFileToUrl($scope.activity.image1);

    act.save({}, $scope.activity, function(returnValue){
        if(returnValue.response_status){
          //success
          $rootScope.resultMessage = true; //show Success alert
          console.log('Success: ' + JSON.stringify(returnValue));
          //This will redirect to view activity
          $location.path('/viewActivity');
        }
        else{
          $rootScope.errorMessage = true; //show Error alert
        }
    });
  };
});


/*activityMainController.controller('selectFocusTemplateController', function ($scope, $modalInstance, getActivityTemplateDetails) {
  $scope.allList = angular.copy(getActivityTemplateDetails);
  $scope.selectList = [];
  $scope.selectFromAllList = $scope.allList[0];
  $scope.selectFromSelectedList = {};
  $scope.addTemplate = function () {
    if ($scope.selectFromAllList != null) {
      $scope.selectList.push($scope.selectFromAllList);
      $scope.allList.splice($scope.allList.indexOf($scope.selectFromAllList), 1);
    }
    if ($scope.allList.length > 0) {
      $scope.selectFromAllList = $scope.allList[0];
    }
    if ($scope.selectList.length > 0) {
      $scope.selectFromSelectedList = $scope.selectList[0];
    }
  }
  $scope.addAll = function () {
    $scope.selectList.push.apply($scope.selectList, $scope.allList);
    $scope.allList.splice(0, $scope.allList.length);
    if ($scope.allList.length > 0) {
      $scope.selectFromAllList = $scope.allList[0];
    }
    if ($scope.selectList.length > 0) {
      $scope.selectFromSelectedList = $scope.selectList[0];
    }
  }
  $scope.removeTemplate = function () {
    if ($scope.selectFromSelectedList != null) {
      $scope.allList.push($scope.selectFromSelectedList);
      $scope.selectList.splice($scope.selectList.indexOf($scope.selectFromSelectedList), 1);
    }
    if ($scope.allList.length > 0) {
      $scope.selectFromAllList = $scope.allList[0];
    }
    if ($scope.selectList.length > 0) {
      $scope.selectFromSelectedList = $scope.selectList[0];
    }
  }
  $scope.removeAll = function () {
    $scope.allList.push.apply($scope.allList, $scope.selectList);
    $scope.selectList.splice(0, $scope.selectList.length);
    if ($scope.allList.length > 0) {
      $scope.selectFromAllList = $scope.allList[0];
    }
    if ($scope.selectList.length > 0) {
      $scope.selectFromSelectedList = $scope.selectList[0];
    }
  };

  $scope.save = function () {
    //faculty.organizers.push($scope.entity);
    // updateOp(faculty.organizers,$scope.entity);
    $modalInstance.close($scope.selectList);
  };
});*/

activityMainController.directive('focusTemplate', function () {
  return {
    restrict: 'E',
    scope: {
      templateData: '=template'
    },
    templateUrl: 'views/activityTemplate/selectFocusTemplate/focusActivityTemplateInActivityPage.html'
  }
});