'use strict';

var editStaffController = angular.module('checkStudentControllers', [
  'ui.grid',
  'ui.grid.edit',
  'ui.grid.selection',
  'ui.bootstrap',
  'schemaForm',
  'singletonFactories',
  'facultyServices',
  'httpClient',
  'ngCookies',
  'angular-ladda'
]);

editStaffController.config(['$routeProvider', function ($routeProvider) {
  $routeProvider.
  when('/staffCheckStudent', {
    templateUrl: 'views/Staff/staffCheckStudent.html',
    controller: 'staffCheckStudentController',
    resolve: {
      checkLogin: checkLogin,
      getAllAvailableActivity: getAvailableActivity
    }
  })
}]);

editStaffController.controller('staffCheckStudentController', function ($scope, $cookieStore, $window, $rootScope, $modal, $q, httpRequest, $timeout, checkLogin, getAllAvailableActivity) {
  
  //////////////////////////////////////
  ////////// CHECK USER LOGIN //////////
  //////////////////////////////////////
  var userInfo = checkLogin;
  //Check Login
  /*var loginID = $cookieStore.get('loginID');
  var loginUsername = $cookieStore.get('loginUsername');
  var loginFacultyID = $cookieStore.get('loginFacultyID');
  var loginType = $cookieStore.get('loginType');
  console.log('studentController Login information: ' + userInfo.loginID + ' > ' + userInfo.loginUsername);*/

  //////////
  $scope.isClick = false; //Enable click
  $scope.activityList = getAllAvailableActivity;
  $scope.selectedActivityID = 0;
  $scope.slectedType = 'hour';
  $scope.inputHourORPercent = 0.0;
  $scope.selectStudent = [];
  $scope.reson = "";

  //////////////////////////////////////////
  ////////// ACTIVITY TABLE CONFIG /////////
  //////////////////////////////////////////

  $scope.activityListOptions = {
    enableRowSelection: true,
    enableRowHeaderSelection: false,
    enableSorting: true,
    multiSelect: false,
    columnDefs: [
      //{name: 'ID', field: 'organizerid'},
      {name: 'รหัสกิจกรรม', field: 'activity_id'},
      {name: 'ชื่อกิจกรรม', field: 'activity_name'}
    ]
  };

  $scope.activityListOptions.onRegisterApi = function (activityListApi) {
    $scope.activityListApi = activityListApi;
    activityListApi.selection.on.rowSelectionChanged($scope, function (row) {
      $scope.selectedActivityID = 0;
      $scope.studentListOptions.data = [];
      if(row.isSelected){
        if($rootScope.debug){
          console.log("##: " + JSON.stringify(row.entity));
        }
        $scope.selectedActivityID = row.entity.activity_id;
      }
    });
  }

  $scope.activityListOptions.data = $scope.activityList;

  //////////////////////////////////////////
  ////////// STUDENT TABLE CONFIG //////////
  //////////////////////////////////////////

  $scope.studentListOptions = {
    enableRowSelection: true,
    enableRowHeaderSelection: true,
    enableSorting: true,
    multiSelect: true,
    columnDefs: [
      //{name: 'ID', field: 'organizerid'},
      {name: 'รหัสนักศึกษา', field: 'student_id'},
      {name: 'ชื่อ', field: 'student_name'},
      {name: 'สถานะ', field: 'enroll_status'}
    ]
  };

  $scope.studentListOptions.onRegisterApi = function (studentListApi) {
    $scope.studentListApi = studentListApi;
    studentListApi.selection.on.rowSelectionChanged($scope, function (row) {
      //console.log("123: " + JSON.stringify(row));
      if(row.isSelected){
        if($rootScope.debug){
          console.log("##: " + JSON.stringify(row.entity));
        }
        $scope.selectStudent.push(row.entity.student_id);
        if($rootScope.debug){
          console.log($scope.selectStudent);
        }
      }
      else{
        var index = $scope.selectStudent.indexOf(row.entity.student_id);
        $scope.selectStudent.splice(index, 1);
        if($rootScope.debug){
          console.log($scope.selectStudent);
        }
      }

    });
    studentListApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
      //console.log(row);
    });
  }

  ////////// CHECK WHEN SELECT ACTIVITY //////////
  $scope.$watch('selectedActivityID', function(value){
    if($rootScope.debug){
      console.log("change: " + $scope.selectedActivityID);
    }
    if($scope.selectedActivityID != 0){
      //get all available student in activity
      httpRequest.get('&action=studentgetbyenrollment&enroll_status=approve&activity_id=' + $scope.selectedActivityID, function(res){
        if($rootScope.debug){
          console.log(JSON.stringify(res));
        }
        $scope.studentListOptions.data = res;
      })
    }
  }, true);

  /////////////////////////////////////////
  ////////// SUBMIT BUTTON EVENT //////////
  /////////////////////////////////////////
  $scope.submitStudent = function(){
    $scope.isClick = true; //Disable click again
    //Disable button event
    var submitURL = '&action=studentcheckinstaff&activity_id=' + $scope.selectedActivityID + '&hour=' + $scope.inputHourORPercent + '&hour_type=' + $scope.slectedType;

    for(var i = 0; i < $scope.selectStudent.length; i++){
      submitURL = submitURL + '&student_id[]=' + $scope.selectStudent;
    }

    submitURL = submitURL + '&notice=' + $scope.reson;
    if($rootScope.debug){
      console.log('URL: ' + submitURL);
    }

    
    httpRequest.get(submitURL, function(res){
      //Enable button event
      if($rootScope.debug){
        console.log("result: " + typeof res);
      }
      if(res !== undefined){
        $scope.isClick = false; //Enable click again
        $rootScope.resultMessage = true; //show Success alert
        if($rootScope.debug){
          console.log("submitStudent: " + JSON.stringify(res));
        }
        if(res[0].action_status){
          $scope.studentListOptions.data = [];
          $scope.reson = "";
          alert(res[0].reason);
        }
      }
      else {
        $scope.isClick = false; //Enable click again
        $rootScope.errorMessage = true; //show Error alert
      }
    })
  }

});


var getAvailableActivity = function ($q, $timeout, $http, $cookieStore, $rootScope, $activityIndicator) {
    var defer = $q.defer();
    $activityIndicator.startAnimating();

    if($rootScope.debug){
      console.log("Enter getStudentAvailableActivity: " + $rootScope.userLoginInfo.loginFacultyID);
    }
    $http({
        method: 'GET',
        url: mainURL + '&action=activitygetall&faculty_id=' + $rootScope.userLoginInfo.loginFacultyID,
        headers: {
          //'Access-Control-Request-Headers': '*'
        }
      })
      .success(function(result) {
        $activityIndicator.stopAnimating();
        if(result.response_status){
          defer.resolve(result.response_data);
        }
        else {
          if($rootScope.debug){
            console.log(result.response_message);
          }
        }
      })
      .error(function(data, status, headers, config) {
        $activityIndicator.stopAnimating();
        defer.resolve(status);
      });
    return defer.promise;
};