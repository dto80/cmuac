'use strict';

var editStaffController = angular.module('editStaffControllers', [
  'ui.grid',
  'ui.grid.edit',
  'ui.grid.selection',
  'ui.bootstrap',
  'schemaForm',
  'singletonFactories',
  'facultyServices',
  'httpClient',
  'ngCookies',
  'ngActivityIndicator',
  'angular-ladda'
]);

editStaffController.config(['$routeProvider',
  function ($routeProvider) {
    $routeProvider
    .when('/faculty/editStaff', {
      templateUrl: 'views/Staff/editStaff.html',
      controller: 'staffListController',
      resolve: {
        checkLogin: checkLogin,
        //facultyList: loadFaculties
      }
    }).
    when('/staffAllowStudent', {
      templateUrl: 'views/Staff/allowStudent.html',
      controller: 'staffAllowStudentController',
      resolve: {
        checkLogin: checkLogin,
        getStaffAvailableActivity: getStaffAvailableActivitys
      }
    })
  }]);

editStaffController.controller('staffListController', function ($scope, $cookieStore, $window, $rootScope, $modal, $q, httpRequest, $timeout, checkLogin) {
    ////////// CHECK USER LOGIN //////////
    //Check Login
    var userInfo = checkLogin;
    
    $scope.isClick = false;
    $scope.allData = [];

    $scope.selectedFaculty;
    $scope.selectedStaff = 0;

    $scope.staffGridApi;
    $scope.organizerGridApi;

    $scope.facultiesList = [];
    $scope.organizerList = [];
    $scope.staffList = [];

    $scope.selectedOrganizers = [];

    ////////////////////////////////////////
    ////////// GET FACULTIES LIST //////////
    ////////////////////////////////////////

    httpRequest.get('&action=staffgetallbyfaculty', function(res){
      $scope.allData = res;
      $scope.facultiesList = res;
    });

    $scope.$watch('selectedFaculty', function(value){
      $scope.organizerOptions.data = [];
      if($rootScope.debug){
        console.log('faculty change to: ' + value);
      }
      $scope.getAllStaff();
    }, true);

    ///////////////////////////////////////////////////////
    ////////// SHOW STAFF AFTER SELECTED FACULTY //////////
    ///////////////////////////////////////////////////////

    $scope.getAllStaff = function(){
      $scope.staffList.data = [];
      if($rootScope.debug){
        console.log(JSON.stringify($scope.allData));
      }
      for(var i = 0; i < $scope.allData.length; i++) {
        if($scope.allData[i].faculty_id == $scope.selectedFaculty){
          if($rootScope.debug){
            console.log("index: " + i);
            console.log("Hi staff: " + $scope.allData[i].staffs[0].staff_id);
          }
          $scope.staffList.data = $scope.allData[i].staffs;
        }
      }
    }

    //////////////////////////////////
    ////////// TABLE CONFIG //////////
    //////////////////////////////////

    $scope.staffList = {
      enableRowSelection: true,
      enableRowHeaderSelection: false,
      enableSorting: true,
      multiSelect: false,
      columnDefs: [
      {name: 'ID', field: 'staff_id'},
      {name: 'ชื่อ', field: 'staff_name'},
      {name: 'เบอร์โทรศัพท์', field: 'staff_tel'}
      ]
    };

    $scope.staffList.onRegisterApi = function (staffGridApi) {
      $scope.staffGridApi = staffGridApi;
      staffGridApi.selection.on.rowSelectionChanged($scope, function (row) {
        if($rootScope.debug){
          console.log(row);
        }
        if(row.isSelected){
          if($rootScope.debug){
            console.log('Selected staff ID: ' + row.entity.staff_id);
          }
          $scope.selectedStaff = row.entity.staff_id;
        }
        else {
          if($rootScope.debug){
            console.log("deselect row: " + row.entity.staff_id);
          }
          $scope.selectedStaff = 0;
        }
      });
      staffGridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
        //console.log(row);
      });
    }

    $scope.organizerOptions = {
      enableRowSelection: true,
      enableRowHeaderSelection: false,
      enableSorting: true,
      multiSelect: true,
      columnDefs: [
        //{name: 'ID', field: 'organizerid'},
        {name: 'ชื่อคณะ', field: 'faculty_name'},
        {name: 'ชื่อ', field: 'organizer_name'}
        ]
      };

      $scope.organizerOptions.onRegisterApi = function (organizerGridApi) {
        $scope.organizerGridApi = organizerGridApi;
        organizerGridApi.selection.on.rowSelectionChanged($scope, function (row) {
        //console.log("123: " + JSON.stringify(row));
        if(row.isSelected){
          if($rootScope.debug){
            console.log("##: " + JSON.stringify(row.entity));
          }
          $scope.selectedOrganizers.push(row.entity.organizer_id);
        }
        else{
          var index = $scope.selectedOrganizers.indexOf(row.entity.organizer_id);
          $scope.selectedOrganizers.splice(index, 1);
        }

      });
        organizerGridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
        //console.log(row);
      });
      }

    ////////////////////////////////////////////////////////
    ////////// SHOW ORGANIZERs AFTER SELECT STAFF //////////
    ////////////////////////////////////////////////////////

    $scope.staffOrganizers = function(){
      httpRequest.get('&action=dropdownorganizer&staff_id=' + $scope.selectedStaff, function(res){
        if($rootScope.debug){
          console.log(JSON.stringify(res));
        }
        for(var i = 0; i < res.length; i++){
          $scope.organizerOptions.data.push(res[i]);
        }

        $timeout(function() {
          for(var i = 0; i < res.length; i++){
            if(res[i].isSelect){
              if($rootScope.debug){
                console.log(i);
              }
              $scope.organizerGridApi.selection.selectRow($scope.organizerOptions.data[i]);
            }
          }
        });
      });
    }

    $scope.$watch('selectedStaff', function(value){
      $scope.selectedOrganizers = [];
      $scope.organizerOptions.data = []; // set empty data
      if($rootScope.debug){
        console.log('selected staff ID: ' + $scope.selectedStaff);
      }
      if($scope.selectedStaff != 0){
        $scope.staffOrganizers();
      }
    }, true);

    //////////////////////////////////////
    ////////// EDIT STAFF EVENT //////////
    //////////////////////////////////////

    $scope.addStaff = function(){
      if($scope.selectedStaff !== 0){
        $scope.isClick = true; //Disable button
        // SETUP URL
        var editStaffURL = '&action=staffeditorganizer&staff_id=' + $scope.selectedStaff;

        for(var i = 0;i < $scope.selectedOrganizers.length; i++) {
          editStaffURL = editStaffURL + '&organizer_id[]=' + $scope.selectedOrganizers[i];
        }
        
        httpRequest.get(editStaffURL, function(res){
          if(res !== undefined) {
            $scope.isClick = false; //Disable button
            $rootScope.resultMessage = true;
            if($rootScope.debug){
              console.log("Successful");
            }
          }
          else {
            $scope.isClick = false; //Enable click again
            $rootScope.errorMessage = true; //show Error alert
          }
        });
      }
      else {
        $scope.isClick = false; //Enable click again
        $rootScope.errorMessage = true; //show Error alert
      }
    }
  });

editStaffController.controller('staffAllowStudentController', function ($scope, $rootScope, $cookieStore, $window, $modal, $q, httpRequest, checkLogin, getStaffAvailableActivity){
  ////////// CHECK USER LOGIN //////////
    ////////// CHECK USER LOGIN //////////
    var userInfo = checkLogin;

    $scope.isClick = false;
    $scope.selectedActivity = 0;
    $scope.selectedStudent = [];

    $scope.availableEnroll = 0;
    $scope.enrollStudentCount = 0;

    /////////////////////////////////
    ////////// TABLE SETUP //////////
    /////////////////////////////////

    // ACTIVITY LIST
    $scope.activityListOption = {
      enableRowSelection: true,
      enableSelectAll: true,
      selectionRowHeaderWidth: 35,
      enableRowHeaderSelection: false,
      rowHeight: 35,
      //showGridFooter:true
    };

    $scope.activityListOption.columnDefs = [
    { name: 'ID', field: 'activity_id', width:100},
    { name: 'ชื่อกิจกรรม', field: 'activity_name'}
      //{ name: 'age', displayName: 'Age (not focusable)', allowCellFocus : false },
      //{ name: 'address.city' }
      ];

      $scope.activityListOption.multiSelect = false;

      if($rootScope.debug){
        console.log(JSON.stringify(getStaffAvailableActivity));
      }
      $scope.activityListOption.data = getStaffAvailableActivity;

      $scope.activityListOption.onRegisterApi = function(gridApi){
      //set gridApi on scope
      $scope.gridApi = gridApi;
      gridApi.selection.on.rowSelectionChanged($scope,function(row){
        if($rootScope.debug){
          console.log(">>" + row.entity);
        }
        $scope.selectedActivity = row.entity.activity_id;

        if (!row.isSelected) {
          $scope.selectedActivity = 0;
          $scope.studentListOption.data = []; 
        }
      });

      gridApi.selection.on.rowSelectionChangedBatch($scope,function(rows){
        if($rootScope.debug){
          console.log("un check activity");
        }
        $scope.selectedActivity = 0;
        //$scope.studentListOption.data = [];
      });
    };

    $scope.$watch('selectedActivity', function(value){
      $scope.selectedStudent = [];
      $scope.studentListOption.data = [];
      if($rootScope.debug){
        console.log("selected activity: " + $scope.selectedActivity);
        console.log("all staff activity: " + JSON.stringify(getStaffAvailableActivity));
        console.log("Watch Change: " + $scope.selectedActivity);
      }
      if($scope.selectedActivity !== 0){
        for(var i = 0; i < getStaffAvailableActivity.length; i++){
          if(getStaffAvailableActivity[i].activity_id == $scope.selectedActivity){
            if($rootScope.debug){
              console.log(">>>: " + JSON.stringify(getStaffAvailableActivity[i].enroll_maximum_student));
            }
            $scope.availableEnroll = parseInt(getStaffAvailableActivity[i].enroll_maximum_student);
          }
        }

        $scope.getStudentInActivity();
      }
      //go get student
    }, true);

    // ENROLL STUDENT LIST
    $scope.studentListOption = {
      enableRowSelection: true,
      enableSelectAll: true,
      selectionRowHeaderWidth: 35,
      enableRowHeaderSelection: false,
      rowHeight: 35,
    };

    $scope.studentListOption.columnDefs = [
    { name: 'หรัสนักศึกษา', field: 'student_id', width:100},
    { name: 'ชื่อ', field: 'student_name'},
    { name: 'สถานะ', field: 'enroll_status'}
    ];

    $scope.studentListOption.multiSelect = true;

    ////////// EXPORT TO PDF //////////
    //This need to redirect to another web
    //var redirectPDFURL = "http://wolvescorporation.com/cmuac/system/mod_ex/CMUActivity/o_print_studentenroll_approve.php?c=znrrb&activity_id=";
    $scope.exportPDF = function(){
      if($scope.selectedActivity !== 0){
        $window.location.href = redirectPDFURL + $scope.selectedActivity;
      }
      else {
        alert("Please, Select activity above first");
      }
    }

    $scope.getStudentInActivity = function (){
      httpRequest.get('&action=studentgetbyenrollment&activity_id=' + $scope.selectedActivity, function(res){
        if($rootScope.debug){
          console.log(JSON.stringify(res));
        }
        $scope.studentListOption.data = res;
      });
    };

    $scope.studentListOption.onRegisterApi = function(gridApi){
      //set gridApi on scope
      $scope.gridApi = gridApi;
      gridApi.selection.on.rowSelectionChanged($scope,function(row){
        var msg = 'row selected ' + row.isSelected;
        if(row.isSelected){
          //console.log(($scope.selectedStudent.length+1) + " == " + parseInt($scope.availableEnroll));
          //if(($scope.selectedStudent.length+1) <= parseInt($scope.availableEnroll)){
            $scope.selectedStudent.push(row.entity.student_id);
            $scope.enrollStudentCount = $scope.selectedStudent.length;
            if($rootScope.debug){
              console.log($scope.selectedStudent);
            }
          /*}
          else{
            alert("reach maximum student");
          }*/
        }
        else{
          $scope.selectedStudent.splice($scope.selectedStudent.indexOf(row.entity.student_id), 1);
          if($rootScope.debug){
            console.log($scope.selectedStudent);
          }
          $scope.enrollStudentCount = $scope.selectedStudent.length;
        }
      });

      $scope.submitSelectedEnrollStudent = function(status){
        $scope.isClick = true; //Disable click
        //This need to show dialog first then submit selected activities

        var mURL = '&action=studentenrollchangestatus&activity_id=' + $scope.selectedActivity;
        var sURL = '';

        for(var i = 0; i < $scope.selectedStudent.length; i++){
          sURL = sURL + '&student_id[]=' + $scope.selectedStudent[i];
        }

        var finalURL = mURL+sURL+"&status=" + status;
        if($rootScope.debug){
          console.log('Check submit URL: ' + finalURL);
        }

        httpRequest.get(finalURL, function(res){
          if(res !== undefined) {
            $scope.isClick = false; //Enable click again
            $rootScope.resultMessage = true; //show Success alert
            if($rootScope.debug){
              console.log('>>> ' + JSON.stringify(res));
            }
            $scope.selectedStudent = []; //remove all student list
            $scope.enrollStudentCount = $scope.selectedStudent.length; //count new sutdent
            $scope.getStudentInActivity(); //get new stundent list and status
          }
          else {
            $scope.isClick = false; //Enable click again
            $rootScope.errorMessage = true; //show Error alert
          }
        });
      };
    };
  });

var getStaffAvailableActivitys = function ($q, $rootScope, $timeout, $http, $cookieStore, $activityIndicator) {
  var defer = $q.defer();
  $activityIndicator.startAnimating();

  if($rootScope.debug){
    console.log("Enter getStudentAvailableActivity: " + $rootScope.userLoginInfo.loginFacultyID);
    console.log("URL: " + mainURL + '&action=activitygetallforenroll&faculty_id=' + $rootScope.userLoginInfo.loginFacultyID);
  }
  $http({
    method: 'GET',
    url: mainURL + '&action=activitygetallforenroll&faculty_id=' + $rootScope.userLoginInfo.loginFacultyID,
    headers: {
          //'Access-Control-Request-Headers': '*'
        }
      })
  .success(function(result) {
    $activityIndicator.stopAnimating();
    if(result.response_status){
      defer.resolve(result.response_data);
    }
    else {
      if($rootScope.debug){
        console.log(result.response_message);
      }
    }
  })
  .error(function(data, status, headers, config) {
    $activityIndicator.stopAnimating();
    defer.resolve(status);
  });
  return defer.promise;
};