<div class="container">
	<div class="row">
		<nav id="footernavbar" class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
				<ul class="nav navbar-nav">
					<li>
						<a class="brand" href=<?php echo "\"". Config::sysMakerSite ."\""; ?> target="_blank" ><?php echo Config::sysMaker; ?></a>
					</li>
				</ul>

				<ul class="nav navbar-nav navbar-right">
					<?php
					$data = getrusage();  
					$usertime = ($data['ru_utime.tv_sec'] +  $data['ru_utime.tv_usec'] / 1000000);  
					$systemtime = ($data['ru_stime.tv_sec'] +  $data['ru_stime.tv_usec'] / 1000000); 
					?>
					<li><a>Code licensed <i class="glyphicon glyphicon-copyright-mark"></i> under : Apache License v2.0, User:[<?php echo number_format($usertime, 3, '.', '');?>] | Server:[<?php echo number_format($systemtime, 3, '.', '');?>] </a></li>
				</ul>
			</div>
		</nav>
	</div>
</div>
</body>
</html>