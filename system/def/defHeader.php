<?php
if(count($_POST)!=0 && !headers_sent() && !isset($_POST['noget']))
{
	$pageURL = 'http';
	if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	$pageURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}

	if(stripos(strrev($pageURL), strrev('/')) === 0)
		$pageURL .= 'index.php';

	$pageURL = preg_replace('~(\?|&)submitresult=[^&]*~','',$pageURL);
	$pageURL = preg_replace('~(\?|&)debugmessage=[^&]*~','',$pageURL);

	if(!$_POST['submitresult'])
		$_POST['submitresult'] = '0';

	header("Location: " .$pageURL);

	// if it's function page
	if (strpos($pageURL,'?') !== false) 
	{
		$pieces = explode('&submitresult', $pageURL);
		$pageURL = $pieces[0].'&submitresult='.$_POST['submitresult'];
		if(isset($_POST['debugmessage']))
			$pageURL .= '&debugmessage='.$_POST['debugmessage'];
		header("Location: " .$pageURL);
		exit();
	}
	else // if it's normal page
	{
		$pieces = explode('?submitresult', $pageURL);
		$pageURL = $pieces[0].'?submitresult='.$_POST['submitresult'];
		if(isset($_POST['debugmessage']))
			$pageURL .= '&debugmessage='.$_POST['debugmessage'];
		header("Location: " .$pageURL);
		exit();
	}
}
?>

<!DOCTYPE html>
<html>
<head>
	<title><?php echo Config::sysCustomer; ?>'s Backend</title>

	<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
	<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	<meta name="robots" content="noindex">

	<?php
	// add css
	$cssArr = array(
		// bootstrap
		'bootstrap.min.css',
		'bootstrap-theme.min.css',

		// loading page
		'pace.css',

		// alertify
		'alertify.core.css',
		'alertify.bootstrap.css',

		// select
		'bootstrap-select.css',

		// tag input
		'bootstrap-tagsinput.css',
		'bootstrap-tagsinput-typeahead.css',

		// datatable
		'dataTables.bootstrap.css',

		// wysiwyg
		'bootstrap-wysihtml5.css',

		// animate
		'animate.css',

		// font awesome
		'font-awesome.min.css',

		// default
		'default.css',
		);
	foreach ($cssArr as &$value)
		echo '<link href="'.ROOT_URL.'/src/css/'.$value.'" rel="stylesheet">';

	?>
</script>
</head>
<body>
	<?php
	function showMenuBar($selected) { ?>
	<nav class="navbar navbar-default navbar-static-top" role="navigation">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-nav-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a id="logo" class="navbar-brand"><?php echo Config::sysCustomer; ?></a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="top-nav-collapse">
				<ul class="nav navbar-nav">
					<?php
					$permission = array("Guardian","Dev");
					if (in_array(User::getCurrentUserRole(), $permission)) {
						$code = 'activity'; $file = ROOT_URL.'/sys/logactivity';
						echo "<li";
						if($selected===$code)
							echo " class='active'";
						echo "><a href='". $file ."' style='padding:10px 2px;'><i class='glyphicon glyphicon-tree-conifer'></i><sup><span class='label label-info'>". Log::countActionLog() ."</span></sup></a></li>";
					}

					$permission = array("Guardian","Dev");
					if (in_array(User::getCurrentUserRole(), $permission)) {
						$code = 'sql'; $file = ROOT_URL.'/sys/logsql';
						echo "<li";
						if($selected===$code)
							echo " class='active'";
						echo "><a href='". $file ."' style='padding:10px 2px;'><i class='glyphicon glyphicon-tower'></i><sup><span class='label label-danger'>". Log::countSQLLog() ."</span></sup></a></li>";
					}

					// User
					$permission = array("Guardian");
					if (in_array(User::getCurrentUserRole(), $permission)) {
						$code = 'user'; $file = ROOT_URL.'/sys/user'; $name='User';
						echo "<li";
						if($selected===$code)
							echo " class='active'";
						echo "><a href='". $file ."'><span class='glyphicon glyphicon-user'></span> ". $name ."</a></li>";
					}

					// Module
					$permission = array("Guardian","Dev","Admin");
					if (in_array(User::getCurrentUserRole(), $permission)) {
						$funclist = Func::getFunction();
						if($funclist!=null)
						{
							$permission = array("Guardian","Dev","Admin","Mod");
							if (in_array(User::getCurrentUserRole(), $permission)) {
								$code = 'function'; $name='System';
								echo "<li";
								if($selected===$code)
									echo " class='active'";

								$func = $funclist[0];
								echo "><a href='".ROOT_URL."/". Info::$moduleFile[$func["module"]] ."/index.php?c=". $func['code'] ."'><span class='glyphicon glyphicon-bookmark'></span> ". $name ."</a></li>";
							}
						}
					}

					$permission = array("Mod");
					if (in_array(User::getCurrentUserRole(), $permission)) {
						$userpermissionlist = User::getUserPermission();

						$funclist = Func::getFunction();
						if($funclist!=null && $userpermissionlist!=null)
						{
							$gotpermission = false;

							foreach($funclist as $func) 
							{
								foreach($userpermissionlist as $userpermission) 
								{
									if($userpermission['functionid']==$func['id'] && $userpermission['userid']==User::getcurrentUserID())
										$gotpermission = true;

									if($gotpermission) break;
								}
								if($gotpermission) break;
							}

							if($gotpermission)
							{
								$code = 'function'; $name='System';
								echo "<li";
								if($selected===$code)
									echo " class='active'";

								echo "><a href='".ROOT_URL."/". Info::$moduleFile[$func["module"]] ."/index.php?c=". $func['code'] ."'><i class='glyphicon glyphicon-bookmark'></i> ". $name ."</a></li>";
							}
						}
					}
					?>
				</ul>

				<ul class="nav navbar-nav navbar-right">
					<?php
					// DEV
					$permission = array("Guardian","Dev");
					if (in_array(User::getCurrentUserRole(), $permission)) {
						$code = 'dev'; $file = ROOT_URL.'/sys/dev'; $name='DEV';
						echo "<li";
						if($selected===$code)
							echo " class='active'";
						echo "><a href='". $file ."'><span class='glyphicon glyphicon-cloud'></span> ". $name ."</a></li>";
					}

					// core
					$permission = array("Guardian");
					if (in_array(User::getCurrentUserRole(), $permission)) {
						$code = 'core'; $file = ROOT_URL.'/sys/core'; $name='Core';
						echo "<li";
						if($selected===$code)
							echo " class='active'";
						echo "><a href='". $file ."'><span class='glyphicon glyphicon-tower'></span> ". $name ."</a></li>";
					}

					$permission = array("Dev","Admin","Mod");
					if (in_array(User::getCurrentUserRole(), $permission)) {
						$code = 'userself'; $file = ROOT_URL.'/sys/userself'; $name=User::getcurrentUserName();
						echo "<li";
						if($selected===$code)
							echo " class='active'";
						echo "><a href='". $file ."'><span class='glyphicon glyphicon-user'></span> ". $name ."</a></li>";
					}
					?>
					<li><a href="<?php echo ROOT_URL; ?>/sys/login/index.php?ca=true"><i class="glyphicon glyphicon-remove"></i> Logout</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<?php } ?>

	<?php
	function showFunctionMenuBar($selected) { ?>
	<?php
	$i=0;
	$funclist = Func::getFunction();
	if(count($funclist)>1)
	{	
		?>
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<nav class="navbar navbar-default">

						<div class="navbar-header">
							<a class="navbar-brand" href="#"><i class="glyphicon glyphicon-tags"></i> &nbsp;Module List</a>
						</div>

						<ul class="nav navbar-nav">
							<?php
							$permission = array("Guardian","Dev","Admin");
							if (in_array(User::getCurrentUserRole(), $permission)) {
								foreach($funclist as $func)
								{
									if(file_exists('../../'.Info::$moduleFile[$func['module']].'/index.php'))
									{
										if($selected==$func["code"])
											echo "<li class='active'>";
										else
											echo "<li>";
										echo "<a href='../../" . Info::$moduleFile[$func["module"]] ."/index.php?c=". $func['code'] ."'>". $func["fullname"] ."</a>";
										echo "</li>";
									}
								}
							}

							$permission = array("Mod");
							if (in_array(User::getCurrentUserRole(), $permission)) {
								$userpermissionlist = User::getUserPermission();
								$funclist = Func::getFunction();

								foreach($funclist as $func)
								{
									foreach($userpermissionlist as $userpermission)
									{
										if($userpermission['functionid']==$func['id'] && $userpermission['userid']==User::getcurrentUserID())
										{
											if(file_exists('../../'.Info::$moduleFile[$func['module']]))
											{
												if($selected==$func["code"])
													echo "<li class='active'>";
												else
													echo "<li>";
												echo "<a href='../../". Info::$moduleFile[$func["module"]] ."/index.php?c=". $func['code'] ."'>". $func["fullname"] ."</a>";
												echo "</li>";
											}
										}
									}
								}

							}
							?>
						</ul>
					</nav>
				</div>
			</div>
		</div>
		<?php 
	} 
	?>
	<?php } ?>