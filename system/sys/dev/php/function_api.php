<?php
class APIManagement extends Amst
{	
	function showReturnJson($structurelist)
	{
		?>
		<span style="margin-left:0px;">{</span><br />
		<span style="margin-left:20px;"></span>"response_message": "[message]",<br />
		<span style="margin-left:20px;"></span>"response_status": [true||false],<br />
		<span style="margin-left:20px;"></span>"response_rows": [num rows],<br />
		<span style="margin-left:20px;"></span>"response_data": [<br />
		<span style="margin-left:40px;"></span>{<br />
		<?php
		foreach($structurelist as $structure) 
			echo '<span style="margin-left:60px;"></span>"'.$structure['Field'].'": "[value]",<br />';
		?>
		<span style="margin-left:40px;"></span>},<br />
		<span style="margin-left:20px;"></span>]<br />
		<span style="margin-left:0px;">}<br />
		<?php
	}	

	function showReturnJsonWArray($structurelist)
	{
		?>
		<span style="margin-left:0px;">{</span><br />
		<span style="margin-left:20px;"></span>"response_message": "[message]",<br />
		<span style="margin-left:20px;"></span>"response_status": [true||false],<br />
		<span style="margin-left:20px;"></span>"response_rows": [num rows],<br />
		<span style="margin-left:20px;"></span>"response_data": [<br />
		<span style="margin-left:40px;"></span>{<br />
		<?php
		foreach($structurelist as $structure) 
			echo '<span style="margin-left:60px;"></span>"'.$structure['Field'].'": "[value]",<br />';
		?>
		<span style="margin-left:40px;"></span>},<br />
		<span style="margin-left:40px;"></span>{},..{} * array of data<br />
		<span style="margin-left:20px;"></span>]<br />
		<span style="margin-left:0px;">}<br />
		<?php
	}	

	function showReturnJsonNoData()
	{
		?>
		<span style="margin-left:0px;">{</span><br />
		<span style="margin-left:20px;"></span>"response_message": "[message]",<br />
		<span style="margin-left:20px;"></span>"response_status": [true||false],<br />
		<span style="margin-left:0px;">}<br />
		<?php
	}	

	function showReturnJsonTextDescription($text)
	{
		?>
		<h4 style="margin-left:0px;"><?php echo $text; ?></h4>
		<?php
	}	

	function showReturnJsonCustomData($customdata)
	{
		?>
		<span style="margin-left:0px;">{</span><br />
		<span style="margin-left:20px;"></span>"response_message": "[message]",<br />
		<span style="margin-left:20px;"></span>"response_status": [true||false],<br />
		<span style="margin-left:20px;"></span>"response_rows": [num rows],<br />
		<span style="margin-left:20px;"></span>"response_data": [<br />
		<span style="margin-left:40px;"></span>{<br />
		<?php
		foreach($customdata as $key => $value)
			echo '<span style="margin-left:60px;"></span>"'.$key.'": "['.$value.']",<br />';
		?>
		<span style="margin-left:40px;"></span>},<br />
		<span style="margin-left:0px;">}<br />
		<?php
	}	
}
?>