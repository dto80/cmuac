<?php 
include '../../def/defImport.php';
include '../../def/defCheckUser.php'; checkUser('medium');

include 'php/function_api.php';

$api = new APIManagement();

$functionlist = Func::getFunction();

if(isset($_GET['selectid']))
	$selectfunc = Func::getFunctionByID($_GET['selectid']);

$currentuser = User::getUserByID(User::getCurrentUserID());
?>

<?php include '../../def/defHeader.php'; showMenuBar("dev"); ?>

<div class="container">
	<div class="row">
		<div class="col-sm-3">
			<legend>Function</legend>
			<table class='table table-hover table-condensed table-bordered'>
				<tr>
					<th class="thincell">Code</th>
					<th>Module</th>
					<th class="thincell">&nbsp;</th>
				</tr>

				<?php
				foreach ($functionlist as $function) 
				{
					echo "<tr>";

					echo "<td><b>".$function['code']."</b></td>";
					echo "<td>".$function['fullname']."</td>";

					if((isset($_GET['selectid']) ? $_GET['selectid'] : null)==$function['id'])
						echo "<td><a class='btn btn-xs btn-primary' href='index.php?c=".$function['code']."&selectid=".$function['id']."'><i class='glyphicon glyphicon-chevron-right'></i></a></td>";
					else
						echo "<td><a class='btn btn-xs btn-default' href='index.php?c=".$function['code']."&selectid=".$function['id']."'><i class='glyphicon glyphicon-chevron-right'></i></a></td>";

					echo "</tr>";
				}
				?>
			</table>
		</div>

		<?php
		if(isset($_GET['selectid']))
		{
			$url = ROOT_URL;
			?>
			<div class="col-sm-9">
				<legend>JSON API</legend>

				<b>MAIN LINK</b> <i class="glyphicon glyphglyphicon glyphicon-link"></i> <br /><b class="text-success"><?php echo $url.'/api/json/main.php?apikey='.$currentuser['apikey'].'&code='.$selectfunc['code']; ?></b><br />

				<hr />

				<?php
				if(file_exists('../../'.Info::$moduleFile[$selectfunc['module']].'/required/info_api.php'))
				{
					$modulename = $selectfunc['module'];
					include('../../'.Info::$moduleFile[$selectfunc['module']].'/required/info_api.php');
					?>

					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<?php
						foreach($apiinfo[$selectfunc['module']] as $topic => $data)
						{
							if($topic == 'PINONTOP')
							{
								foreach($data as $key => $value)
								{
									?>
									<div class="row">
										<div class="col-sm-7">
											<p style="font-size:20px;"><b><?php echo $key; ?></b></p>
											<p><b>Description : </b><?php echo $value['description']; ?></p>
											<h5>PARAMETER <i class="glyphicon glyphglyphicon glyphicon-arrow-down"></i></h5>
											<table class='table table-striped table-hover table-condensed table-bordered'>
												<tr>
													<th class="thincell">Name</th>
													<th>memo</th>
												</tr>
												<?php
												foreach($value['required'] as $requiredkey => $requiredvalue)
												{
													echo '<tr>';
													echo '<td>'.$requiredkey.'</td>';
													echo '<td>'.$requiredvalue.'</td>';
													echo '</tr>';
												}
												?>
											</table>
										</div>

										<div class="col-sm-5">
											<h3 class="text-left"><i class="glyphicon glyphglyphicon glyphglyphicon glyphicon-chevron-down"></i> return json</h3>
											<?php
											if($value['return']=='none')
												echo $api->showReturnJsonNoData(); 
											else if($value['return']=='text')
												echo $api->showReturnJsonTextDescription($value['returndata']); 
											else if($value['return']=='custom') 
												echo $api->showReturnJsonCustomData($value['returndata']); 
											else
											{
												$structurelist = Amst::query("SHOW COLUMNS FROM ".Info::moduleTablePrefix.$selectfunc['code'].'_'.$value['returntable']);
												echo $api->showReturnJson($structurelist); 
											}
											?>
										</div>
									</div>

									<hr />
									<?php
								}
							}
						}
						?>
					</div>
					
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<?php
						$num = 1;
						foreach($apiinfo[$selectfunc['module']] as $topic => $data)
						{
							if($topic != 'PINONTOP')
							{
								?>

								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="collapsed" data-toggle="collapse" href="#tab<?php echo $num; ?>"  style="font-size:25px;">
												<?php echo $topic; ?>
											</a>
										</h4>
									</div>
									<div id="tab<?php echo $num; ?>" class="panel-collapse collapse">
										<div class="panel-body">
											<?php
											foreach($data as $key => $value)
											{
												?>
												<div class="row">
													<div class="col-sm-7">
														<p style="font-size:20px;"><b><?php echo $key; ?></b></p>
														<p><b>Description : </b><?php echo $value['description']; ?></p>
														<h5>PARAMETER <i class="glyphicon glyphglyphicon glyphicon-arrow-down"></i></h5>
														<table class='table table-striped table-hover table-condensed table-bordered'>
															<tr>
																<th class="thincell">Name</th>
																<th>memo</th>
															</tr>
															<?php
															foreach($value['required'] as $requiredkey => $requiredvalue)
															{
																echo '<tr>';
																echo '<td>'.$requiredkey.'</td>';
																echo '<td>'.$requiredvalue.'</td>';
																echo '</tr>';
															}
															?>
														</table>
													</div>

													<div class="col-sm-5">
														<h3 class="text-left"><i class="glyphicon glyphglyphicon glyphglyphicon glyphicon-chevron-down"></i> return json</h3>
														<?php
														if($value['return']=='none')
															echo $api->showReturnJsonNoData(); 
														else if($value['return']=='text')
															echo $api->showReturnJsonTextDescription($value['returndata']); 
														else if($value['return']=='custom') 
															echo $api->showReturnJsonCustomData($value['returndata']); 
														else
														{
															$structurelist = Amst::query("SHOW COLUMNS FROM ".Info::moduleTablePrefix.$selectfunc['code'].'_'.$value['returntable']);
															echo $api->showReturnJson($structurelist); 
														}
														?>
													</div>
												</div>

												<hr />
												<?php
											}
											?>
										</div>
									</div>
								</div>
								<?php
							}
							$num++;
						}
						?>
					</div>
					<?php
				}
				?>
			</div>
			<?php
		}
		?>
	</div>
</div>

<?php include '../../def/defJS.php'; ?>
<script type="text/javascript">
$(document).ready(function()
{

});
</script>

<?php include '../../def/defFooter.php'; ?>