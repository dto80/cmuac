<?php
include '../../def/defImport.php';
include '../../def/defCheckUser.php'; checkUser('medium');

if(isset($_POST['remove']))
{
	$where = array('id' => $_POST['editedid']);
	$_POST['submitresult'] = Amst::delete($_GET['c'].'_user',$where);

	$where = array('user_id' => $_POST['editedid']);
	$_POST['submitresult'] = Amst::delete($_GET['c'].'_',$where);

	if($_POST['submitresult'])
		header('Location: index.php?c='.$_GET['c']);
}

?>

<?php include '../../def/defHeader.php'; showMenuBar("user"); ?>

<div class="container content">

	<div class="row">
		<div class="col-sm-12 text-center">
			<nav>
				<ul class="pagination">
					<li><a <? echo 'href="index.php"'; ?>>User</a></li>
					<li class="active"><a <? echo 'href="o_minion.php"'; ?>>Minion</a></li>
				</ul>
			</nav>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-5">
			<legend>Minion List</legend>

			<table class="table table-striped table-hover table-condensed table-bordered table-data">
				<thead>
					<tr>
						<th class="thincell">Type</th>
						<th>Info</th>
						<th class="thincell">&nbsp;</th>
					</tr>
				</thead>

				<tbody>
					<?php
					$_GET['minion'] = (isset($_GET['minion']) ? $_GET['minion'] : null);

					$where = array(
						'ORDER' => 'name ASC'
						);
					$minionlist = Minion::getMinionByList($where);

					foreach($minionlist as $minion)
					{
						echo '<td><b>'.$minion['usertype'].'</b></td>';

						echo '<td>';
						echo '<b>'.$minion['name'].' '.$minion['surname'].'</b>';
						if($minion['email']!='')
							echo '<br />'.$minion['email'];
						echo '</td>';

						echo '<td>';
						if($minion['id']==$_GET['minion'])
							echo "<a class='btn btn-xs btn-primary' href='o_minion.php?c=".$_GET['c']."&minion=".$minion['id']."'><i class='glyphicon glyphicon-chevron-right'></i></a>";
						else
							echo "<a class='btn btn-xs btn-default' href='o_minion.php?c=".$_GET['c']."&minion=".$minion['id']."'><i class='glyphicon glyphicon-chevron-right'></i></a>";
						echo '</td>';

						echo '</tr>';
					}
					?>
				</tbody>
			</table>
		</div>

		<div class="col-sm-3">
			<?php
			if(isset($_GET['minion']))
			{
				$selectminion = Amst::getMinionByID($_GET['minion']);
				if($selectminion)
				{
					?>
					<legend>User Info</legend>
					<div class="row">
						<div class="col-xs-4 col-xs-offset-4">
							<img src="<?php echo $selectminion['imageurl']; ?>" width="100%" class="img-responsive img-circle">
						</div>
					</div>

					<br />

					<label>Information</label>
					<table class="table table-bordered table-condensed">
						<tr>
							<th>User type</th>
							<td class="thincell"><?php echo $selectminion['usertype'];?></td>
						</tr>

						<tr>
							<th>Name</th>
							<td><?php echo $selectminion['name'].' '.$selectminion['surname']; ?></td>
						</tr>
						<tr>
							<th>Gender</th>
							<td><?php echo $selectminion['gender']; ?></td>
						</tr>
						<tr>
							<th>วันเกิด</th>
							<td nowrap><?php echo $selectminion['birth_day'].'/'.$selectminion['birth_month'].'/'.$selectminion['birth_year']; ?></td>
						</tr>
						<tr>
							<th>ที่อยู่</th>
							<td><?php echo $selectminion['address']; ?></td>
						</tr>
						<tr>
							<th>เบอร์โทร</th>
							<td><?php echo $selectminion['tel']; ?></td>
						</tr>
						<tr>
							<th>อีเมลล์</th>
							<td><?php echo $selectminion['email']; ?></td>
						</tr>	
					</table>

					<hr />

					<form action="" method="POST">
						<input type="hidden" name="editedid" value="<?php echo $selectminion['id']; ?>">
						<button name="remove" type="submit" class="btn btn-block btn-danger" value="remove">REMOVE</button>
					</form>
					<?php
				}
			}
			?>
		</div>

		<div class="col-sm-4">
			<?php
			if(isset($_GET['minion']))
			{
				$loglist = Minion::getMinionLogByID($_GET['minion']);

				?>
				<legend>LOG</legend>
				<table class="table table-bordered table-condensed">
					<tr>
						<th class="thincell">#</th>
						<th>เหตุการณ์</th>
						<th class="thincell">วันที่</th>
					</tr>

					<?php
					$i = 1;
					foreach ($loglist as $log) 
					{
						echo '<tr>';

						echo '<td>'.($i++).'</td>';
						echo '<td>'.$log['topicname'].'</td>';
						echo '<td>'.Amst::formatDate($log["insertdatetime"]).'</td>';

						echo '</tr>';
					}
					?>
				</table>
				<?php
			}
			?>
		</div>
	</div>
</div>

<?php include '../../def/defJS.php'; ?>
<script type="text/javascript">
$(document).ready(function(){

});

</script>

<?php include '../../def/defFooter.php'; ?>