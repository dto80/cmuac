<?php
include '../../def/defImport.php';
include '../../def/defCheckUser.php'; checkUser('low');

if(isset($_POST['addstaff']))
{
	$data = array(
		'faculty_id' => $_GET['faculty'],
		'email' => $_POST['email'],
		'name' => $_POST['name'],
		'tel' => $_POST['tel'],
		'hash' => md5($_POST['email'])
		);
	$_POST['submitresult'] = Amst::insert($_GET['c'].'_staff',$data,false);
}

if(isset($_POST['addstudent']))
{
	$data = array(
		'faculty_id' => $_GET['faculty'],
		'student_id' => $_POST['student_id'],
		'name' => $_POST['name'],
		'hash' => md5($_POST['student_id'])
		);
	$_POST['submitresult'] = Amst::insert($_GET['c'].'_student',$data,false);
}

?>

<?php 
include '../../def/defHeader.php'; showMenuBar('function'); showFunctionMenuBar($_GET['c']); 
include 'def/defHeader.php'; showMenu('faculty');
?>

<div class="container" style="min-height:400px;">
	<div class="row">
		<div class="col-sm-3">
			<legend>FACULTY</legend>

			<table class="table table-bordered table-condensed">
				<tr>
					<th class="thincell text-right">code</th>
					<th>name</th>
					<th class="thincell">&nbsp;</th>
				</tr>

				<?php
				$_GET['faculty'] = (isset($_GET['faculty']) ? $_GET['faculty'] : null);

				$where = array('ORDER' => 'name ASC');
				$facultylist = Amst::select($_GET['c'].'_faculty','*',$where);
				foreach ($facultylist as $faculty) 
				{
					echo '<tr>';

					echo '<td class="text-right">'.$faculty['id'].'</td>';
					echo '<td>'.$faculty['name'].'</td>';

					$where = array(
						'faculty_id' => $faculty['id'],
						);
					$staffcount = Amst::count($_GET['c'].'_staff',$where);

					echo '<td>';
					if($faculty['id']==$_GET['faculty'])
						echo "<a class='btn btn-xs btn-primary' href='o_faculty.php?c=".$_GET['c']."&faculty=".$faculty['id']."'>".$staffcount."</a>";
					else
						echo "<a class='btn btn-xs btn-default' href='o_faculty.php?c=".$_GET['c']."&faculty=".$faculty['id']."'>".$staffcount."</a>";
					echo '</td>';

					echo '</tr>';
				}
				?>
			</table>
		</div>

		<div class="col-sm-5">
			<?php
			if($_GET['faculty'])
			{
				?>
				<legend>STAFF</legend>

				<a class='btn btn-xs btn-success' data-toggle='addstaff'><i class='glyphicon glyphicon-plus'></i> เพิ่ม STAFF</a>
				<br />

				<table class="table table-bordered table-condensed">
					<tr>
						<th class="thincell">#</th>
						<th class="thincell">email</th>
						<th>name</th>
						<th class="thincell">tel</th>
						<th class="thincell">&nbsp;</th>
						<th class="thincell">&nbsp;</th>
					</tr>

					<?php
					$where = array(
						'faculty_id' => $_GET['faculty'],
						'ORDER' => 'name ASC'
						);
					$stafflist = Amst::select($_GET['c'].'_staff','*',$where);

					$_GET['staff'] = (isset($_GET['staff']) ? $_GET['staff'] : null);
					foreach ($stafflist as $staff) 
					{
						echo '<tr>';

						echo '<td nowrap>'.$staff['id'].'</td>';
						echo '<td nowrap>'.$staff['email'].'</td>';
						echo '<td nowrap>'.$staff['name'].'</td>';
						echo '<td nowrap>'.$staff['tel'].'</td>';

						$where = array(
							'staff_id' => $staff['id'],
							);
						$organizerstaffcount = Amst::count($_GET['c'].'_staff_organizer',$where);

						echo '<td>';
						echo "<a class='btn btn-xs btn-primary' target='_blank' href='http://www.wolvescorporation.com/cmuac/#/login/".$staff['hash']."/staff'>LOGIN</a>";
						echo '</td>';

						echo '<td>';
						if($staff['id']==$_GET['staff'])
							echo "<a class='btn btn-xs btn-primary' href='o_faculty.php?c=".$_GET['c']."&faculty=".$_GET['faculty']."&staff=".$staff['id']."'>".$organizerstaffcount."</a>";
						else
							echo "<a class='btn btn-xs btn-default' href='o_faculty.php?c=".$_GET['c']."&faculty=".$_GET['faculty']."&staff=".$staff['id']."'>".$organizerstaffcount."</a>";
						echo '</td>';

						echo '</tr>';
					}
					?>
				</table>

				<hr />

				<legend>STUDENT</legend>

				<a class='btn btn-xs btn-success' data-toggle='addstudent'><i class='glyphicon glyphicon-plus'></i> เพิ่ม STUDENT</a>
				<br />

				<table class="table table-bordered table-condensed">
					<tr>
						<th class="thincell">#</th>
						<th class="thincell">student_id</th>
						<th>name</th>
						<th class="thincell">&nbsp;</th>
					</tr>

					<?php
					$where = array(
						'faculty_id' => $_GET['faculty'],
						'ORDER' => 'name ASC'
						);
					$studentlist = Amst::select($_GET['c'].'_student','*',$where);

					foreach ($studentlist as $student) 
					{
						echo '<tr>';

						echo '<td nowrap>'.$student['id'].'</td>';
						echo '<td nowrap>'.$student['student_id'].'</td>';
						echo '<td nowrap>'.$student['name'].'</td>';

						echo '<td>';
						echo "<a class='btn btn-xs btn-primary' target='_blank' href='http://www.wolvescorporation.com/cmuac/#/login/".$student['hash']."/student'>LOGIN</a>";
						echo '</td>';

						echo '</tr>';
					}
					?>
				</table>
				<?php
			}
			?>
		</div>

		<div class="col-sm-4">
			<?php
			if($_GET['faculty'] && $_GET['staff']==null)
			{
				?>
				<legend>ORGANIZER</legend>

				<table class="table table-bordered table-condensed">
					<tr>
						<th>name</th>
					</tr>

					<?php
					$where = array(
						'faculty_id' => $_GET['faculty'],
						'ORDER' => 'name ASC'
						);
					$organizerlist = Amst::select($_GET['c'].'_organizer','*',$where);
					foreach ($organizerlist as $organizer) 
					{
						echo '<tr>';

						echo '<td>'.$organizer['name'].'</td>';

						echo '</tr>';
					}
					?>
				</table>
				<?php
			}
			else if($_GET['faculty'])
			{
				?>
				<legend>STAFF'S ORGANIZER</legend>

				<table class="table table-bordered table-condensed">
					<tr>
						<th class="thincell">Faculty</th>
						<th>Organizer</th>
					</tr>

					<?php
					$where = array('ORDER' => 'name ASC');
					$facultylist = Amst::select($_GET['c'].'_faculty','*',$where);
					foreach ($facultylist as $faculty) 
					{
						$where = array(
							'faculty_id' => $faculty['id'],
							'ORDER' => 'name ASC'
							);
						$organizerlist = Amst::select($_GET['c'].'_organizer','*',$where);
						foreach ($organizerlist as $organizer) 
						{
							$where = array(
								'AND' => array(
									'staff_id' => $_GET['staff'],
									'organizer_id' => $organizer['id']
									)
								);
							if(Amst::has($_GET['c'].'_staff_organizer',$where))
							{
								echo '<tr>';

								echo '<td>'.$faculty['name'].'</td>';
								echo '<td>'.$organizer['name'].'</td>';

								echo '</tr>';
							}
						}	
					}
					?>
				</table>
				<?php
			}
			?>
		</div>
	</div>
</div>

<form class="form-horizontal" method="POST" action="">
	<div id="addStaffModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h3 class="modal-title">เพิ่ม Staff</h3>
				</div>
				<div class="modal-body">
					<div class="control-group">
						<label class="control-label">อีเมลล์</label>
						<input type="email" name="email" class="form-control" placeholder="email"/>
					</div>
					<div class="control-group">
						<label class="control-label">ชื่อ</label>
						<input type="text" name="name" class="form-control" placeholder="name"/>
					</div>
					<div class="control-group">
						<label class="control-label">เบอร์โทร</label>
						<input type="text" name="tel" class="form-control" placeholder="tel"/>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" name="addstaff" value="1">เพิ่ม</button>
					<button class="btn" data-dismiss="modal" aria-hidden="true">ปิด</button>
				</div>
			</div>
		</div>
	</div>
</form>

<form class="form-horizontal" method="POST" action="">
	<div id="addStudentModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h3 class="modal-title">เพิ่ม Student</h3>
				</div>
				<div class="modal-body">
					<div class="control-group">
						<label class="control-label">รหัสนักศึกษา</label>
						<input type="text" name="student_id" class="form-control" placeholder="id"/>
					</div>
					<div class="control-group">
						<label class="control-label">ชื่อ</label>
						<input type="text" name="name" class="form-control" placeholder="name"/>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" name="addstudent" value="1">เพิ่ม</button>
					<button class="btn" data-dismiss="modal" aria-hidden="true">ปิด</button>
				</div>
			</div>
		</div>
	</div>
</form>

<?php include '../../def/defJS.php'; ?>
<script type="text/javascript">
$(document).ready(function(){
	$("a[data-toggle=addstaff]").click(function(){ $('#addStaffModal').modal('show'); });
	$("a[data-toggle=addstudent]").click(function(){ $('#addStudentModal').modal('show'); });
});
</script>

<?php include '../../def/defFooter.php'; ?>