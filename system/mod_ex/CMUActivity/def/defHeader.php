<?php
function showMenu($selected) 
{ 
	?>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<ul class="nav nav-tabs">
					<?php
					$code="faculty"; $file="o_faculty.php?c=".$_GET['c']; $name="Faculty";
					if($selected==$code)
						echo "<li class=\"active\">";
					else
						echo "<li>";
					echo "<a href=\"". $file ."\"><i class='glyphicon glyphicon-search'></i> ". $name ."</a>";
					echo "</li>";

					$code="organizer"; $file="o_organizer.php?c=".$_GET['c']; $name="Organizer";
					if($selected==$code)
						echo "<li class=\"active\">";
					else
						echo "<li>";
					echo "<a href=\"". $file ."\"><i class='glyphicon glyphicon-search'></i> ". $name ."</a>";
					echo "</li>";
					?>
				</ul>
			</div>
		</div>
	</div>
	<br />
	<?php 
} 
?>