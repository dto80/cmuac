<?php
if(!isset($_GET['code']))
{
	echo 'need GET [code] / hint : code is code !!';
	exit();
}

if(!isset($_GET['activity_id']))
{
	echo 'need GET [activity_id]';
	exit();
}

define('FPDF_FONTPATH','../../src/lib/fpdf17/font/');
include_once '../../src/lib/fpdf17/fpdf.php';
include_once '../../src/lib/fpdf17/fpdfmy.php';

include '../../def/defImport.php';

error_reporting(0);
ini_set('display_errors', 0);

// config
$dataperpage = 36;

// FETCH DATA
$where = array(
	'AND' => array(
		"activity_id" => $_GET['activity_id'],
		"enroll_status" => "approve",
		),
	);
$enrolllist = Amst::select($_GET['code'].'_student_enroll','*',$where);

$where = array('id' => $_GET['activity_id']);
$activity = Amst::get($_GET['code'].'_activity','*',$where);

// top page info
$name = $activity['name'];
$username = $_GET['user'];

// start
$pdf = new FPDFMY();
$pdf->SetAutoPageBreak(false);
$pdf->AddFont('angsana','','angsa.php');
$pdf->AddFont('angsana','B','angsab.php');
$pdf->AddFont('angsana','I','angsai.php');
$pdf->AddFont('angsana','BI','angsaz.php');
$pdf->SetFont('angsana','',14);
$pdf->SetTextColor(0,0,0);

$currentrow = 1;
$currentpage = 1;

$studentlist = array();
foreach ($enrolllist as $enroll)
{
	$where = array('id' => $enroll['student_id']);
	$student = Amst::get($_GET['code'].'_student','*',$where);

	if($student)
		$studentlist[$student['student_id']] = $student['name'];
}

ksort($studentlist);


foreach ($studentlist as $student_id => $student_name)
{
	if($currentrow==1)
	{
		$pdf->addPage(); 

		$pdf->Image('../../conf/logo.png',25,5,20,20,'PNG');

		$pdf->SetFont('angsana','',22);
		$pdf->setX(5);
		$pdf->CellFitScale( 198  , 10 , iconv( 'UTF-8','cp874' , $name), "" , 1 , "C" );	

		$pdf->SetFont('angsana','',18);
		$pdf->setX(5);
		$pdf->CellFitScale( 198  , 7 , iconv( 'UTF-8','cp874' , 'พิมพ์วันที่ ['.date('Y-m-d H:i:s', time()).'] ['.$username.']'), "" , 1 , "C" );

		$pdf->SetFont('angsana','',22);
		$pdf->Text( 185 , 10 , 'Page '.$currentpage);
		$pdf->setY(35);
	}

	$pdf->SetFont('angsana','',14);
	$pdf->CellFitScale( 30  , 7 , iconv( 'UTF-8','cp874' , "  ".$student_id), "LRBT" , 0 , "C" );
	$pdf->CellFitScale( 160  , 7 , iconv( 'UTF-8','cp874' , "  ".$student_name), "LRBT" , 1 , "L" );

	if($currentrow==$dataperpage)
	{
		$currentrow=0;
		$currentpage++;
	}

	$currentrow++;
}

$pdf->Output(); 
