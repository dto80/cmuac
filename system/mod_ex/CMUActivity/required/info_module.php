<?php
$sqlinfo[$modulename] = array(
	// FACULTY
	'_faculty' => '_faculty` (
		`id` BIGINT NOT NULL PRIMARY KEY ,

		`name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci ,'.
		$appenddata.') ENGINE = MYISAM ;',

	// ORGANIZER
	'_organizer' => '_organizer` (
		`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,

		`name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci,
		`faculty_id` BIGINT ,'.
		$appenddata.') ENGINE = MYISAM ;',

	'_staff_organizer' => '_staff_organizer` (
		`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,

		`staff_id` BIGINT NOT NULL,
		`organizer_id` BIGINT NOT NULL ,'.
		$appenddata.') ENGINE = MYISAM ;',

	// STAFF
	'_staff' => '_staff` (
		`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,

		`faculty_id` BIGINT NOT NULL,

		`email` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
		`name` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
		`tel` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,

		`hash` CHAR(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,'.
		$appenddata.') ENGINE = MYISAM ;',

	// ACTIVITY TEMPLATE
	'_activity_template' => '_activity_template` (
		`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
		`name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci,
		`organizer_id` BIGINT ,'.
		$appenddata.') ENGINE = MYISAM ;',

	'_activity_template_entity' => '_activity_template_entity` (
		`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
		`activity_template_id` BIGINT,
		`abbreviation` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci,
		`name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci ,'.
		$appenddata.') ENGINE = MYISAM ;',

	// ACTIVITY REVIEW TEMPLATE
	'_activity_review_template' => '_activity_review_template` (
		`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
		`name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci,
		`organizer_id` BIGINT ,'.
		$appenddata.') ENGINE = MYISAM ;',

	'_activity_review_template_entity' => '_activity_review_template_entity` (
		`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
		`activity_review_template_id` BIGINT,
		`name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci,
		`review_type` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci ,'.
		$appenddata.') ENGINE = MYISAM ;',

	// ACTIVTY
	'_activity' => '_activity` (
		`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,

		`activity_type` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci,

		`academic_semester` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci,
		`academic_year` INT NOT NULL,
		`name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci,
		`description` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci,
		`place` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci,
		`no_of_hours` DECIMAL(12,2),

		`date_open_register` TIMESTAMP NOT NULL DEFAULT 0,
		`date_close_register` TIMESTAMP NOT NULL DEFAULT 0,
		`date_start_activity` TIMESTAMP NOT NULL DEFAULT 0,
		`date_end_activity` TIMESTAMP NOT NULL DEFAULT 0,
		`date_start_evaluate` TIMESTAMP NOT NULL DEFAULT 0,
		`date_stop_evaluate` TIMESTAMP NOT NULL DEFAULT 0,

		`description_objective` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci,
		`description_target` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci,

		`budget_estimate` DECIMAL(12,2) NOT NULL DEFAULT 0,
		`budget_actual` DECIMAL(12,2) NOT NULL DEFAULT 0,
		`enroll_maximum_student` INT NOT NULL DEFAULT 0,

		`activity_check_type` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci,

		`faculty_id` BIGINT NOT NULL,
		`organizer_id` BIGINT NOT NULL,
		`staff_id` BIGINT NOT NULL ,
		`activity_review_template_id` BIGINT NOT NULL,'.
		$appenddata.') ENGINE = MYISAM ;',

	'_activity_info_allow_enroll' => '_activity_info_allow_enroll` (
		`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,

		`activity_id` BIGINT NOT NULL,
		`faculty_id` BIGINT NOT NULL,'.
		$appenddata.') ENGINE = MYISAM ;',

	'_activity_info_template_entity' => '_activity_info_template_entity` (
		`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,

		`activity_id` BIGINT NOT NULL,
		`activity_template_entity_id` BIGINT NOT NULL,
		`value` INT NOT NULL,'.
		$appenddata.') ENGINE = MYISAM ;',

	// STUDENT
	'_student' => '_student` (
		`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,

		`student_id` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci,
		UNIQUE ( `student_id` ),
		`faculty_id` BIGINT,

		`name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci,
		
		`hash` CHAR(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,'.
		$appenddata.') ENGINE = MYISAM ;',

	'_student_enroll' => '_student_enroll` (
		`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
		`student_id` BIGINT,
		`activity_id` BIGINT,
		`enroll_status` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci,'.
		$appenddata.') ENGINE = MYISAM ;',

	'_student_checkin' => '_student_checkin` (
		`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
		`student_id` BIGINT,
		`activity_id` BIGINT,
		`hour` DECIMAL(12,1) NOT NULL ,
		`notice` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci,'.
		$appenddata.') ENGINE = MYISAM ;',);
?>