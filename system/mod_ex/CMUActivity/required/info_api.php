<?php
$apiinfo[$modulename] = array(
	'FACULTY' => array(
		'ADD' => array(
			'description' => 'เพิ่ม คณะใหม่ ถ้ามี ID ซ้ำ จะ add ไม่ได้',
			'required' => array(
				'action' => 'facultyadd',
				'faculty_id' => '',
				'faculty_name' => '',
				),
			'return' => 'text',
			'returndata' => 'FACULTY:GETALL',
			),
		'EDIT' => array(
			'description' => 'แก้ไขชื่อคณะ อิงจาก ID ที่ส่งมา',
			'required' => array(
				'action' => 'facultyedit',
				'old_faculty_id' => '',
				'new_faculty_id' => '',
				'new_faculty_name' => '',
				),
			'return' => 'text',
			'returndata' => 'FACULTY:GETALL',
			),
		'GET ALL' => array(
			'description' => 'ดึงคณะทั้งหมด',
			'required' => array(
				'action' => 'facultygetall',
				),
			'return' => 'custom',
			'returndata' => array(
				'faculty_id' => 'value',
				'faculty_name' => 'value',
				'organizers' => 'organizer list',
				),
			),
		),



	'ORGANIZER' => array(
		'ADD' => array(
			'description' => 'เพิ่ม หน่วยงานใหม่ในคณะ ถ้ามีชื่อซ้ำ จะ add ไม่ได้',
			'required' => array(
				'action' => 'organizeradd',
				'faculty_id' => '',
				'organizer_name' => '',
				),
			'return' => 'text',
			'returndata' => 'FACULTY:GETALL',
			),
		'EDIT' => array(
			'description' => 'แก้ไขชื่อหน่วยงานในคณะ อิงจาก ID ที่ส่งมา',
			'required' => array(
				'action' => 'organizeredit',
				'organizer_id' => '',
				'new_organizer_name' => '',
				),
			'return' => 'text',
			'returndata' => 'FACULTY:GETALL',
			),
		'DELETE' => array(
			'description' => 'ลบหน่วยงานออกจากระบบ',
			'required' => array(
				'action' => 'organizerdelete',
				'organizer_id' => '',
				),
			'return' => 'text',
			'returndata' => 'FACULTY:GETALL',
			),
		),



	'STAFF' => array(
		'EDIT' => array(
			'description' => 'จัดการ organizer ของ staff',
			'required' => array(
				'action' => 'staffeditorganizer',
				'staff_id' => '',
				'organizer_id[]' => 'array of organizer',
				),
			'return' => 'text',
			'returndata' => 'STAFF:GET ALL BY FACULTY',
			),
		'GET ALL BY FACULTY' => array(
			'description' => 'ดึง STAFF ทั้งหมด ตาม faculty',
			'required' => array(
				'action' => 'staffgetallbyfaculty',
				),
			'return' => 'custom',
			'returndata' => array(
				'faculty_id' => 'value',
				'faculty_name' => 'value',
				'staffs' => 'staff list',
				),
			),
		),



	'ACTIVITY TEMPLATE' => array(
		'ADD' => array(
			'description' => 'สร้าง template อิงจาก organizer id',
			'required' => array(
				'action' => 'activitytemplateadd',
				'organizer_id' => '',
				'activity_template_name' => ''
				),
			'return' => 'text',
			'returndata' => 'ACTIVITY TEMPLATE:GET ALL BY ORGANIZER',
			),
		'EDIT' => array(
			'description' => 'แก้ไข template อิงจาก activity_template_id แต่ส่ง organizer_id มาด้วย',
			'required' => array(
				'action' => 'activitytemplateedit',
				'organizer_id' => '',
				'activity_template_id' => '',
				'new_activity_template_name' => ''
				),
			'return' => 'text',
			'returndata' => 'ACTIVITY TEMPLATE:GET ALL BY ORGANIZER',
			),
		'DELETE' => array(
			'description' => 'ลบ template อิงจาก activity_template_id แต่ส่ง organizer_id มาด้วย',
			'required' => array(
				'action' => 'activitytemplatedelete',
				'organizer_id' => '',
				'activity_template_id' => ''
				),
			'return' => 'text',
			'returndata' => 'ACTIVITY TEMPLATE:GET ALL BY ORGANIZER',
			),
		'GET ALL BY ORGANIZER' => array(
			'description' => 'ดึง template อิงจาก organizer id',
			'required' => array(
				'action' => 'activitytemplategetbyorganizer',
				'organizer_id' => ''
				),
			'return' => 'custom',
			'returndata' => array(
				'activity_template_id' => 'value',
				'activiry_template_name' => 'value',
				'activitytemplateentities' => 'array',
				),
			),
		),



	'ACTIVITY TEMPLATE ENTITY' => array( //
		'ADD' => array(
			'description' => 'สร้าง template entity อิงจาก activity_template_id',
			'required' => array(
				'action' => 'activitytemplateentityadd',
				'activity_template_id' => '',
				'activity_template_entity_abbreviation' => '',
				'activity_template_entity_name' => ''
				),
			'return' => 'none',
			),
		'EDIT' => array(
			'description' => 'แก้ไข template entity อิงจาก activity_template_entity_id',
			'required' => array(
				'action' => 'activitytemplateentityedit',
				'activity_template_entity_id' => '',
				'new_activity_template_entity_abbreviation' => '',
				'new_activity_template_entity_name' => ''
				),
			'return' => 'none',
			),
		'DELETE' => array(
			'description' => 'ลบ template อิงจาก activity_template_entity_id',
			'required' => array(
				'action' => 'activitytemplateentitydelete',
				'activity_template_entity_id' => ''
				),
			'return' => 'none',
			),
		),



	'ACTIVITY REVIEW TEMPLATE' => array(
		'ADD' => array(
			'description' => 'สร้าง review template อิงจาก organizer id',
			'required' => array(
				'action' => 'activityreviewtemplateadd',
				'organizer_id' => '',
				'activity_review_template_name' => ''
				),
			'return' => 'text',
			'returndata' => 'ACTIVITY REVIEW TEMPLATE:GET ALL BY ORGANIZER',
			),
		'EDIT' => array(
			'description' => 'แก้ไข review template อิงจาก activity_review_template_id แต่ส่ง organizer_id มาด้วย',
			'required' => array(
				'action' => 'activityreviewtemplateedit',
				'organizer_id' => '',
				'activity_review_template_id' => '',
				'new_activity_review_template_name' => ''
				),
			'return' => 'text',
			'returndata' => 'ACTIVITY REVIEW TEMPLATE:GET ALL BY ORGANIZER',
			),
		'DELETE' => array(
			'description' => 'ลบ review template อิงจาก activity_review_template_id แต่ส่ง organizer_id มาด้วย',
			'required' => array(
				'action' => 'activityreviewtemplatedelete',
				'organizer_id' => '',
				'activity_review_template_id' => ''
				),
			'return' => 'text',
			'returndata' => 'ACTIVITY REVIEW TEMPLATE:GET ALL BY ORGANIZER',
			),
		'GET ALL BY ORGANIZER' => array(
			'description' => 'ดึง review template อิงจาก organizer id',
			'required' => array(
				'action' => 'activityreviewtemplategetbyorganizer',
				'organizer_id' => ''
				),
			'return' => 'custom',
			'returndata' => array(
				'activity_review_template_id' => 'value',
				'activity_review_template_name' => 'value',
				'activityreviewtemplateentities' => 'array',
				),
			),
		),



	'ACTIVITY REVIEW TEMPLATE ENTITY' => array( //
		'ADD' => array(
			'description' => 'สร้าง review template entity อิงจาก activity_review_template_id',
			'required' => array(
				'action' => 'activityreviewtemplateentityadd',
				'activity_review_template_id' => '',
				'activity_review_template_entity_name' => '',
				'activity_review_template_entity_review_type' => '',
				),
			'return' => 'none',
			),
		'EDIT' => array(
			'description' => 'แก้ไข review template entity อิงจาก activity_review_template_entity_id',
			'required' => array(
				'action' => 'activityreviewtemplateentityedit',
				'activity_review_template_entity_id' => '',
				'new_activity_review_template_entity_name' => '',
				'new_activity_review_template_entity_review_type' => ''
				),
			'return' => 'none',
			),
		'DELETE' => array(
			'description' => 'ลบ template อิงจาก activity_review_template_entity_id',
			'required' => array(
				'action' => 'activityreviewtemplateentitydelete',
				'activity_review_template_entity_id' => ''
				),
			'return' => 'none',
			),
		),



	'ACTIVITY' => array( //
		'ADD' => array(
			'description' => 'เพิ่ม Activity',
			'required' => array(
				'action' => 'activityadd',
				'activity_type' => '',

				'academic_semester' => '',
				'academic_year' => '',
				'name' => '',
				'description' => '',
				'place' => '',
				'no_of_hours' => '',

				'date_open_register' => 'YYYY-MM-DD',
				'date_close_register' => 'YYYY-MM-DD',
				'date_start_activity' => 'YYYY-MM-DD',
				'date_end_activity' => 'YYYY-MM-DD',
				'date_start_evaluate' => 'YYYY-MM-DD',
				'date_stop_evaluate' => 'YYYY-MM-DD',

				'description_objective' => '',
				'description_target' => '',

				'budget_estimate' => '',
				'budget_actual' => '',
				'enroll_maximum_student' => '',

				'activity_check_type' => '',

				'image1' => 'blob',
				'image2' => 'blob',

				'faculty_id' => '',
				'organizer_id' => '',
				'staff_id' => '',
				'activity_review_template_id' => '',

				'faculty_list[]' => 'array of faculty',
				'activity_template[]' => 'array of activity template',
				),
			'return' => 'none',
			),
		'GET BY ID' => array(
			'description' => 'ดึง ACTIVITY ตาม activityid',
			'required' => array(
				'action' => 'activitygetbyid',
				'activity_id' => 'value',
				),
			'return' => 'custom',
			'returndata' => array(
				'activity_id' => 'id',
				'activity_type' => 'value',

				'academic_semester' => 'number',
				'academic_year' => 'number',
				'name' => 'value',
				'description' => 'value',
				'place' => 'value',
				'no_of_hours' => 'number',

				'date_open_register' => 'value',
				'date_close_register' => 'value',
				'date_start_activity' => 'value',
				'date_end_activity' => 'value',
				'date_start_evaluate' => 'value',
				'date_stop_evaluate' => 'value',

				'description_objective' => 'value',
				'description_target' => 'value',

				'budget_estimate' => 'number',
				'budget_actual' => 'number',
				'enroll_maximum_student' => 'number',

				'activity_check_type' => 'value',

				'image1' => 'path',
				'image2' => 'path',

				'faculty_id' => 'id',
				'organizer_id' => 'id',
				'staff_id' => 'id',
				'activity_review_template_id' => 'id',

				'faculty_list' => 'array of faculty',
				'activity_template' => 'array of activity template',
				),
			),
		'GET ALL' => array(
			'description' => 'ดึง ACTIVITY ทั้งหมด ตาม facultyid',
			'required' => array(
				'action' => 'activitygetall',
				'faculty_id' => 'value',
				),
			'return' => 'custom',
			'returndata' => array(
				'activity_id' => 'value',
				'activity_name' => 'value',
				),
			),
		'GET CURRENT ENROLL LIST [STUDENT SIDE]' => array(
			'description' => 'ดึง ACTIVITY ทั้งหมดที่ลงทะเบียนไปแล้ว ส่ง student_id มาด้วย',
			'required' => array(
				'action' => 'activitygetcurrentenrolllist',
				'student_id' => 'value',
				'enroll_status' => '(optional) sent all enroll_status if this parameter is null',
				),
			'return' => 'custom',
			'returndata' => array(
				'activity_id' => 'value',
				'activity_name' => 'value',
				'enroll_maximum_student' => 'value',
				'enroll_status' => 'not_assign | waiting | approve | cancel',
				),
			),
		'GET EVALUATE LIST [STUDENT SIDE]' => array(
			'description' => 'ดึงข้อมูลว่าต้องรีวิวอะไรบ้างของ Activity',
			'required' => array(
				'action' => 'activitygetreviewlist',
				'student_id' => 'value',
				'activity_id' => 'value',
				),
			'return' => 'custom',
			'returndata' => array(
				'activity_review_template_entity_id' => 'value',
				'activity_review_template_entity_name' => 'value',
				'activity_review_template_entity_review_type' => 'array',
				),
			),
		'GET AVAILABLE LIST FOR ENROLL [STAFF SIDE]' => array(
			'description' => 'ดึง ACTIVITY ทั้งหมดที่ลงทะเบียนได้ ส่ง faculty_id มาด้วย',
			'required' => array(
				'action' => 'activitygetallforenroll',
				'faculty_id' => 'value',
				),
			'return' => 'custom',
			'returndata' => array(
				'activity_id' => 'value',
				'activity_name' => 'value',
				'enroll_maximum_student' => 'value',
				),
			),
		),



	'STUDENT' => array( //
		'ENROLLMENT : ENROLL' => array(
			'description' => 'ลงทะเบียน Student เป็น not_assign',
			'required' => array(
				'action' => 'studentenroll',
				'student_id' => '',
				'activity_id[]' => 'array of activity',
				),
			'return' => 'custom',
			'returndata' => array(
				'activity_id' => 'value',
				'action_status' => 'true | false',
				'reason' => '',
				),
			),
		'ENROLLMENT : APPROVE / WAITING LIST / CANCEL' => array(
			'description' => 'ยืนยันการลงทะเบียน Student เป็น approve, waiting_list, cancel',
			'required' => array(
				'action' => 'studentenrollchangestatus',
				'activity_id' => '',
				'student_id[]' => '',
				'status' => '',
				),
			'return' => 'custom',
			'returndata' => array(
				'student_id' => 'value',
				'action_status' => 'true | false',
				'reason' => '',
				),
			),
		'GET ENROLL STUDENT LIST' => array(
			'description' => 'ดึงรายชื่อนักเรียนที่ลงทะเบียนใน activity',
			'required' => array(
				'action' => 'studentgetbyenrollment',
				'activity_id' => 'value',
				'enroll_status' => '(optional) sent all enroll_status if this parameter is null',
				),
			'return' => 'custom',
			'returndata' => array(
				'student_id' => 'value',
				'student_name' => 'value',
				'enroll_status' => 'not_assign | waiting | approve | cancel',
				),
			),

		'CHECKIN : STAFF' => array(
			'description' => 'ลงข้อมูลนักเรียนเข้าร่วมกิจกรรม Student',
			'required' => array(
				'action' => 'studentcheckinstaff',
				'activity_id' => 'value',
				'student_id[]' => 'array of students',
				'hour' => 'double 1,1.5,2,2.5 etc',
				'hour_type' => 'hour | percentage',
				'notice' => 'หมายเหตุกิจกรรม',
				),
			'return' => 'custom',
			'returndata' => array(
				'student_id' => 'value',
				'checkin_status' => 'true | false',
				'reason' => '',
				),
			),
		),




	'DROPDOWN LIST' => array( //
		'ORGANIZER LIST' => array(
			'description' => 'ตัว Dropdown ที่โชว์ตาม organizer',
			'required' => array(
				'action' => 'dropdownorganizer',
				'staff_id' => '',
				),
			'return' => 'custom',
			'returndata' => array(
				'faculty_id' => 'value',
				'organizer_id' => 'value',
				'faculty_name' => 'value',
				'organizer_name' => 'value',
				'isSelect' => 'boolean',
				),
			),
		'FACULTY LIST' => array(
			'description' => 'ตัว Dropdown ที่โชว์ตาม faculty',
			'required' => array(
				'action' => 'dropdownfaculty',
				'staff_id' => '',
				),
			'return' => 'custom',
			'returndata' => array(
				'faculty_id' => 'value',
				'faculty_name' => 'value',
				),
			),
		),

	'LOGIN' => array(
		'LOGIN' => array(
			'description' => 'Login เข้าระบบ',
			'required' => array(
				'action' => 'login',
				'user_type' => 'student || staff',
				'user_hash' => 'md5hash',
				),
			'return' => 'custom',
			'returndata' => array(
				'user_id' => 'value',
				'user_name' => 'value',
				'user_faculty_id' => 'value',
				'user_type' => 'value',
				),
			),
		),
	); //
?>