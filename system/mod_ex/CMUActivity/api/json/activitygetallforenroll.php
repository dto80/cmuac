<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// GET PARAMETER
$faculty_id = $_REQUEST['faculty_id'];

// REQUIRED
$callarr = array(
	$faculty_id,
	);
if(array_search("", $callarr) !== false)
	repError('Missing Parameter');

// SHOW FACULTY LIST
$where = array(
	'faculty_id' => $faculty_id,
	);
$activityinfoallowenrolllist = Amst::select($code.'_activity_info_allow_enroll','*',$where);

$i = 0;

foreach ($activityinfoallowenrolllist as $activityinfoallowenroll) 
{
	$where = array('id' => $activityinfoallowenroll['activity_id'],);
	$activity = Amst::get($code.'_activity','*',$where);

	$wrap[$i]['activity_id'] = $activity['id'];
	$wrap[$i]['activity_name'] = $activity['name'];
	$wrap[$i]['enroll_maximum_student'] = $activity['enroll_maximum_student'];

	$i++;
}

rep('success',true,$wrap);
?>