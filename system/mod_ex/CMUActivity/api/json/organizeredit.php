<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// GET PARAMETER
$organizer_id = $_REQUEST['organizer_id'];
$new_organizer_name = $_REQUEST['new_organizer_name'];

// REQUIRED
$callarr = array(
	$organizer_id,
	$new_organizer_name,
	);
if(array_search("", $callarr) !== false)
	repError('Missing Parameter');

// fetch
$where = array('id' => $organizer_id);
$organizerdata = Amst::get($code.'_organizer','*',$where);

if(!$organizerdata)
	repError('ORGANIZER ID Does not exists');

// INSERT DATA IF FACULTY NAME NOT EXISTS
$data = array('name' => $new_organizer_name);
$where = array('id' => $organizer_id);
$result = Amst::update($code.'_organizer',$data,$where,false);

if(!$result)
	repError('Failed to edit data');

// SHOW FACULTY LIST
include 'facultygetall.php';
?>