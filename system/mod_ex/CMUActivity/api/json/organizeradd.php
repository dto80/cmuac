<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// GET PARAMETER
$faculty_id = $_REQUEST['faculty_id'];
$organizer_name = $_REQUEST['organizer_name'];

// REQUIRED
$callarr = array(
	$faculty_id,
	$organizer_name,
	);
if(array_search("", $callarr) !== false)
	repError('Missing Parameter');

// fetch
$where = array('id' => $faculty_id);
$facultydata = Amst::get($code.'_faculty','*',$where);

if(!$facultydata)
	repNoData('Faculty not exists',false);

$where = array(
	'AND' => array(
		'faculty_id' => $faculty_id,
		'name' => $organizer_name
		)
	);
$organizerdata = Amst::get($code.'_organizer','*',$where);

if($organizerdata)
	repNoData('ORGANIZER NAME already exists',false);

// INSERT DATA IF FACULTY NAME NOT EXISTS
$data = array(
	'name' => $organizer_name,
	'faculty_id' => $faculty_id
	);
$organizerid = Amst::insert($code.'_organizer',$data,false);

if(!$organizerid)
	repNoData('Failed to add data',false);

// SHOW FACULTY LIST
include 'facultygetall.php';
?>