<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// GET PARAMETER
$organizer_id = $_REQUEST['organizer_id'];

// REQUIRED
$callarr = array(
	$organizer_id
	);
if(array_search("", $callarr) !== false)
	repError('Missing Parameter');

// SHOW STAFF LIST BY FACULTY
$where = array(
	'organizer_id' => $organizer_id,
	'ORDER' => 'name ASC'
	);
$activityreviewtemplatelist = Amst::select($code.'_activity_review_template','*',$where);

$i = 0;

$wrap = array();

foreach ($activityreviewtemplatelist as $activityreviewtemplate) 
{
	$wrap[$i]['activity_review_template_id'] = $activityreviewtemplate['id'];
	$wrap[$i]['activity_review_template_name'] = $activityreviewtemplate['name'];

	$wrap[$i]['activityreviewtemplateentities'] = array();
	$where = array(
		'activity_review_template_id' => $activityreviewtemplate['id'],
		'ORDER' => 'name ASC'
		);
	$activityreviewtemplateentitylist = Amst::select($code.'_activity_review_template_entity','*',$where);
	$j = 0;
	foreach ($activityreviewtemplateentitylist as $activityreviewtemplateentity) 
	{
		$wrap[$i]['activityreviewtemplateentities'][$j]['activity_review_template_entity_id'] = $activityreviewtemplateentity['id'];
		$wrap[$i]['activityreviewtemplateentities'][$j]['activity_review_template_entity_name'] = $activityreviewtemplateentity['name'];
		$wrap[$i]['activityreviewtemplateentities'][$j]['activity_review_template_entity_review_type'] = $activityreviewtemplateentity['review_type'];

		$j++;
	}

	$i++;
}

rep('success',true,$wrap);
?>