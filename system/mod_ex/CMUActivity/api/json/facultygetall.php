<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// SHOW FACULTY LIST
$where = array('ORDER' => 'name ASC');
$facultylist = Amst::select($code.'_faculty','*',$where);

$i = 0;

foreach ($facultylist as $faculty) 
{
	$wrap[$i]['faculty_id'] = $faculty['id'];
	$wrap[$i]['faculty_name'] = $faculty['name'];

	$wrap[$i]['organizers'] = array();
	$where = array(
		'faculty_id'=>$faculty['id'],
		'ORDER' => 'name ASC'
		);
	$organizerlist = Amst::select($code.'_organizer','*',$where);
	$j = 0;
	foreach ($organizerlist as $organizer) 
	{
		$wrap[$i]['organizers'][$j]['organizer_id'] = $organizer['id'];
		$wrap[$i]['organizers'][$j]['organizer_name'] = $organizer['name'];

		$j++;
	}

	$i++;
}

rep('success',true,$wrap);
?>