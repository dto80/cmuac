<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// GET PARAMETER
$staff_id = $_REQUEST['staff_id'];
$organizer_id_list = $_REQUEST['organizer_id'];

// REQUIRED
$callarr = array(
	$staff_id,
	);
if(array_search("", $callarr) !== false)
	repError('Missing Parameter');

// remove staff organizer
$where = array('staff_id' => $staff_id);
Amst::delete($code.'_staff_organizer',$where);

// add list of organizer to staff
foreach ($organizer_id_list as $key => $organizer_id) 
{
	$data = array(
		'staff_id' => $staff_id,
		'organizer_id' => $organizer_id,
		);
	Amst::insert($code.'_staff_organizer',$data,false);
}

// SHOW STAFF LIST BY FACULTY
include 'staffgetall.php';
?>