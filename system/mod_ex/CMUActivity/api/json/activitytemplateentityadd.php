<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// GET PARAMETER
$activity_template_id = $_REQUEST['activity_template_id'];
$activity_template_entity_abbreviation = $_REQUEST['activity_template_entity_abbreviation'];
$activity_template_entity_name = $_REQUEST['activity_template_entity_name'];

// REQUIRED
$callarr = array(
	$activity_template_id,
	$activity_template_entity_abbreviation,
	$activity_template_entity_name
	);
if(array_search("", $callarr) !== false)
	repError('Missing Parameter');

// INSERT DATA
$data = array(
	'activity_template_id' => $activity_template_id,
	'abbreviation' => $activity_template_entity_abbreviation,
	'name' => $activity_template_entity_name
	);
$result = Amst::insert($code.'_activity_template_entity',$data,false);

if(!$result)
	repError('Problem add activity template entity data',false);

repNoData('success',true);
?>