<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// GET PARAMETER
$faculty_id = $_REQUEST['faculty_id'];
$faculty_name = $_REQUEST['faculty_name'];

// REQUIRED
$callarr = array(
	$faculty_id,
	$faculty_name,
	);
if(array_search("", $callarr) !== false)
	repError('Missing Parameter');

// check id
$where = array('id' => $faculty_id);
$facultydata = Amst::get($code.'_faculty','*',$where);

if($facultydata)
	repNoData('FACULTY ID already exists',false);

// check name
$where = array('name' => $faculty_name);
$facultydata = Amst::get($code.'_faculty','*',$where);

if($facultydata)
	repNoData('FACULTY NAME already exists',false);

// INSERT DATA IF FACULTY NAME NOT EXISTS
$data = array(
	'id' => $faculty_id,
	'name' => $faculty_name
	);
Amst::insert($code.'_faculty',$data,false);

// SHOW FACULTY LIST
include 'facultygetall.php';
?>