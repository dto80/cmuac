<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// GET PARAMETER
$activity_id = $_REQUEST['activity_id'];
$student_id_list = $_REQUEST['student_id'];
$hour = $_REQUEST['hour'];
$hour_type = $_REQUEST['hour_type'];
$notice = $_REQUEST['notice'];

// REQUIRED
$callarr = array(
	$activity_id,
	$student_id_list,
	$hour,
	$hour_type,
	);
if(array_search("", $callarr) !== false)
	repError('Missing Parameter');

// fetch
$where = array('id' => $activity_id);
$activitydata = Amst::get($code.'_activity','*',$where);

if(!$activitydata)
	repError('Activity not found');

if($hour<=0)
	repError('[hour] less than zero');

if($hour_type != 'hour' && $hour_type != 'percentage')
	repError('[hour_type] value is not [hour] or [percentage]');

$i = 0;

foreach ($student_id_list as $student_id) 
{
	$wrap[$i]['student_id'] = $student_id;	

	$where = array('student_id' => $student_id);
	$student = Amst::get($code.'_student','*',$where);

	if(!$student)
	{
		$wrap[$i]['checkin_status'] = false;
		$wrap[$i]['reason'] = 'Student not found';	
		$i++;
		continue;
	}

	$where = array(
		'AND' => array(
			'student_id' => $student['id'],
			'activity_id' => $activity_id
			)
		);
	$studentenroll = Amst::get($code.'_student_enroll','*',$where);

	if(!$studentenroll)
	{
		$wrap[$i]['checkin_status'] = false;	
		$wrap[$i]['reason'] = 'student not enroll in this activity';	
		$i++;
		continue;
	}


	$insert_hour = $hour;
	if($hour_type != 'hour')
		$insert_hour = ($hour * $activitydata['no_of_hours']) / 100;

	$data = array(
		'student_id' => $student['id'],
		'activity_id' => $activity_id,
		'hour' => $insert_hour,
		'notice' => $notice
		);
	$result = Amst::insert($code.'_student_checkin',$data);

	if($result)
	{
		$wrap[$i]['action_status'] = true;	
		$wrap[$i]['reason'] = 'success';	
		$i++;
	}
	else
	{
		$wrap[$i]['action_status'] = false;	
		$wrap[$i]['reason'] = 'Checkin failed';	
		$i++;
	}
}

rep('success',true,$wrap);
?>