<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// GET PARAMETER
$organizer_id = $_REQUEST['organizer_id'];
$activity_template_name = $_REQUEST['activity_template_name'];

// REQUIRED
$callarr = array(
	$organizer_id,
	$activity_template_name
	);
if(array_search("", $callarr) !== false)
	repError('Missing Parameter');

// INSERT DATA
$data = array(
	'name' => $activity_template_name,
	'organizer_id' => $organizer_id
	);
$result = Amst::insert($code.'_activity_template',$data,false);

if(!$result)
	repError('Problem add activity template data',false);

// SHOW STAFF LIST BY FACULTY
include 'activitytemplategetbyorganizer.php';
?>