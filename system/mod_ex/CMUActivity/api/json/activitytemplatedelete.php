<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// GET PARAMETER
$organizer_id = $_REQUEST['organizer_id'];
$activity_template_id = $_REQUEST['activity_template_id'];

// REQUIRED
$callarr = array(
	$organizer_id,
	$activity_template_id
	);
if(array_search("", $callarr) !== false)
	repError('Missing Parameter');

// INSERT DATA
$where = array('id' => $activity_template_id,);
$result = Amst::delete($code.'_activity_template',$where);

if(!$result)
	repError('Problem delete activity template data (AKA no data to delete)',false);

// SHOW STAFF LIST BY FACULTY
include 'activitytemplategetbyorganizer.php';
?>