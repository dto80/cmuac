<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// GET PARAMETER
$faculty_id = $_REQUEST['faculty_id'];

// REQUIRED
$callarr = array(
	$faculty_id,
	);
if(array_search("", $callarr) !== false)
	repError('Missing Parameter');

// SHOW FACULTY LIST
$where = array(
	'faculty_id' => $faculty_id,
	'ORDER' => 'name ASC'
	);
$activitylist = Amst::select($code.'_activity','*',$where);

$i = 0;

foreach ($activitylist as $activity) 
{
	$wrap[$i]['activity_id'] = $activity['id'];
	$wrap[$i]['activity_name'] = $activity['name'];

	$i++;
}

rep('success',true,$wrap);
?>