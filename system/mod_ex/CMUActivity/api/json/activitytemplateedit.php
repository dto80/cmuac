<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// GET PARAMETER
$organizer_id = $_REQUEST['organizer_id'];
$activity_template_id = $_REQUEST['activity_template_id'];
$new_activity_template_name = $_REQUEST['new_activity_template_name'];

// REQUIRED
$callarr = array(
	$organizer_id,
	$activity_template_id,
	$new_activity_template_name
	);
if(array_search("", $callarr) !== false)
	repError('Missing Parameter');

// INSERT DATA
$data = array(
	'name' => $new_activity_template_name
	);
$where = array('id' => $activity_template_id);
$result = Amst::update($code.'_activity_template',$data,$where,false);

if(!$result)
	repError('Problem edit activity template data',false);

// SHOW STAFF LIST BY FACULTY
include 'activitytemplategetbyorganizer.php';
?>