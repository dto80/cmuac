<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// GET PARAMETER
$activity_id = $_REQUEST['activity_id'];
$student_id_list = $_REQUEST['student_id'];
$status = $_REQUEST['status'];

// REQUIRED
$callarr = array(
	$activity_id,
	$student_id_list,
	);
if(array_search("", $callarr) !== false)
	repError('Missing Parameter');

// fetch
$where = array('id' => $activity_id);
$activitydata = Amst::get($code.'_activity','*',$where);

if(!$activitydata)
	repError('Activity not found');

if($status != 'approve' && $status != 'waiting_list' && $status != 'cancel')
	repError('[status] value is not [approve],[waiting_list],[cancel]');

$i = 0;

foreach ($student_id_list as $student_id) 
{
	$wrap[$i]['student_id'] = $student_id;

	$where = array('student_id' => $student_id);
	$student = Amst::get($code.'_student','*',$where);

	if(!$student)
	{
		$wrap[$i]['action_status'] = false;	
		$wrap[$i]['reason'] = 'Student not found';	
		$i++;
		continue;
	}

	$where = array(
		'AND' => array(
			'student_id' => $student['id'],
			'activity_id' => $activity_id
			)
		);
	$studentenroll = Amst::get($code.'_student_enroll','*',$where);

	if(!$studentenroll)
	{
		$wrap[$i]['action_status'] = false;	
		$wrap[$i]['reason'] = 'activity never enroll before';	
		$i++;
		continue;
	}

	$data = array(
		'enroll_status' => $status
		);
	$where = array('id' => $studentenroll['id']);
	$result = Amst::update($code.'_student_enroll',$data,$where);

	if($result)
	{
		$wrap[$i]['action_status'] = true;	
		$wrap[$i]['reason'] = 'success';	
		$i++;
	}
	else
	{
		$wrap[$i]['action_status'] = false;	
		$wrap[$i]['reason'] = 'Change status failed';	
		$i++;
	}
}

rep('success',true,$wrap);
?>