<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// SHOW STAFF LIST BY FACULTY
$where = array('ORDER' => 'name ASC');
$facultylist = Amst::select($code.'_faculty','*',$where);

$i = 0;

foreach ($facultylist as $faculty) 
{
	$wrap[$i]['faculty_id'] = $faculty['id'];

	$wrap[$i]['staffs'] = array();
	$where = array('faculty_id'=>$faculty['id']);
	$stafflist = Amst::select($code.'_staff','*',$where);
	$j = 0;
	foreach ($stafflist as $staff) 
	{
		$wrap[$i]['staffs'][$j]['staff_id'] = $staff['id'];

		$wrap[$i]['staffs'][$j]['staff_email'] = $staff['email'];
		$wrap[$i]['staffs'][$j]['staff_name'] = $staff['name'];
		$wrap[$i]['staffs'][$j]['staff_tel'] = $staff['tel'];

		$j++;
	}

	$wrap[$i]['faculty_name'] = $faculty['name'].' ('.$j.')';

	$i++;
}

rep('success',true,$wrap);
?>