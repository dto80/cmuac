<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// GET PARAMETER
$user_type = $_REQUEST['user_type'];
$user_hash = $_REQUEST['user_hash'];

// REQUIRED
$callarr = array(
	$user_type,
	$user_hash,
	);
if(array_search("", $callarr) !== false)
	repError('Missing Parameter');

if($user_type!='student' && $user_type!='staff')
	repError('Invalid user type');

// fetch
$where = array('hash' => $user_hash);
$userdata = Amst::get($code.'_'.$user_type,'*',$where);

if(!$userdata)
	repNoData('User not found',false);

$i = 0;

$wrap[$i]['user_id'] = $userdata['id'];
$wrap[$i]['user_name'] = $userdata['name'];
$wrap[$i]['user_faculty_id'] = $userdata['faculty_id'];
$wrap[$i]['user_type'] = $user_type;

rep('success',true,$wrap);
?>