<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// GET PARAMETER
$student_id = $_REQUEST['student_id'];
$enroll_status = $_REQUEST['enroll_status'];

// REQUIRED
$callarr = array(
	$student_id,
	);
if(array_search("", $callarr) !== false)
	repError('Missing Parameter');

if($enroll_status!='')
{
	if($enroll_status!='not_assign' && $enroll_status!='waiting_list' && $enroll_status!='approve' && $enroll_status!='cancel')
		repError('[enroll_status] has invalid value (not_assign,waiting_list,approve,cancel)');
}

// SHOW FACULTY LIST
$where = array(
	'AND' => array(
		'student_id' => $student_id,
		)
	);
if($enroll_status!='')
	$where['AND']['enroll_status'] = $enroll_status;
$studentenrolllist = Amst::select($code.'_student_enroll','*',$where);

$i = 0;

foreach ($studentenrolllist as $studentenroll) 
{
	$where = array('id' => $studentenroll['activity_id'],);
	$activity = Amst::get($code.'_activity','*',$where);

	$wrap[$i]['activity_id'] = $activity['id'];
	$wrap[$i]['activity_name'] = $activity['name'];
	$wrap[$i]['enroll_maximum_student'] = $activity['enroll_maximum_student'];
	$wrap[$i]['enroll_status'] = $studentenroll['enroll_status'];

	$i++;
}

rep('success',true,$wrap);
?>