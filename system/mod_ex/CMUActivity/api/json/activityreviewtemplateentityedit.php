<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// GET PARAMETER
$activity_review_template_entity_id = $_REQUEST['activity_review_template_entity_id'];
$new_activity_review_template_entity_name = $_REQUEST['new_activity_review_template_entity_name'];
$new_activity_review_template_entity_review_type = $_REQUEST['new_activity_review_template_entity_review_type'];

// REQUIRED
$callarr = array(
	$activity_review_template_entity_id,
	$new_activity_review_template_entity_name,
	$new_activity_review_template_entity_review_type,
	);
if(array_search("", $callarr) !== false)
	repError('Missing Parameter');

// INSERT DATA
$data = array(
	'name' => $new_activity_review_template_entity_name,
	'review_type' => $new_activity_review_template_entity_review_type,
	);
$where = array('id' => $activity_review_template_entity_id);
$result = Amst::update($code.'_activity_review_template_entity',$data,$where,false);

if(!$result)
	repError('Problem edit activity review template entity data',false);

repNoData('success',true);
?>