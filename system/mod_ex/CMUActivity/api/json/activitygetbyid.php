<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// GET PARAMETER
$activity_id = $_REQUEST['activity_id'];

// REQUIRED
$callarr = array(
	$activity_id,
	);
if(array_search("", $callarr) !== false)
	repError('Missing Parameter');

// SHOW FACULTY LIST
$where = array('id' => $activity_id,);
$activity = Amst::get($code.'_activity','*',$where);

if(!$activity)
	repError('Activity not found');

$i = 0;

$wrap[$i]['activity_id'] = $activity['id'];

$wrap[$i]['activity_type'] = $activity['activity_type'];

$wrap[$i]['academic_semester'] = $activity['academic_semester'];
$wrap[$i]['academic_year'] = $activity['academic_year'];
$wrap[$i]['name'] = $activity['name'];
$wrap[$i]['description'] = $activity['description'];
$wrap[$i]['place'] = $activity['place'];
$wrap[$i]['no_of_hours'] = $activity['no_of_hours'];

$wrap[$i]['description_objective'] = $activity['description_objective'];
$wrap[$i]['date_close_register'] = $activity['date_close_register'];
$wrap[$i]['date_start_activity'] = $activity['date_start_activity'];
$wrap[$i]['date_end_activity'] = $activity['date_end_activity'];
$wrap[$i]['date_start_evaluate'] = $activity['date_start_evaluate'];
$wrap[$i]['no_of_hours'] = $activity['date_stop_evaluate'];

$wrap[$i]['description_objective'] = $activity['description_objective'];
$wrap[$i]['description_target'] = $activity['description_target'];

$wrap[$i]['budget_estimate'] = $activity['budget_estimate'];
$wrap[$i]['budget_actual'] = $activity['budget_actual'];
$wrap[$i]['enroll_maximum_student'] = $activity['enroll_maximum_student'];

$wrap[$i]['activity_check_type'] = $activity['activity_check_type'];

$wrap[$i]['image1'] = File::getPath($code.'_activity1_'.$activity['id'],'../../');
$wrap[$i]['image2'] = File::getPath($code.'_activity2_'.$activity['id'],'../../');

$wrap[$i]['faculty_id'] = $activity['faculty_id'];
$wrap[$i]['organizer_id'] = $activity['organizer_id'];
$wrap[$i]['staff_id'] = $activity['staff_id'];
$wrap[$i]['activity_review_template_id'] = $activity['activity_review_template_id'];

$wrap[$i]['faculty_list'] = array();
$where = array('activity_id' => $activity['id']);
$allowenroll_list = Amst::select($code.'_activity_info_allow_enroll','*',$where);
$j = 0;
foreach($allowenroll_list as $allowenroll)
{
	$where = array('id' => $allowenroll['faculty_id']);
	$faculty = Amst::get($_GET['c'].'_faculty','*',$where);

	$wrap[$i]['faculty_list'][$j]['id'] = $faculty['id'];
	$wrap[$i]['faculty_list'][$j]['name'] = $faculty['name'];
	$j++;
}

$wrap[$i]['activity_template'] = array();
$where = array('activity_id' => $activity['id']);
$infotemplateentity_list = Amst::select($code.'_activity_info_template_entity','*',$where);
$j = 0;
foreach($infotemplateentity_list as $infotemplateentity)
{
	$where = array('id' => $infotemplateentity['activity_template_entity_id']);
	$templateentity = Amst::get($_GET['c'].'_activity_template_entity','*',$where);

	$wrap[$i]['activity_template'][$j]['id'] = $templateentity['id'];
	$wrap[$i]['activity_template'][$j]['abbreviation'] = $templateentity['abbreviation'];
	$wrap[$i]['activity_template'][$j]['name'] = $templateentity['name'];
	$j++;
}

rep('success',true,$wrap);
?>