<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// GET PARAMETER
$student_id = $_REQUEST['student_id'];
$activity_id_list = $_REQUEST['activity_id'];

// REQUIRED
$callarr = array(
	$student_id,
	$activity_id_list,
	);
if(array_search("", $callarr) !== false)
	repError('Missing Parameter');

// fetch
$where = array('id' => $student_id);
$studentdata = Amst::get($code.'_student','*',$where);

if(!$studentdata)
	repError('Student not found');

$i = 0;

foreach ($activity_id_list as $activity_id) 
{
	$wrap[$i]['activity_id'] = $activity_id;

	$where = array('id' => $activity_id);
	$activity = Amst::get($code.'_activity','*',$where);

	if(!$activity)
	{
		$wrap[$i]['action_status'] = false;	
		$wrap[$i]['reason'] = 'Activity not found';	
		$i++;
		continue;
	}

	$where = array(
		'AND' => array(
			'student_id' => $student_id,
			'activity_id' => $activity_id
			)
		);
	$studentenroll = Amst::get($code.'_student_enroll','*',$where);

	if($studentenroll)
	{
		$wrap[$i]['action_status'] = false;	
		$wrap[$i]['reason'] = 'activity already enroll';	
		$i++;
		continue;
	}

	$data = array(
		'student_id' => $student_id,
		'activity_id' => $activity_id,
		'enroll_status' => 'waiting_list'
		);
	$result = Amst::insert($code.'_student_enroll',$data);

	if($result)
	{
		$wrap[$i]['action_status'] = true;	
		$wrap[$i]['reason'] = 'success';	
		$i++;
	}
	else
	{
		$wrap[$i]['action_status'] = false;	
		$wrap[$i]['reason'] = 'Enroll failed';	
		$i++;
	}
}

rep('success',true,$wrap);
?>