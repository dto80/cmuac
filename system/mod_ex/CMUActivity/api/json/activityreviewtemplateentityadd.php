<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// GET PARAMETER
$activity_review_template_id = $_REQUEST['activity_review_template_id'];
$activity_review_template_entity_name = $_REQUEST['activity_review_template_entity_name'];
$activity_review_template_entity_review_type = $_REQUEST['activity_review_template_entity_review_type'];

// REQUIRED
$callarr = array(
	$activity_review_template_id,
	$activity_review_template_entity_name,
	$activity_review_template_entity_review_type,
	);
if(array_search("", $callarr) !== false)
	repError('Missing Parameter');

// INSERT DATA
$data = array(
	'activity_review_template_id' => $activity_review_template_id,
	'name' => $activity_review_template_entity_name,
	'review_type' => $activity_review_template_entity_review_type,
	);
$result = Amst::insert($code.'_activity_review_template_entity',$data,false);

if(!$result)
	repError('Problem add activity review template entity data',false);

repNoData('success',true);
?>