<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// GET PARAMETER
$old_faculty_id = $_REQUEST['old_faculty_id'];
$new_faculty_id = $_REQUEST['new_faculty_id'];
$new_faculty_name = $_REQUEST['new_faculty_name'];

// REQUIRED
$callarr = array(
	$old_faculty_id,
	$new_faculty_id,
	$new_faculty_name,
	);
if(array_search("", $callarr) !== false)
	repError('Missing Parameter');

// fetch
$where = array('id' => $old_faculty_id);
$facultydata = Amst::get($code.'_faculty','*',$where);

if(!$facultydata)
	repNoData('FACULTY ID Does not exists',false);

// INSERT DATA IF FACULTY NAME NOT EXISTS
$data = array(
	'id' => $new_faculty_id,
	'name' => $new_faculty_name
	);
$where = array('id' => $old_faculty_id);
$result = Amst::update($code.'_faculty',$data,$where,false);

if(!$result)
	repNoData('Failed to edit data',false);

// SHOW FACULTY LIST
include 'facultygetall.php';
?>