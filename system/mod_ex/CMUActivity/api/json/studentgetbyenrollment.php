<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// GET PARAMETER
$activity_id = $_REQUEST['activity_id'];
$enroll_status = $_REQUEST['enroll_status'];

// REQUIRED
$callarr = array(
	$activity_id,
	);
if(array_search("", $callarr) !== false)
	repError('Missing Parameter');

if($enroll_status!='')
{
	if($enroll_status!='not_assign' && $enroll_status!='waiting_list' && $enroll_status!='approve' && $enroll_status!='cancel')
		repError('[enroll_status] has invalid value (not_assign,waiting_list,approve,cancel)');
}

// fetch
$where = array('id' => $activity_id);
$activitydata = Amst::get($code.'_activity','*',$where);

if(!$activitydata)
	repError('Activity not found');

$where = array(
	'AND' => array(
		'activity_id' => $activity_id,
		)
	);
if($enroll_status!='')
	$where['AND']['enroll_status'] = $enroll_status;
$studentenrolllist = Amst::select($code.'_student_enroll','*',$where);

$i = 0;

foreach ($studentenrolllist as $studentenroll) 
{
	$where = array('id' => $studentenroll['student_id']);
	$student = Amst::get($code.'_student','*',$where);

	if($student)
	{
		$wrap[$i]['student_id'] = $student['student_id'];
		$wrap[$i]['student_name'] = $student['name'];	
		$wrap[$i]['enroll_status'] = $studentenroll['enroll_status'];	
		$i++;
	}
}

rep('success',true,$wrap);
?>