<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// GET PARAMETER
$activity_template_entity_id = $_REQUEST['activity_template_entity_id'];
$new_activity_template_entity_abbreviation = $_REQUEST['new_activity_template_entity_abbreviation'];
$new_activity_template_entity_name = $_REQUEST['new_activity_template_entity_name'];

// REQUIRED
$callarr = array(
	$activity_template_entity_id,
	$new_activity_template_entity_abbreviation,
	$new_activity_template_entity_name
	);
if(array_search("", $callarr) !== false)
	repError('Missing Parameter');

// INSERT DATA
$data = array(
	'abbreviation' => $new_activity_template_entity_abbreviation,
	'name' => $new_activity_template_entity_name
	);
$where = array('id' => $activity_template_entity_id);
$result = Amst::update($code.'_activity_template_entity',$data,$where,false);

if(!$result)
	repError('Problem edit activity template entity data',false);

repNoData('success',true);
?>