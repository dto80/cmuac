<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// GET PARAMETER
$student_id = $_REQUEST['student_id'];
$activity_id = $_REQUEST['activity_id'];

// REQUIRED
$callarr = array(
	$student_id,
	$activity_id,
	);
if(array_search("", $callarr) !== false)
	repError('Missing Parameter');

$where = array(
	'id' => $activity_id,
	);
$activity = Amst::get($code.'_activity','*',$where);

if(!$activity)
	repError('ACTIVITY not found');

$where = array(
	'activity_review_template_id' => $activity['activity_review_template_id'],
	);
$activityreviewtemplateentitylist = Amst::select($code.'_activity_review_template_entity','*',$where);

if(!$activityreviewtemplateentitylist)
	repError('ACTIVITY REVIEW TEMPLATE ENTITY not found');

$i = 0;

foreach ($activityreviewtemplateentitylist as $activityreviewtemplateentity) 
{
	$wrap[$i]['activity_review_template_id'] = $activityreviewtemplateentity['id'];
	$wrap[$i]['activity_review_template_name'] = $activityreviewtemplateentity['name'];
	$wrap[$i]['activity_review_template_review_type'] = $activityreviewtemplateentity['review_type'];

	$i++;
}

rep('success',true,$wrap);
?>