<?php
// GET PARAMETER
$activity_type = $_REQUEST['activity_type'];

$academic_semester = $_REQUEST['academic_semester'];
$academic_year = $_REQUEST['academic_year'];
$name = $_REQUEST['name'];
$description = $_REQUEST['description'];
$place = $_REQUEST['place'];
$no_of_hours = $_REQUEST['no_of_hours'];

$date_open_register = $_REQUEST['date_open_register'];
$date_close_register = $_REQUEST['date_close_register'];
$date_start_activity = $_REQUEST['date_start_activity'];
$date_end_activity = $_REQUEST['date_end_activity'];
$date_start_evaluate = $_REQUEST['date_start_evaluate'];
$date_stop_evaluate = $_REQUEST['date_stop_evaluate'];

$description_objective = $_REQUEST['description_objective'];
$description_target = $_REQUEST['description_target'];

$budget_estimate = $_REQUEST['budget_estimate'];
$budget_actual = $_REQUEST['budget_actual'];
$enroll_maximum_student = $_REQUEST['enroll_maximum_student'];

$activity_check_type = $_REQUEST['activity_check_type'];

$image1 = $_REQUEST['image1'];
$image2 = $_REQUEST['image2'];

$faculty_id = $_REQUEST['faculty_id'];
$organizer_id = $_REQUEST['organizer_id'];
$staff_id = $_REQUEST['staff_id'];
$activity_review_template_id = $_REQUEST['activity_review_template_id'];

$faculty_list = $_REQUEST['faculty_list'];
$activity_template_list = $_REQUEST['activity_template'];

// REQUIRED
$callarr = array(
	$apikey,
	$code,

	$activity_type,

	$academic_semester,
	$academic_year,
	$name,
	$description,
	$place,
	$no_of_hours,

	$date_open_register,
	$date_close_register,
	$date_start_activity,
	$date_end_activity,
	$date_start_evaluate,
	$date_stop_evaluate,

	$description_objective,
	$description_target,
	
	$activity_check_type,

	$organizer_id,
	$staff_id,
	$activity_review_template_id,
	);
if(array_search("", $callarr) !== false)
	repError('Missing Parameter');

// INSERT DATA
$data = array(
	'activity_type' => $activity_type,

	'academic_semester' => $academic_semester,
	'academic_year' => $academic_year,
	'name' => $name,
	'description' => $description,
	'place' => $place,
	'no_of_hours' => $no_of_hours,
	
	'date_open_register' => $date_open_register,
	'date_close_register' => $date_close_register.' 23:59:59',
	'date_start_activity' => $date_start_activity,
	'date_end_activity' => $date_end_activity.' 23:59:59',
	'date_start_evaluate' => $date_start_evaluate,
	'date_stop_evaluate' => $date_stop_evaluate.' 23:59:59',

	'description_objective' => $description_objective,
	'description_target' => $description_target,

	'budget_estimate' => $budget_estimate,
	'budget_actual' => $budget_actual,
	'enroll_maximum_student' => $enroll_maximum_student,

	'activity_check_type' => $activity_check_type,

	'faculty_id' => $faculty_id,
	'organizer_id' => $organizer_id,
	'staff_id' => $staff_id,
	'activity_review_template_id' => $activity_review_template_id,
	);
$result = Amst::insert($code.'_activity',$data);

if(!$result)
	repError('Problem add activity data');

$result = File::addImage_base64($code."_activity1_".$result,$image1,'../../');
$result = File::addImage_base64($code."_activity2_".$result,$image2,'../../');

foreach($faculty_list as $faculty)
{
	if($faculty['checked'])
	{
		$data = array(
			'activity_id' => $result,
			'faculty_id' => $faculty['id'],
			);
		Amst::insert($code.'_activity_info_allow_enroll',$data);
	}
}

foreach($activity_template_list as $activity_template)
{
	foreach($activity_template['activitytemplateentities'] as $activity_template_entity)
	{
		$data = array(
			'activity_id' => $result,
			'activity_template_entity_id' => $activity_template_entity['activity_template_entity_id'],
			'value' => $activity_template_entity['percent'],
			);
		Amst::insert($code.'_activity_info_template_entity',$data);
	}
}


rep('success',true);
?>