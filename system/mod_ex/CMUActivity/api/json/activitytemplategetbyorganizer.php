<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// GET PARAMETER
$organizer_id = $_REQUEST['organizer_id'];

// REQUIRED
$callarr = array(
	$organizer_id
	);
if(array_search("", $callarr) !== false)
	repError('Missing Parameter');

// SHOW STAFF LIST BY FACULTY
$where = array(
	'organizer_id' => $organizer_id,
	'ORDER' => 'name ASC'
	);
$activitytemplatelist = Amst::select($code.'_activity_template','*',$where);

$i = 0;

$wrap = array();

foreach ($activitytemplatelist as $activitytemplate) 
{
	$wrap[$i]['activity_template_id'] = $activitytemplate['id'];
	$wrap[$i]['activity_template_name'] = $activitytemplate['name'];

	$wrap[$i]['activitytemplateentities'] = array();
	$where = array(
		'activity_template_id' => $activitytemplate['id'],
		'ORDER' => 'name ASC'
		);
	$activitytemplateentitylist = Amst::select($code.'_activity_template_entity','*',$where);
	$j = 0;
	foreach ($activitytemplateentitylist as $activitytemplateentity) 
	{
		$wrap[$i]['activitytemplateentities'][$j]['activity_template_entity_id'] = $activitytemplateentity['id'];
		$wrap[$i]['activitytemplateentities'][$j]['activity_template_entity_abbreviation'] = $activitytemplateentity['abbreviation'];
		$wrap[$i]['activitytemplateentities'][$j]['activity_template_entity_name'] = $activitytemplateentity['name'];

		$j++;
	}

	$i++;
}

rep('success',true,$wrap);
?>