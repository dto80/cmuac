<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// GET PARAMETER
$staff_id = $_REQUEST['staff_id'];

// REQUIRED
$callarr = array(
	$staff_id
	);
if(array_search("", $callarr) !== false)
	repError('Missing Parameter');

// SHOW STAFF LIST BY FACULTY
$where = array('ORDER' => 'name ASC');
$facultylist = Amst::select($code.'_faculty','*',$where);

$i = 0;

foreach ($facultylist as $faculty) 
{
	$where = array(
		'faculty_id' => $faculty['id'],
		'ORDER' => 'name ASC'
		);
	$organizerlist = Amst::select($code.'_organizer','*',$where);

	foreach ($organizerlist as $organizer) 
	{
		$wrap[$i]['faculty_id'] = $faculty['id'];
		$wrap[$i]['organizer_id'] = $organizer['id'];
		$wrap[$i]['faculty_name'] = $faculty['name'];
		$wrap[$i]['organizer_name'] = $faculty['name'].' - '.$organizer['name'];

		$isSelect = false;
		$where = array(
			'AND' => array(
				'staff_id' => $staff_id,
				'organizer_id' => $organizer['id']
				)
			);
		if(Amst::has($code.'_staff_organizer',$where))
			$isSelect = true;
		$wrap[$i]['isSelect'] = $isSelect;

		$i++;
	}
}

rep('success',true,$wrap);
?>