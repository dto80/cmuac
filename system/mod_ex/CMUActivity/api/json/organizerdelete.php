<?php
if(!defined('CALLFROMMAIN'))
	die('Direct access not permitted');

// GET PARAMETER
$organizer_id = $_REQUEST['organizer_id'];

// REQUIRED
$callarr = array(
	$organizer_id,
	);
if(array_search("", $callarr) !== false)
	repError('Missing Parameter');

// fetch
$where = array('id' => $organizer_id);
$organizerdata = Amst::get($code.'_organizer','*',$where);

if(!$organizerdata)
	repError('ORGANIZER ID does not exists');

// INSERT DATA IF FACULTY NAME NOT EXISTS
$where = array('id' => $organizer_id);
$result = Amst::delete($code.'_organizer',$where,false);

if(!$result)
	repError('Failed to delete data');

// SHOW FACULTY LIST
include 'facultygetall.php';
?>