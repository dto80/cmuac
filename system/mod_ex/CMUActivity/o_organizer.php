<?php
include '../../def/defImport.php';
include '../../def/defCheckUser.php'; checkUser('low');

?>

<?php 
include '../../def/defHeader.php'; showMenuBar('function'); showFunctionMenuBar($_GET['c']); 
include 'def/defHeader.php'; showMenu('organizer');
?>

<div class="container" style="min-height:400px;">
	<div class="row">
		<div class="col-sm-3">
			<legend>ORGANIZER</legend>

			<table class="table table-bordered table-condensed">
				<tr>
					<th class="thincell">faculty</th>
					<th>organizer</th>
					<th class="thincell">&nbsp;</th>
				</tr>

				<?php
				$_GET['organizer'] = (isset($_GET['organizer']) ? $_GET['organizer'] : null);

				$where = array('ORDER' => 'name ASC');
				$facultylist = Amst::select($_GET['c'].'_faculty','*',$where);
				foreach ($facultylist as $faculty) 
				{
					$where = array(
						'faculty_id' => $faculty['id'],
						'ORDER' => 'name ASC'
						);
					$organizerlist = Amst::select($_GET['c'].'_organizer','*',$where);

					foreach ($organizerlist as $organizer) 
					{
						echo '<tr>';

						echo '<td>'.$faculty['name'].'</td>';
						echo '<td>'.$organizer['name'].'</td>';

						$where = array(
							'organizer_id' => $organizer['id'],
							);
						$activitytemplatecount = Amst::count($_GET['c'].'_activity_template',$where);

						echo '<td>';
						if($organizer['id']==$_GET['organizer'])
							echo "<a class='btn btn-xs btn-primary' href='o_organizer.php?c=".$_GET['c']."&organizer=".$organizer['id']."'>".$activitytemplatecount."</a>";
						else
							echo "<a class='btn btn-xs btn-default' href='o_organizer.php?c=".$_GET['c']."&organizer=".$organizer['id']."'>".$activitytemplatecount."</a>";
						echo '</td>';

						echo '</tr>';
					}
				}
				?>
			</table>
		</div>

		<div class="col-sm-3">
			<?php
			$_GET['activitytemplate'] = (isset($_GET['activitytemplate']) ? $_GET['activitytemplate'] : null);
			if($_GET['organizer'])
			{
				?>
				<legend>TEMPLATE</legend>

				<table class="table table-bordered table-condensed">
					<tr>
						<th>name</th>
						<th class="thincell">&nbsp;</th>
					</tr>

					<?php
					$where = array(
						'organizer_id' => $_GET['organizer'],
						'ORDER' => 'name ASC'
						);
					$activitytemplatelist = Amst::select($_GET['c'].'_activity_template','*',$where);

					foreach ($activitytemplatelist as $activitytemplate) 
					{
						echo '<tr>';

						echo '<td>'.$activitytemplate['name'].'</td>';

						$where = array(
							'activity_template_id' => $activitytemplate['id'],
							);
						$activitytemplateentitycount = Amst::count($_GET['c'].'_activity_template_entity',$where);

						echo '<td>';
						if($activitytemplate['id']==$_GET['activitytemplate'])
							echo "<a class='btn btn-xs btn-primary' href='o_organizer.php?c=".$_GET['c']."&organizer=".$_GET['organizer']."&activitytemplate=".$activitytemplate['id']."'>".$activitytemplateentitycount."</a>";
						else
							echo "<a class='btn btn-xs btn-default' href='o_organizer.php?c=".$_GET['c']."&organizer=".$_GET['organizer']."&activitytemplate=".$activitytemplate['id']."'>".$activitytemplateentitycount."</a>";
						echo '</td>';

						echo '</tr>';
					}
					?>
				</table>
				<?php
			}
			?>
		</div>

		<div class="col-sm-4">
			<?php
			if($_GET['activitytemplate'])
			{
				?>
				<legend>TEMPLATE ENTITY</legend>

				<table class="table table-bordered table-condensed">
					<tr>
						<th class="thincell">abbreviation</th>
						<th>name</th>
					</tr>

					<?php
					$where = array(
						'activity_template_id' => $_GET['activitytemplate'],
						'ORDER' => 'name ASC'
						);
					$activitytemplateentitylist = Amst::select($_GET['c'].'_activity_template_entity','*',$where);
					foreach ($activitytemplateentitylist as $activitytemplateentity) 
					{
						echo '<tr>';

						echo '<td>'.$activitytemplateentity['abbreviation'].'</td>';
						echo '<td>'.$activitytemplateentity['name'].'</td>';

						echo '</tr>';
					}
					?>
				</table>
				<?php
			}
			?>
		</div>
	</div>
</div>

<?php include '../../def/defJS.php'; ?>
<script type="text/javascript">
$(document).ready(function(){

});
</script>

<?php include '../../def/defFooter.php'; ?>