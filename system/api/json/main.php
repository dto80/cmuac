<?php
define('CALLFROMMAIN', TRUE);

include '../../def/defImport.php';
include '../def/output_json.php';

// GET PARAMETER
$apikey = $_REQUEST['apikey'];
$code = $_REQUEST['code'];
$action = $_REQUEST['action'];

// REQUIRED
$callarr = array(
	$apikey,
	$code,
	$action,
	);
if(array_search("", $callarr) !== false)
	repError('Invalid Ajax Call');

// CHECK USER
if(!User::login_api($apikey))
	repError('Verification Failed');

// CHECK FUNCTION
$selectfunc = Func::getFunctionByCode($code);
if(!$selectfunc)
	repError('Function Not Found');

// CHECK OPERATION FILE
if(!file_exists('../../'.Info::$moduleFile[$selectfunc['module']].'/api/json/'.$action.'.php'))
	repError('Action Not Found'.'../../'.Info::$moduleFile[$selectfunc['module']].'/api/json/'.$action.'.php');

include '../../'.Info::$moduleFile[$selectfunc['module']].'/api/json/'.$action.'.php';
?>