<?php
include '../../def/defImport.php';
include '../def/output_json.php';

// GET PARAMETER
$apikey = $_REQUEST['apikey'];
$code = $_REQUEST['code'];
$table = $_REQUEST['table'];

// REQUIRED
$callarr = array(
	$apikey,
	$code
);
if(array_search("", $callarr) !== false)
	repInvalidCall();

// CHECK USER
if(!User::login_api($apikey))
	repVerificationFailed();

// INITIAL DATA
$targettable = $code;
if(isset($table) && $table!='')
	$targettable .= '_'.$table;

$arr = array();

$nameresult = Amst::query("SELECT  `COLUMN_NAME` FROM  `INFORMATION_SCHEMA`.`COLUMNS` WHERE  `TABLE_SCHEMA` =  '".Config::dbname."' AND  `TABLE_NAME` =  '".Info::moduleTablePrefix.$targettable."';");
mysql_data_seek($nameresult, 0);
while($name = mysql_fetch_array($nameresult))
{
	if($name['COLUMN_NAME']=='id') continue;

	if(isset($_REQUEST['add'.$name['COLUMN_NAME']]))
		$arr[$name['COLUMN_NAME']] = $_REQUEST['add'.$name['COLUMN_NAME']];

	if(isset($_POST['add'.$name['COLUMN_NAME']]))
		$arr[$name['COLUMN_NAME']] = $_POST['add'.$name['COLUMN_NAME']];
}

// ADD DATA
$result = Amst::add($targettable,$arr);

// CHECK IF DATA NOT EXISTS
if ($result==0)
	repOperationFailed();

// SHOW RESULT
$resultdata = Amst::getByID($targettable,$result);
$i = 0;
mysql_data_seek($resultdata, 0);
while($array = mysql_fetch_array($resultdata))
{
	mysql_data_seek($nameresult, 0);
	while($name = mysql_fetch_array($nameresult))
		$wrap[$i][$name['COLUMN_NAME']] = $array[$name['COLUMN_NAME']];
	$i++;
}

repOperationSuccess($wrap);
?>