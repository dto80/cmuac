<?php
error_reporting(0);
ini_set('display_errors', 0);

// Allow from any origin
if (isset($_SERVER['HTTP_ORIGIN'])) {
	header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
	header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

	exit(0);
}

// OPERATION
function repError($message)
{
	$output['response_message'] = $message;
	$output['response_status'] = false;

	$code = (isset($_REQUEST['code']) ? $_REQUEST['code'] : '');
	$data = array();
	foreach ($_REQUEST as $key => $value)  
		$data[$key] = gettype($value).' : '.$value;

	$result = Log::addActionLog($code,$message, $data);

	if(!$result)
		$output['response_message'] .= '[LOG ERROR]';

	echo json_encode($output);
	exit();
}

function rep($message, $status, $wrap = array())
{
	$output['response_message'] = $message;
	$output['response_status'] = $status;
	$output['response_rows'] = count($wrap);
	$output['response_data'] = $wrap;

	echo json_encode($output);
	exit();
}

function repNoData($message, $status)
{
	$output['response_message'] = $message;
	$output['response_status'] = $status;

	echo json_encode($output);
	exit();
}

$jsondataarr = json_decode(file_get_contents('php://input'),true);
foreach($jsondataarr as $key => $value)
{
	if(isset($_REQUEST))
		$_REQUEST[$key] = $value;
}
?>