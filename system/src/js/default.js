$(document).ready(function(){
	// tooltip
	$('[data-toggle="tooltip"]').tooltip();

	// data table
	$('.table-data').dataTable({
		"bStateSave": true
	});

	// table row clickable
	$("tr[data-link]").click(function() {
		window.location = this.dataset.link
	});

	// select
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		$('.live').selectpicker('mobile');
	}
	else {
		$('.live').selectpicker({
			liveSearch: true
		});
	}
	
	// focus first input
	$('.modal').on('shown.bs.modal', function() {
		document.activeElement.blur();
		$(this).find(".modal-body :input:visible").first().focus();
	});

	// confirm dialog
	$('form').not('[name*="noconfirm"]').submit(function() {
		var c = confirm("Click OK to continue?");
		return c;
	});

	// save scroll position
	if (typeof $.cookie("scroll") !== undefined ) {
		$(document).scrollTop( $.cookie("scroll") );
	}
	$(window).on("scroll", function() {
		$.cookie("scroll", $(document).scrollTop() );
	});

	// css animated
	$('#logo').addClass('animated flash');
	$('#logo').on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
		$('#logo').removeClass('animated flash');

		setTimeout(function () {
			$('#logo').addClass('animated flash');
		}, 2000);
	});
});