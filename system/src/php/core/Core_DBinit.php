<?php
class DBInit
{	
	static function dropSQLTable()
	{
		// remove associate file from function
		File::deleteAllFile('../../');

		// delete database
		Amst::query("DROP DATABASE IF EXISTS ". Config::dbname .";");

		return true;
	}

	static function regenerateSQLTable()
	{
		//// create Database
		Amst::query("CREATE DATABASE IF NOT EXISTS ". Config::dbname .";");

		//// create System
		Amst::query("CREATE TABLE IF NOT EXISTS `". Config::dbname ."`.`". Info::$sysTable['logaction'] ."` (
			`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
			`userid` BIGINT NOT NULL ,
			`code` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
			`description` VARCHAR(2000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
			`type` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
			`datetime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
			) ENGINE = MYISAM ;");
		Amst::query("CREATE TABLE IF NOT EXISTS `". Config::dbname ."`.`". Info::$sysTable['logsql'] ."` (
			`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
			`userid` BIGINT NOT NULL ,
			`code` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
			`description` VARCHAR(2000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
			`sqlquery` VARCHAR(5000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
			`type` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
			`datetime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
			) ENGINE = MYISAM ;");

		Amst::query("CREATE TABLE IF NOT EXISTS `". Config::dbname ."`.`". Info::$sysTable['function'] ."` (
			`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
			`code` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
			`fullname` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
			`module` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
			`datetime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
			`sortorderid` BIGINT NOT NULL ,
			`userid` BIGINT NOT NULL ,
			UNIQUE ( `code` )
			) ENGINE = MYISAM ;");

		Amst::query("CREATE TABLE IF NOT EXISTS `". Config::dbname ."`.`". Info::$sysTable['user'] ."` (
			`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
			`username` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
			`password` CHAR(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
			`role` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
			`apikey` CHAR(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
			`status` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL Default 'Active' ,
			`register` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
			`lastlogin` TIMESTAMP NOT NULL ,
			`lastlogin_api` TIMESTAMP NOT NULL ,
			UNIQUE ( `username` )
			) ENGINE = MYISAM ;");
		Amst::query("CREATE TABLE IF NOT EXISTS `". Config::dbname ."`.`". Info::$sysTable['userpermission'] ."` (
			`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
			`userid` BIGINT NOT NULL ,
			`functionid` BIGINT NOT NULL
			) ENGINE = MYISAM ;");

		Amst::query("CREATE TABLE IF NOT EXISTS `". Config::dbname ."`.`". Info::$sysTable['minion'] ."` (
			`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,

			`fb_id` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
			`google_id` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
			`twitter_id` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,

			`username` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
			`password` CHAR(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,

			`usertype` VARCHAR(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,

			`name` VARCHAR(70) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
			`surname` VARCHAR(70) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
			`gender` VARCHAR(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
			`birth_day` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
			`birth_month` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
			`birth_year` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
			`address` VARCHAR(300) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
			`tel` VARCHAR(70) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
			`email` VARCHAR(70) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,

			`status` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL Default 'Active' ,
			`register` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
			`lastlogin` TIMESTAMP NOT NULL ,
			UNIQUE ( `username` )
			) ENGINE = MYISAM ;");
		Amst::query("CREATE TABLE IF NOT EXISTS `". Config::dbname ."`.`". Info::$sysTable['minionlog'] ."` (
			`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,

			`minionid` BIGINT NOT NULL ,
			`topiccode` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
			`topicname` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
			`description` VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
			) ENGINE = MYISAM ;");

		//// create Function
		$functionlist = Func::getFunction();

		// initial 
		if($functionlist!=null)
		{
			foreach($functionlist as $function)
			{
				$prependdata = "CREATE TABLE IF NOT EXISTS `". Config::dbname ."`.`". Info::moduleTablePrefix.$function['code'];
				$appenddata = "`insertdatetime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
				`updatedatetime` TIMESTAMP NOT NULL ,
				`sortorderid` BIGINT NOT NULL ,
				`isUser` BOOLEAN NOT NULL DEFAULT 1 ,
				`userid` BIGINT NOT NULL ,
				`version` BIGINT NOT NULL DEFAULT 1 ,
				`status` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL Default 'Active'";
				$modulename = $function['module'];
				include_once '../../'.Info::$moduleFile[$function['module']].'/required/info_module.php';

				if($sqlinfo[$function['module']] != null)
				{
					foreach ($sqlinfo[$function['module']] as $value)
						Amst::query($prependdata.$value);
				}
			}
		}
	}
}
?>