<?php
class File
{	
	/* 
	code convention

	code_id
	code_talble_id
	code_table_type_id
	*/

	static function addImage_base64($code,$base64,$prefixrelative)
	{
		$arr = array(
			"code" => $code,
			"prefixrelative" => $prefixrelative,
			);

		if ( base64_encode(base64_decode($base64, true)) === $base64)
			$arr['base64'] = 'base64 valid';
		else
			$arr['base64'] = 'base64 invalid';

		$image = new ImageManagement();
		if(!$image->load($base64))
		{
			Log::addActionLog('file',"failed add base64 : base64 invalid",$arr);
			return false;
		}

		$savepath = $prefixrelative."conf/file/".$code.'.'.$image->getFileType();
		$arr['imagetype'] = $image->getFileType();
		$arr['savepath'] = $savepath;

		self::delete($code,$prefixrelative);

		if($image->getWidth()>600)
			$image->resizeToWidth(600);

		if(!$image->save($savepath))
		{
			Log::addActionLog('file',"failed add base64 : Unable to save data",$arr);
			return false;
		}
		return true;
	}

	static function addFile($code,$file,$prefixrelative)
	{
		if($file["name"]!='' && !is_null($file["name"]) && !is_null($file))
		{
			self::delete($code,$prefixrelative);

			$arr = array(
				"code" => $code,
				"filename" => $file["name"],
				"filetmp_name" => $file["tmp_name"],
				"extension" => $extension,
				"size" => $file["size"],
				);

			$allowedExts = array("PDF", "DOC", "DOCX", "pdf", "doc", "docx");
			$extension = end(explode(".", $file["name"]));

			if (($file["type"] == "application/pdf")
				|| ($file["type"] == "application/rtf"))
			{
				if(in_array($extension, $allowedExts))
				{
					if ($file["error"] > 0)
					{
						Log::addActionLog('file',"failed add file : file error",$arr);
						return false;
					}
					else
					{
						if (!is_uploaded_file($file['tmp_name']))
						{
							Log::addActionLog('file',"failed add file : file is not upload properly",$arr);
							return false;
						}

						$result = move_uploaded_file($file["tmp_name"], $prefixrelative."conf/file/".$code.".". $extension);

						if(!$result)
							Log::addActionLog('file',"failed add file : move_uploaded_file not done [error:".$file['error']."]",$arr);

						return $result;
					}
				}
				else
				{
					Log::addActionLog('file',"failed add file : extension not allowed",$arr);
					return false;
				}
			}
			else
			{
				Log::addActionLog('file',"failed add file : file type not allowed",$arr);
				return false;
			}
		}
	}

	static function addImage($code,$file,$prefixrelative)
	{
		if($file["name"]!='' && !is_null($file["name"]) && !is_null($file))
		{
			self::delete($code,$prefixrelative);

			$allowedExts = array("JPG", "JPEG", "GIF", "PNG", "jpg", "jpeg", "gif", "png");
			$extension = end(explode(".", $file["name"]));

			$arr = array(
				"code" => $code,
				"filename" => $file["name"],
				"filetmp_name" => $file["tmp_name"],
				"extension" => $extension,
				"size" => $file["size"],
				);

			if (($file["type"] == "image/gif")
				|| ($file["type"] == "image/png")
				|| ($file["type"] == "image/jpg")
				|| ($file["type"] == "image/jpeg"))
			{
				if(in_array($extension, $allowedExts))
				{
					if ($file["error"] > 0)
					{
						Log::addActionLog('file',"failed add file : file error",$arr);
						return false;
					}
					else
					{
						if (!is_uploaded_file($file['tmp_name']))
						{
							Log::addActionLog('file',"failed add file : file is not upload properly",$arr);
							return false;
						}

						$image = new ImageManagement($file["tmp_name"]);
						if($image->getWidth()>600)
						{
							$image->resizeToWidth(600);
							$image->save($file["tmp_name"]);
						}
						
						$result = move_uploaded_file($file["tmp_name"], $prefixrelative."conf/file/".$code.".". $extension);

						if(!$result)
							Log::addActionLog('file',"failed add image : move_uploaded_file not done [error:".$file['error']."]",$arr);

						return $result;
					}
				}
				else
				{
					Log::addActionLog('file',"failed add image : extension not allowed",$arr);
					return false;
				}
			}
			else
			{
				Log::addActionLog('file',"failed add image : file type allowed",$arr);
				return false;
			}
		}
	}

	static function addImageAndResize($code,$file,$prefixrelative,$width,$height)
	{
		if($file["name"]!='' && !is_null($file["name"]) && !is_null($file))
		{
			self::delete($code,$prefixrelative);
			
			$arr = array(
				"code" => $code,
				"filename" => $file["name"],
				"filetmp_name" => $file["tmp_name"],
				"extension" => $extension,
				"size" => $file["size"],
				);

			$allowedExts = array("JPG", "JPEG", "GIF", "PNG", "jpg", "jpeg", "gif", "png");
			$extension = end(explode(".", $file["name"]));

			if (($file["type"] == "image/gif")
				|| ($file["type"] == "image/png")
				|| ($file["type"] == "image/jpg")
				|| ($file["type"] == "image/jpeg"))
			{
				if(in_array($extension, $allowedExts))
				{
					if ($file["error"] > 0)
					{
						Log::addActionLog('file',"failed add file : file error",$arr);
						return false;
					}
					else
					{
						if (!is_uploaded_file($file['tmp_name']))
						{
							Log::addActionLog('file',"failed add file : file is not upload properly",$arr);
							return false;
						}

						$image = new ImageManagement($file["tmp_name"]);
						if($width==0 && $height>0)
							$image->resizeToHeight($height);
						else if($height==0 && $width>0)
							$image->resizeToWidth($width);
						else if($height>0 && $width>0)
							$image->resize($width,$height);
						$image->save($file["tmp_name"]);
						
						$result = move_uploaded_file($file["tmp_name"], $prefixrelative."conf/file/".$code.".". $extension);

						if(!$result)
							Log::addActionLog('file',"failed add file : move_uploaded_file not done [error:".$file['error']."]",$arr);

						return $result;
					}
				}
				else
				{
					Log::addActionLog('file',"failed add file : extension not allowed",$arr);
					return false;
				}
			}
			else
			{
				Log::addActionLog('file',"failed add file : file type allowed",$arr);
				return false;
			}
		}
	}

	static function copy($code1,$code2,$prefixrelative)
	{
		$oldfilepath = File::getPath($code1);
		$oldfileextension = end(explode(".", $oldfilepath));

		return copy($oldfilepath, $prefixrelative.'conf/file/'.$code2.'.'.$oldfileextension);
	}

	static function getPath($code,$prefixrelative) // get file path
	{
		$file = glob($prefixrelative.'conf/file/'. $code .'.*');

		if(count($file)!=1)
			return null;

		$filedata = explode("/", $file[0]);
		$filename = end($filedata);

		if($filename=='')
			return null;

		return ROOT_URL.'/conf/file/'.$filename;
	}

	static function delete($code,$prefixrelative)
	{
		$files = glob($prefixrelative.'conf/file/'. $code .'.*');
		foreach($files as $file)
		{
			if(is_file($file))
				unlink($file);
		}
	}

	// recieve only $_GET['c']
	static function deleteAllFile($prefixrelative)
	{
		$files = glob($prefixrelative.'conf/file/*'); // get all file names
		foreach($files as $file)
		{
			if(is_file($file))
				unlink($file);
		}
		$files = glob($prefixrelative.'conf/tinymce/*'); // get all file names
		foreach($files as $file)
		{
			if(is_file($file))
				unlink($file);
		}
	}
}
?>