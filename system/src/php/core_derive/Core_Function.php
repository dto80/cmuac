<?php
class Func
{	
	static function addFunction($code, $fullname, $module)
	{
		$arr = array(
			"code" => $code,
			"fullname" => $fullname,
			"module" => $module,
			"userid" => User::getCurrentUserID()
			);
		$insertid = DB::insert(Info::$sysTable['function'], $arr);

		if(!$insertid) {
			Log::addActionLog("system","FAILED to ADD function Name[$fullname] Code[$code].",$arr);
			return false;
		}
		Log::addActionLog("system","ADD function #[$fullname] to the system.",$arr);

		$data = array('sortorderid' => $insertid);
		DB::update(Info::$sysTable['function'], $data, array('id'=>$insertid));

		return true;
	}

	static function editFunction($functionid, $fullname) 
	{
		$arr = array(
			"id" => $functionid,
			"fullname" => $fullname
			);
		$updatenum = DB::update(Info::$sysTable['function'], $arr, array('id'=>$functionid));

		if(!$updatenum)
			return false; // function not existed

		Log::addActionLog("system","RENAME function #$functionid to name[$fullname]",$arr);

		return true;
	}

	static function removeFunction($functionid, $module, $code, $fullname)
	{
		$removenum = DB::delete(Info::$sysTable["function"], array('id'=>$functionid));

		if(!$removenum)
			return false; // error

		Log::addActionLog("system","REMOVE function #[$fullname] from the system.",null);

		File::delete($code,'../../');

		$modulename = $module;
		include_once '../../'.Info::$moduleFile[$module].'/required/info_module.php';
		foreach ($sqlinfo[$module] as $key => $value)
			Amst::query('DROP TABLE IF EXISTS '.Info::moduleTablePrefix.$code.$key.';');

		Log::addActionLog("system","REMOVE function #[$fullname] data.",null);

		return true;
	}

	static function swapFunction($currentid, $movetoid)
	{
		$arr = array(
			'currentid' => $currentid,
			'movetoid' => $movetoid
			);

		$currententity = DB::get(Info::$sysTable["function"], "sortorderid", array("id" => $currentid));
		$movetoentity = DB::get(Info::$sysTable["function"], "sortorderid", array("id" => $movetoid));

		$currentarr = array( 'sortorderid' => $currententity['sortorderid'] );
		$movetoarr = array( 'sortorderid' => $movetoentity['sortorderid'] );

		if(!DB::update(Info::$sysTable["function"], $movetoarr, array('id'=>$currentid)))
		{
			Log::addActionLog("system","FAILED to SWAP function id [".$currentid."],[".$movetoid."] in the system.",null);
			return false;
		}
		DB::update(Info::$sysTable["function"], $currentarr, array('id'=>$movetoid));

		Log::addActionLog("system","SWAP function id [".$currentid."],[".$movetoid."] in the system.",null);
		return true;
	}

	static function getFunction()
	{
		$where = array('ORDER' => 'sortorderid ASC');
		return DB::select(Info::$sysTable['function'], '*', $where);
	}

	static function getFunctionByID($id)
	{
		return DB::get(Info::$sysTable['function'], '*', array('id'=>$id));
	}

	static function getFunctionByCode($code)
	{
		return DB::get(Info::$sysTable['function'], '*', array('code'=>$code));
	}
}
?>