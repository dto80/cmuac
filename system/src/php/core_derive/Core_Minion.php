<?php
class Minion
{
	static function login($username, $password)
	{
		$arr = array("username" => $username);
		$minion = DB::get(Info::$sysTable['minion'], '*', $arr);

		if (!$minion)
			return "Invalid username or password"; 	// wrong username

		if ($minion['password'] != md5($password))
			return "Invalid username or password";	// wrong password

		// login successful //

		// check banned
		if($minion['status']=='Banned')
			return "You've been banned from the system";
		else if($minion['status']=='Deleted')
			return "You've been deleted from the system";

		// update userlogin date
		$arr = array("#lastlogin" => 'NOW()');
		$result = DB::update(Info::$sysTable['minion'], $arr, array('id'=>$minion['id']));
		$error = DB::error();
		if($error[0] != '00000')
			Log::addActionLog("system","Minion unable to update lastest login time to : ". $minion['username'],DB::error());

		return 'success';
	}

	static function register($arr)
	{
		$arr['status'] = 'Active';

		$result = DB::insert(Info::$sysTable['minion'], $arr);

		if(!$result) {
			Log::addActionLog("system","Minion anonymous Name[".$arr['username']."] failed to register.",$arr);
			return $result;
		}

		if(User::getCurrentUsername()==Config::adminid)
			Log::addActionLog("system","Minion [". $arr['username'] ."] summon to the system",$arr);
		else if(User::getCurrentUserid()!=null || User::getCurrentUserid()!="")
			Log::addActionLog("system","Minion [". User::getCurrentUsername() ."] add [". $arr['username'] ."] to the system",$arr);
		else
			Log::addActionLog("system","Minion [". $arr['username'] ."] register to the system",$arr);

		return $result;
	}

	static function editMinion($minionid, $arr)
	{
		$result = self::getMinionByID($minionid);

		if (!$result)
			return false; // minion not existed

		if($arr['username']==$result['username'])
			unset($arr['username']);
		if($arr['password']==$result['password'] || $arr['password']=='')
			unset($arr['password']);
		else
			$arr['password'] = md5($arr['password']);

		$result = DB::update(Info::$sysTable['minion'], $arr, array('id'=>$minionid));

		if(!$result)
			return false; // error

		if(User::getCurrentUsername()==Config::adminid)
			Log::addActionLog("system","Minion system change userid[$minionid] detail to",$arr);
		else if($userid==self::getCurrentUserid())
			Log::addActionLog("system","Minion userid[$minionid] change own detail to",$arr);
		else
			Log::addActionLog("system","Minion userid[". User::getCurrentUserid() ."] change [$minionid] detail to",$arr);

		return true;
	}

	static function getMinion()
	{
		return DB::select(Info::$sysTable['minion'] , '*');
	}

	static function getMinionByList($where)
	{
		return DB::select(Info::$sysTable['minion'] , '*', $where);
	}

	static function getMinionByID($minionid)
	{
		return DB::get(Info::$sysTable['minion'], '*', array('id' => $minionid));
	}

	static function getMinionByUsername($username)
	{
		return DB::get(Info::$sysTable['minion'], '*', array('username' => $username));
	}

	// LOG
	static function addMinionLog($data)
	{
		return DB::insert(Info::$sysTable['minionlog'], $data);
	}

	static function getMinionLogByID($minionid)
	{
		return DB::select(Info::$sysTable['minionlog'], '*', array('minionid' => $minionid));
	}
}
?>