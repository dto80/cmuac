<?php
class User
{
	static function login_server($username, $password)
	{
		if($username == Config::adminid && md5($password) == Config::adminpw)
		{
			$_SESSION['amst_userid'] = 0;
			$_SESSION['amst_username'] = Config::adminid;
			$_SESSION['amst_userhash'] = Config::adminpw;
			$_SESSION['amst_userRole'] = Config::adminrole;
			$_SESSION['amst_logintype'] = 'server';
			$_SESSION['amst_isLogin'] = true;

			return "success";
		}

		$arr = array(
			"username" => $username
			);
		$user = DB::get(Info::$sysTable['user'], '*', $arr);

		if (!$user)
			return "Invalid username or password"; 	// wrong username

		if ($user['password'] != md5($password))
			return "Invalid username or password";	// wrong password

		// login successful //

		// check banned
		if($user['status']=='Banned')
			return "You've been banned from the system";
		else if($user['status']=='Deleted')
			return "You've been deleted from the system";

		// save to session
		$_SESSION['amst_userid'] = $user['id'];
		$_SESSION['amst_username'] = $user['username'];
		$_SESSION['amst_userhash'] = $user['password'];
		$_SESSION['amst_userRole'] = $user['role'];
		$_SESSION['amst_logintype'] = 'server';
		$_SESSION['amst_isLogin'] = true;

		// update userlogin date
		$arr = array("#lastlogin" => 'NOW()');
		$result = DB::update(Info::$sysTable['user'], $arr, array('id'=>$user['id']));
		$error = DB::error();
		if($error[0] != '00000')
			Log::addActionLog("system","User unable to update lastest login time to : ". $user['username'],DB::error());

		return 'success';
	}

	static function relogin_server($userid, $username, $role, $userhash)
	{
		if($username == Config::adminid && $userhash == Config::adminpw)
		{
			$_SESSION['amst_userid'] = 0;
			$_SESSION['amst_username'] = Config::adminid;
			$_SESSION['amst_userhash'] = Config::adminpw;
			$_SESSION['amst_userRole'] = Config::adminrole;
			$_SESSION['amst_logintype'] = 'server';
			$_SESSION['amst_isLogin'] = true;
			
			return true;
		}

		if($userid == 0)
			return false;

		$arr = array(
			"username" => $username
			);
		$user = DB::get(Info::$sysTable['user'], '*', $arr);	

		if (!$user)
			return false; 	// wrong username

		if ($user['password'] != $userhash)
			return;	// wrong password	

		$_SESSION['amst_userid'] = $userid;
		$_SESSION['amst_username'] = $username;
		$_SESSION['amst_userhash'] = $userhash;
		$_SESSION['amst_userRole'] = $role;
		$_SESSION['amst_logintype'] = 'server';
		$_SESSION['amst_isLogin'] = true;

		return true;
	}

	static function login_api($apikey)
	{
		$arr = array(
			"apikey" => $apikey
			);
		$user = DB::get(Info::$sysTable['user'],'*',$arr);

		if (!$user)
			return false; 	// wrong api key

		// login successful //

		// save to session
		$_SESSION['amst_userid'] = $user['id'];
		$_SESSION['amst_username'] = $user['username'];
		$_SESSION['amst_userRole'] = $user['role'];
		$_SESSION['amst_logintype'] = 'api';

		// update userlogin date
		$arr = array(
			"#lastlogin_api" => "NOW()"
			);
		$result = DB::update(Info::$sysTable['user'], $arr, array('id'=>$user['id']));
		$error = DB::error();
		if($error[0] != '00000')
			Log::addActionLog("system","User unable to update lastest api login time to : ". $user['username'],DB::error());

		return true;
	}

	static function logout()
	{
		if(isset($_SESSION['amst_userid']))
			unset($_SESSION['amst_userid']);
		if(isset($_SESSION['amst_username']))
			unset($_SESSION['amst_username']);
		if(isset($_SESSION['amst_userhash']))
			unset($_SESSION['amst_userhash']);
		if(isset($_SESSION['amst_userRole']))
			unset($_SESSION['amst_userRole']);
		if(isset($_SESSION['amst_logintype']))
			unset($_SESSION['amst_logintype']);
		if(isset($_SESSION['amst_isLogin']))
			unset($_SESSION['amst_isLogin']);

		if(isset($_COOKIE['amst_userid']))
			setcookie("amst_userid", "", time()-360000,'/');
		if(isset($_COOKIE['amst_username']))
			setcookie("amst_username", "", time()-360000,'/');
		if(isset($_COOKIE['amst_userhash']))
			setcookie("amst_userhash", "", time()-360000,'/');
		if(isset($_COOKIE['amst_userRole']))
			setcookie("amst_userRole", "", time()-360000,'/');
		if(isset($_COOKIE['amst_logintype']))
			setcookie("amst_logintype", "", time()-360000,'/');
		if(isset($_COOKIE['amst_isLogin']))
			setcookie("amst_isLogin", "", time()-360000,'/');
	}

	static function getCurrentUserid()
	{
		if(isset($_SESSION['amst_userid']))
			return $_SESSION['amst_userid'];
		else
			return -1;
	}

	static function getCurrentUsername()
	{
		if(isset($_SESSION['amst_username']))
			return $_SESSION['amst_username'];
		else
			return null;
	}

	static function getCurrentUserHash()
	{
		if(isset($_SESSION['amst_userhash']))
			return $_SESSION['amst_userhash'];
		else
			return null;
	}

	static function getCurrentUserRole()
	{
		if(isset($_SESSION['amst_userRole']))
			return $_SESSION['amst_userRole'];
		else
			return null;
	}

	static function getCurrentUserLogintype()
	{
		if(isset($_SESSION['amst_logintype']))
			return $_SESSION['amst_logintype'];
		else
			return 'unknown';
	}

	static function isLogin()
	{
		if(isset($_SESSION['amst_isLogin']))
			return $_SESSION['amst_isLogin'];
		else
			return null;
	}

	static function register($username, $password, $role)
	{
		if($username==Config::adminid)
			return false;

		$arr = array(
			'username' => $username,
			'password' => md5($password),
			'role' => $role,
			'apikey' => md5($username.'api'),
			'status' => 'Active'
			);
		$result = DB::insert(Info::$sysTable['user'], $arr);

		if(!$result) {
			Log::addActionLog("system","User anonymous Name[".$username."] failed to register.",$arr);
			return $result;
		}

		if(self::getCurrentUsername()==Config::adminid)
			Log::addActionLog("system","User [". $username ."] summon to the system",$arr);
		else if(self::getCurrentUserid()!=null || self::getCurrentUserid()!="")
			Log::addActionLog("system","User [". self::getCurrentUsername() ."] add [". $username ."] to the system",$arr);
		else
			Log::addActionLog("system","User [". $username ."] register to the system",$arr);

		return $result;
	}

	static function editUser($userid, $arr)
	{
		$result = self::getUserByID($userid);

		if (!$result)
			return false; // user not existed

		if($arr['username']==$result['username'])
			unset($arr['username']);
		if($arr['password']==$result['password'] || $arr['password']=='')
			unset($arr['password']);
		else
			$arr['password'] = md5($arr['password']);
		if($arr['role']=='')
			unset($arr['role']);
		if($arr['status']=='')
			unset($arr['status']);

		$result = DB::update(Info::$sysTable['user'], $arr, array('id'=>$userid));

		if(!$result)
			return false; // error

		if(self::getCurrentUsername()==Config::adminid)
			Log::addActionLog("system","User system change userid[$userid] detail to",$arr);
		else if($userid==self::getCurrentUserid())
			Log::addActionLog("system","User userid[$userid] change own detail to",$arr);
		else
			Log::addActionLog("system","User userid[". self::getCurrentUserid() ."] change [$userid] detail to",$arr);

		return true;
	}

	static function getUser()
	{
		return DB::select(Info::$sysTable['user'] , '*');
	}

	static function getUserByList($where)
	{
		return DB::select(Info::$sysTable['user'] , '*', $where);
	}

	static function getUserByID($userid)
	{
		return DB::get(Info::$sysTable['user'], '*', array('id' => $userid));
	}

	static function getUserByUsername($username)
	{
		return DB::get(Info::$sysTable['user'], '*', array('username' => $username));
	}

	static function getUserByAPIKey($apikey)
	{
		return DB::get(Info::$sysTable['user'], '*', array('apikey' => $apikey));
	}





	// user permission
	static function addUserPermission($userid,$functionid)
	{
		$arr = array(
			'userid' => $userid,
			'functionid' => $functionid
			);
		return DB::insert(Info::$sysTable['userpermission'], $arr);
	}

	static function removeUserPermission($id)
	{
		return DB::delete(Info::$sysTable['userpermission'],array('id'=>$id));
	}

	static function getUserPermission()
	{
		return DB::select(Info::$sysTable['userpermission'], '*');
	}

	static function getUserPermissionByList($where)
	{
		return DB::select(Info::$sysTable['userpermission'], '*', $where);
	}





	// tool
	static function isUserHavePermission($userid,$functionid)
	{
		$selectuser = User::getUserByID($userid);

		if($selectuser['role']=='Mod')
		{
			$where = array(
				'AND' => array(
					'userid' => $userid,
					'functionid' => $functionid
					)
				);
			return DB::get(Info::$sysTable['userpermission'], '*', $where);
		}
		else
			return true;
	}
}
?>