<?php
class Info
{
	//// system config
	const keepNormalActivityLog = false;
	const keepErrorActivityLog = true;
	// upfirst then down
	const keepServerActivityLog = true;
	const keepServerSQLLog = true;
	const keepAPIActivityLog = true;
	const keepAPISQLLog = true;

	const maximumActivityLog = 200;

	// system
	// map relative between [classname => file] *only class
	static $sysFile = array(
		// main Info
		"Info" => "Info",

		// config
		"Config-Server" => "conf/config-server",
		"Config-Localhost" => "conf/config-localhost",

		// core
		"DB" => "src/php/core/Core_DB",
		"DBInit" => "src/php/core/Core_DBinit",

		// core derive
		"Amst" => "src/php/core_derive/Core_Amst",
		"File" => "src/php/core_derive/Core_File",
		"Func" => "src/php/core_derive/Core_Function",
		"Log" => "src/php/core_derive/Core_Log",
		"User" => "src/php/core_derive/Core_User",
		"Minion" => "src/php/core_derive/Core_Minion",

		// lib
		"ImageManagement" => "src/php/core_other/Lib_image",
		);

	// map relative between [name => file] *only non-class
	static $sysOtherFile = array(
		// main Info
		"json-get-getbyid" => "api/json-get/getbyid",
		);
	
	// map relative between [functiontable => tablename]
	static $sysTable = array(
		"logaction" => "sys_log_action",
		"logsql" => "sys_log_sql",
		"function" => "sys_function",
		"user" => "sys_user",
		"userpermission" => "sys_user_permission",
		"minion" => "sys_minion",
		"minionlog" => "sys_minionlog",
		);
	// end

	//// module
	// module prefix
	const moduleTablePrefix = "func_";

	// map relative between [modulename => file]
	static $moduleFile = array(
		"Content" => "mod/Content",
		"ImageCode" => "mod/ImageCode",
		"SlideShow" => "mod/SlideShow",

		// SYS module
		"User" => "mod_sys/User",
		"Minion" => "mod_sys/Minion",

		// EX module
		"SMSm" => "mod_ex/SMSm",
		"AccGen" => "mod_ex/AccGen",
		
		"AbleOrder" => "mod_ex/AbleOrder",
		"Alice" => "mod_ex/Alice",
		
		"CMTrail" => "mod_ex/CMTrail",
		"CMUActivity" => "mod_ex/CMUActivity",
		"MuakPOS" => "mod_ex/MuakPOS",
		"DangBakery" => "mod_ex/DangBakery",
		"WisdomLocation" => "mod_ex/WisdomLocation",
		"GoGym" => "mod_ex/GoGym",

		"ExamM" => "mod_ex/ExamM",
		"StockBill" => "mod_ex/StockBill",
		"StockPrisoner" => "mod_ex/StockPrisoner",
		"StockS" => "mod_ex/StockS",
		"Company" => "mod_ex/Company",
		"Gas" => "mod_ex/Gas",

		"JoinTour" => "mod_ex/JoinTour",

		"ProudFront" => "mod_ex/ProudFront",
		"ProudMagazine" => "mod_ex/ProudMagazine",
		"LemonProperty" => "mod_ex/LemonProperty",
		"SmileCastle" => "mod_ex/SmileCastle",
		"SmileRestaurant" => "mod_ex/SmileRestaurant",
		"SmileFront" => "mod_ex/SmileFront",
		"Company" => "mod_ex/Company",
		);
	// end
}
?>