-- Adminer 4.2.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `func_znrrb_activity`;
CREATE TABLE `func_znrrb_activity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `academic_year` int(11) NOT NULL,
  `activity_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `actual_budget` decimal(12,2) DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `estimate_budget` decimal(12,2) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_of_hours` decimal(12,2) DEFAULT NULL,
  `objective` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `semester` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `target` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `activity_id` bigint(20) DEFAULT NULL,
  `assessment_id` bigint(20) DEFAULT NULL,
  `assessment_form_id` bigint(20) DEFAULT NULL,
  `organizer_id` bigint(20) DEFAULT NULL,
  `registration_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=tis620;


DROP TABLE IF EXISTS `func_znrrb_activity_template`;
CREATE TABLE `func_znrrb_activity_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `organizer_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=tis620;

INSERT INTO `func_znrrb_activity_template` (`id`, `name`, `organizer_id`) VALUES
(8,	'Template name 1',	18),
(9,	'Template name 2',	18),
(10,	'Template name 3',	18),
(11,	'G',	18);

DROP TABLE IF EXISTS `func_znrrb_activity_template_entity`;
CREATE TABLE `func_znrrb_activity_template_entity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `activity_template_id` bigint(20) DEFAULT NULL,
  `abbreviation` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=tis620;

INSERT INTO `func_znrrb_activity_template_entity` (`id`, `activity_template_id`, `abbreviation`, `name`) VALUES
(2,	8,	'TH',	'TEST1'),
(3,	10,	'a',	'A'),
(4,	10,	'b',	'B'),
(5,	10,	'f',	'F'),
(6,	10,	'd',	'D'),
(7,	8,	'a',	'A'),
(8,	8,	'b',	'B');

DROP TABLE IF EXISTS `func_znrrb_faculty`;
CREATE TABLE `func_znrrb_faculty` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=tis620;

INSERT INTO `func_znrrb_faculty` (`id`, `name`) VALUES
(21,	'CAMT'),
(11,	'DENT'),
(12,	'ECON'),
(13,	'fdsa'),
(14,	'qwerty'),
(15,	'xxcv'),
(16,	'qzqz');

DROP TABLE IF EXISTS `func_znrrb_organizer`;
CREATE TABLE `func_znrrb_organizer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `faculty_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=tis620;

INSERT INTO `func_znrrb_organizer` (`id`, `name`, `faculty_id`) VALUES
(1,	'test1',	11),
(2,	'test2',	11),
(3,	'test3',	11),
(17,	'test1',	16),
(6,	'test1',	13),
(7,	'test1',	14),
(9,	'test3',	12),
(10,	'test2',	13),
(21,	'test3',	13),
(18,	'test1',	21),
(20,	'test4',	12);

DROP TABLE IF EXISTS `func_znrrb_staff`;
CREATE TABLE `func_znrrb_staff` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `faculty_id` bigint(20) NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tel` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=tis620;

INSERT INTO `func_znrrb_staff` (`id`, `faculty_id`, `email`, `name`, `tel`) VALUES
(1,	11,	'Grongtong@gotmail.com',	'กรองทอง ปัญญาโน',	'0832123212'),
(2,	12,	'amstre.t@gmail.com',	'เจษฏา ศรีบุญวรกุล',	'0610698890'),
(3,	12,	'max.demonic@gmail.com',	'Chantanat Sensupa',	'0123545678');

DROP TABLE IF EXISTS `func_znrrb_staff_organizer`;
CREATE TABLE `func_znrrb_staff_organizer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `staff_id` bigint(20) NOT NULL,
  `organizer_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=tis620;

INSERT INTO `func_znrrb_staff_organizer` (`id`, `staff_id`, `organizer_id`) VALUES
(34,	2,	18),
(33,	2,	1),
(32,	2,	3),
(31,	2,	2),
(38,	3,	2),
(37,	3,	1),
(39,	3,	9),
(40,	3,	20);

DROP TABLE IF EXISTS `sys_function`;
CREATE TABLE `sys_function` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `module` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sortorderid` bigint(20) NOT NULL,
  `userid` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=tis620;

INSERT INTO `sys_function` (`id`, `code`, `fullname`, `module`, `datetime`, `sortorderid`, `userid`) VALUES
(1,	'znrrb',	'cmu',	'CMUActivity',	'2015-10-03 17:23:50',	1,	0);

DROP TABLE IF EXISTS `sys_log_action`;
CREATE TABLE `sys_log_action` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userid` bigint(20) NOT NULL,
  `code` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(800) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=tis620;

INSERT INTO `sys_log_action` (`id`, `userid`, `code`, `description`, `type`, `datetime`) VALUES
(135,	-1,	'',	'Invalid ajax call<dl class=\"dl-horizontal\"><dt>REQUEST_apikey</dt><dd>cfcd208495d565ef66e7dff9f98764da</dd><dt>GET_apikey</dt><dd>cfcd208495d565ef66e7dff9f98764da</dd><dt>json</dt><dd>[]</dd><dt>json_another</dt><dd>{\"name\":\"ter\"}</dd></dl><br /><br />http://amstre.com/project/studentacc/mod_sm/CMUActivity/api/json/activitytemplateadd.php?apikey=cfcd208495d565ef66e7dff9f98764da',	'unknown',	'2015-10-24 15:15:55'),
(136,	-1,	'',	'Invalid ajax call<dl class=\"dl-horizontal\"><dt>REQUEST_apikey</dt><dd>cfcd208495d565ef66e7dff9f98764da</dd><dt>GET_apikey</dt><dd>cfcd208495d565ef66e7dff9f98764da</dd><dt>json</dt><dd>[]</dd><dt>json_another</dt><dd>{\"name\":\"ter\"}</dd></dl><br /><br />http://amstre.com/project/studentacc/mod_sm/CMUActivity/api/json/activitytemplateadd.php?apikey=cfcd208495d565ef66e7dff9f98764da',	'unknown',	'2015-10-24 15:16:05'),
(137,	-1,	'',	'Invalid ajax call<dl class=\"dl-horizontal\"><dt>REQUEST_apikey</dt><dd>cfcd208495d565ef66e7dff9f98764da</dd><dt>GET_apikey</dt><dd>cfcd208495d565ef66e7dff9f98764da</dd><dt>json</dt><dd>[]</dd><dt>json_another</dt><dd>{\"name\":\"ter\"}</dd></dl><br /><br />http://amstre.com/project/studentacc/mod_sm/CMUActivity/api/json/activitytemplateadd.php?apikey=cfcd208495d565ef66e7dff9f98764da',	'unknown',	'2015-10-24 15:57:39'),
(138,	-1,	'',	'Invalid ajax call<dl class=\"dl-horizontal\"><dt>REQUEST_apikey</dt><dd>cfcd208495d565ef66e7dff9f98764da</dd><dt>GET_apikey</dt><dd>cfcd208495d565ef66e7dff9f98764da</dd><dt>json</dt><dd>[]</dd><dt>json_another</dt><dd>{\"name\":\"ter\"}</dd></dl><br /><br />http://amstre.com/project/studentacc/mod_sm/CMUActivity/api/json/activitytemplateadd.php?apikey=cfcd208495d565ef66e7dff9f98764da',	'unknown',	'2015-10-24 15:57:42'),
(139,	-1,	'',	'Invalid ajax call<dl class=\"dl-horizontal\"><dt>REQUEST_apikey</dt><dd>cfcd208495d565ef66e7dff9f98764da</dd><dt>GET_apikey</dt><dd>cfcd208495d565ef66e7dff9f98764da</dd><dt>json</dt><dd>[]</dd><dt>json_another</dt><dd>{\"name\":\"ter\"}</dd></dl><br /><br />http://amstre.com/project/studentacc/mod_sm/CMUActivity/api/json/activitytemplateadd.php?apikey=cfcd208495d565ef66e7dff9f98764da',	'unknown',	'2015-10-24 16:21:33'),
(140,	-1,	'',	'Invalid ajax call<dl class=\"dl-horizontal\"><dt>REQUEST_apikey</dt><dd>cfcd208495d565ef66e7dff9f98764da</dd><dt>GET_apikey</dt><dd>cfcd208495d565ef66e7dff9f98764da</dd><dt>json</dt><dd>[]</dd><dt>json_another</dt><dd>{\"name\":\"ter\"}</dd></dl><br /><br />http://amstre.com/project/studentacc/mod_sm/CMUActivity/api/json/activitytemplateadd.php?apikey=cfcd208495d565ef66e7dff9f98764da',	'unknown',	'2015-10-24 16:40:38');

DROP TABLE IF EXISTS `sys_log_sql`;
CREATE TABLE `sys_log_sql` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userid` bigint(20) NOT NULL,
  `code` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(300) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sqlquery` varchar(5000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=tis620;

INSERT INTO `sys_log_sql` (`id`, `userid`, `code`, `description`, `sqlquery`, `type`, `datetime`) VALUES
(107,	0,	'',	'Column \'userid\' cannot be null<br /><br />LINK : http://amstre.com/project/studentacc/mod_sm/CMUActivity/api/json/activitytemplateadd.php?apikey=cfcd208495d565ef66e7dff9f98764da<br /><br />FILE : Core_DB.php [761]<br />FUNCTION : exec<br />-------------------<br />FILE : Core_Log.php [37]<br />FUNCT',	'INSERT INTO \"sys_log_action\" (\"userid\", \"code\", \"description\", \"type\") VALUES (NULL, \'\', \'Invalid ajax call<dl class=\\\"dl-horizontal\\\"><dt>REQUEST_apikey</dt><dd>cfcd208495d565ef66e7dff9f98764da</dd><dt>GET_apikey</dt><dd>cfcd208495d565ef66e7dff9f98764da</dd><dt>json</dt><dd>[]</dd></dl><br /><br />http://amstre.com/project/studentacc/mod_sm/CMUActivity/api/json/activitytemplateadd.php?apikey=cfcd208495d565ef66e7dff9f98764da\', \'unknown\')',	'user',	'2015-10-24 15:04:54'),
(108,	0,	'',	'Column \'userid\' cannot be null<br /><br />LINK : http://amstre.com/project/studentacc/mod_sm/CMUActivity/api/json/activitytemplateadd.php?apikey=cfcd208495d565ef66e7dff9f98764da<br /><br />FILE : Core_DB.php [761]<br />FUNCTION : exec<br />-------------------<br />FILE : Core_Log.php [37]<br />FUNCT',	'INSERT INTO \"sys_log_action\" (\"userid\", \"code\", \"description\", \"type\") VALUES (NULL, \'\', \'Invalid ajax call<dl class=\\\"dl-horizontal\\\"><dt>REQUEST_apikey</dt><dd>cfcd208495d565ef66e7dff9f98764da</dd><dt>GET_apikey</dt><dd>cfcd208495d565ef66e7dff9f98764da</dd><dt>json</dt><dd>[]</dd><dt>json_another</dt><dd>{\\\"name\\\":\\\"ter\\\"}</dd></dl><br /><br />http://amstre.com/project/studentacc/mod_sm/CMUActivity/api/json/activitytemplateadd.php?apikey=cfcd208495d565ef66e7dff9f98764da\', \'unknown\')',	'user',	'2015-10-24 15:08:30');

DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `apikey` char(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `register` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastlogin` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastlogin_api` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=tis620;

INSERT INTO `sys_user` (`id`, `username`, `password`, `role`, `apikey`, `status`, `register`, `lastlogin`, `lastlogin_api`) VALUES
(1,	'max',	'2ffe4e77325d9a7152f7086ea7aa5114',	'Dev',	'cfcd208495d565ef66e7dff9f98764da',	'Active',	'2015-10-03 18:26:05',	'2015-10-21 12:32:22',	'2015-10-24 16:48:32');

DROP TABLE IF EXISTS `sys_user_info`;
CREATE TABLE `sys_user_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userid` bigint(20) NOT NULL,
  `topic` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sortorderid` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=tis620;


DROP TABLE IF EXISTS `sys_user_permission`;
CREATE TABLE `sys_user_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userid` bigint(20) NOT NULL,
  `functionid` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=tis620;


-- 2015-10-24 16:49:57
