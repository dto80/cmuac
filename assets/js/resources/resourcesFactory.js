'use strict'
var resourceFactory = angular.module('resourceFactories', ['ngResource']);

resourceFactory.factory('facultyResource',
    function ($resource) {
        return $resource('/faculty/:action', {action:''}, {
                saveFaculty: {
                    method: 'POST'
                },
                getFaculties:{
                    method: 'GET',
                    isArray:true
                }
            }
        )
    }
);

resourceFactory.factory('activityResource',
    function($resource){
        return $resource('/activity',{},{
           saveActivity:{
               method: 'POST'
           }
        });
    });

resourceFactory.factory('activityFocusTemplateResource',
    function($resource){
        return $resource('/activity/activityFocusTemplate',{},{
            saveActivityFocusTemplate:{
                method: 'POST'
            }
        })
    });

resourceFactory.factory('activityFocusTemplateDetailsResource',
    function($resource){
        return $resource('/activity/activityFocusTemplateDetails',{},{
            saveActivityFocusTemplateDetails:{
                method:'POST'
            },
            getActivityFocusTemplateDetails:{
                method:'GET',
                isArray:true
            }
        })
    }
)