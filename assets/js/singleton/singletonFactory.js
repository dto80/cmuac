'use strict'
var singletonFactorys = angular.module('singletonFactories',['singletonDao', 'resourceFactories']);

singletonFactorys.factory('facultyFactory',['facultyResource',
    function(facultyResource){
        return facultyResource;
    }
]);

singletonFactorys.factory('activityFactory',['activityResource',
    function(activityResource){
        return activityResource;
    }]);

singletonFactorys.factory('activityFocusTemplateFactory',['activityFocusTemplateResource',
    function(activityFocusTemplateResource){
        return activityFocusTemplateResource;
    }]);

singletonFactorys.factory('activityFocusTemplateDetailsFactory',['activityFocusTemplateDetailsResource',
    function(activityFocusTemplateDetailsResource){
        return activityFocusTemplateDetailsResource;
    }])

