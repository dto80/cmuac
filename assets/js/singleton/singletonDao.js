var singletonDaos = angular.module('singletonDao', ['resourceFactories']);

singletonDaos.factory('facultyDao', ['facultyResource',
    function (facultyResource) {
        var getFaculties = function () {
            return facultyResource.getFaculties();
        }

        var addFaculty = function (saveData) {

            return facultyResource.saveFaculty(saveData);
        }

        var updateFaculty = function (updatedData) {
            return facultyResource.saveFaculty(updatedData);
        }
        return {
            getFaculties: getFaculties,
            addFaculty: addFaculty,
            updateFaculty: updateFaculty
        }
    }])

var singletonMockFactories = angular.module('singletonMockFactories', []);

singletonMockFactories.factory('facultyDao', [
    function () {
        var data = [
            {
                "facultyId": "01",
                "name": "มนุษยศาสตร์",
                "organizers": [
                    {"id": "01", "name": "คณะ"},
                    {"id": "02", "name": "สโมสรนักศึกษาคณะมนุษยศาสตร์"}
                ]
            },
            {
                "facultyId": "02",
                "name": "ศึกษาศาสตร์",
                "organizers": [
                    {"id": "01", "name": "คณะ"},
                    {"id": "02", "name": "สโมสรนักศึกษาคณะศึกษาศาสตร์"}
                ]

            },
            {
                "facultyId": "03",
                "name": "สังคมศาสตร์",
                "organizers": [
                    {"id": "01", "name": "คณะ"},
                    {"id": "02", "name": "สโมสรนักศึกษาคณะสังคมศาสตร์"}
                ]

            }, {
                "facultyId": "04",
                "name": "วิทยาศาสตร์",
                "organizers": [
                    {"id": "01", "name": "คณะ"},
                    {"id": "02", "name": "สโมสรนักศึกษาคณะวิทยาศาสตร์"}
                ]

            }
        ];
        var getFaculties = function () {
            return data;
        }

        var addFaculties = function (saveData) {
            data.push(saveData);
            return data;
        }

        var updateFaculty = function (oldData, updatedData) {
            data.extend()
        }
        return {
            getFaculties: getFaculties,
            addFaculty: addFaculty,
            updateFaculty: updateFaculty
        }
    }])