var gulp = require('gulp');
var connect = require('gulp-connect');
var rename = require("gulp-rename");
//var browserify = require('browserify');
//var source = require ('vinyl-source-stream');
gulp.task('connect',function(){
    connect.server({
        root: 'public',
        port: 4000
    })
});
/*
gulp.task('browserify',function(){
    return browserify('./public/app.js')
        .bundle()
        .pipe(source(''))
})*/

gulp.task('renameProduction',function(){
	gulp.src("./system/conf/config-production.php")
		.pipe(rename("config.php"))
		.pipe(gulp.dest("./system/conf"));
});